<?php use_helper('I18N')?> 

<div class="col-md-7">    
    <div class="panel panel-success">
        <div class="panel-heading ">
            <span class="panel-title"><?php echo __("Actualizar Estrato")?></span>
            <div class="panel-heading-controls">
                <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
            </div>
        </div>
        <div class="panel-body">

            <div class="wizard ui-wizard-example">
                <div class="wizard-wrapper">
                    <ul class="wizard-steps">
                        <li data-target="#wizard-example-step1" >
                            <span class="wizard-step-number">1</span>
                            <span class="wizard-step-caption">
                                Paso 1
                                <span class="wizard-step-description">Eliminación de registros</span>
                            </span>
                        </li
                        ><li data-target="#wizard-example-step2"> <!-- ! Remove space between elements by dropping close angle -->
                            <span class="wizard-step-number">2</span>
                            <span class="wizard-step-caption">
                                Paso 2
                                <span class="wizard-step-description">Validación de registros</span>
                            </span>
                        </li
                        ><li data-target="#wizard-example-step3"> <!-- ! Remove space between elements by dropping close angle -->
                            <span class="wizard-step-number">3</span>
                            <span class="wizard-step-caption">
                                Paso 3
                                <span class="wizard-step-description">Actualización de predios a partir de la base catastral</span>
                            </span>
                        </li
                        ><li data-target="#wizard-example-step4"> <!-- ! Remove space between elements by dropping close angle -->
                            <span class="wizard-step-number">4</span>
                            <span class="wizard-step-caption">
                                Paso 4
                                <span class="wizard-step-description">Actualización de predios a partir de la ES</span>
                            </span>
                        </li
                        ><li data-target="#wizard-example-step5"> <!-- ! Remove space between elements by dropping close angle -->
                            <span class="wizard-step-number">5</span>
                            <span class="wizard-step-caption">
                                Paso 5
                                <span class="wizard-step-description">Actualización de estrato de los predios</span>
                            </span>
                        </li
                        ><li data-target="#wizard-example-step6"> <!-- ! Remove space between elements by dropping close angle -->
                            <span class="wizard-step-number">6</span>
                            <span class="wizard-step-caption">
                                Generar reporte
                            </span>
                        </li>
                    </ul> <!-- / .wizard-steps -->
                </div> <!-- / .wizard-wrapper -->
                
                <div class="wizard-content panel">
                    <div class="wizard-pane" id="wizard-example-step1">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description">En este paso se eliminaran los registros de las tablas <strong>"est_bdcatastral"</strong>, <strong>"est_es_anterior"</strong> y <strong>"est_es"</strong>. <br><br></span>
                        </div>
                        <button class="btn btn-success wizard-eliminar-btn" url="../estratificacion/ElimiarRegistrosEstrato">Eliminar</button>
                        <button class="btn btn-primary wizard-next-step-btn">Siguiente</button>
                        <!--<button class="btn btn-primary wizard-prueba">Prueba</button>-->
                    </div> <!-- / .wizard-pane -->
                    
                    <div class="wizard-pane" id="wizard-example-step2" style="display: none;">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description"> En este paso se valida los registros ingresados en las tablas <strong>"est_bdcatastral"</strong>, <strong>"est_es_anterior"</strong> y <strong>"est_es"</strong>.<br><br></span>
                        </div>
                        <!--<button class="btn wizard-prev-step-btn">Anterior</button>-->
                        <button class="btn btn-success wizard-validar-btn" url="../estratificacion/ValidarRegistrosEstrato">Validar</button>
                        <button class="btn btn-primary wizard-next-step-btn">Siguiente</button>
                    </div> <!-- / .wizard-pane -->
                    
                    <div class="wizard-pane" id="wizard-example-step3" style="display: none;">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description"> En este paso se actualizan los predios cuya comuna, barrio, manzana o lado hayan cambiando.<br><br></span>
                        </div>
                        <!--<button class="btn wizard-prev-step-btn">Prev</button>-->
                        <button class="btn btn-success wizard-actualizar-predio-btn" url="../estratificacion/ActualizarPrediosEstrato?codigoestadosubproceso=actualizarpredios">Actualizar</button>
                        <button class="btn btn-primary wizard-next-step-btn">Siguiente</button>
                    </div> <!-- / .wizard-pane -->

                    <div class="wizard-pane" id="wizard-example-step4" style="display: none;">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description"> En este paso se actualizan los predios cuyo estrato ha cambiando con respecto a la ES anterior.<br><br></span>
                        </div>
                        <!--<button class="btn wizard-prev-step-btn">Prev</button>-->
                        <button class="btn btn-success wizard-actualizar-predioes-btn" url="../estratificacion/ActualizarPrediosEstrato?codigoestadosubproceso=actualizarpredioses">Actualizar</button>
                        <button class="btn btn-primary wizard-next-step-btn">Siguiente</button>
                    </div> <!-- / .wizard-pane -->

                    <div class="wizard-pane" id="wizard-example-step5" style="display: none;">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description"> En este paso se actualizan los estratos de los  predios.<br><br></span>
                        </div>
                        <!--<button class="btn wizard-prev-step-btn">Prev</button>-->
                        <button class="btn btn-success wizard-actualizar-estrato-btn" url="../estratificacion/ActualizarMasivoEstrato">Actualizar</button>
                        <button class="btn btn-primary wizard-next-step-btn">Siguiente</button>
                    </div> <!-- / .wizard-pane -->

                    <div class="wizard-pane" id="wizard-example-step6" style="display: none;">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description"> Generar reporte final del proceso<br><br></span>
                            <div id="reporte_est"> </div>
                        </div>
                        <button class="btn btn-success wizard-generar-reporte-btn" url="../estratificacion/GenerarReporteProcesoEstrato">Generar</button>
                        <button class="btn btn-primary wizard-finish-step-btn">Finalizar</button>
                    </div> <!-- / .wizard-pane -->
                </div> <!-- / .wizard-content -->
            </div> <!-- / .wizard -->
        </div>
    </div>    
</div>
