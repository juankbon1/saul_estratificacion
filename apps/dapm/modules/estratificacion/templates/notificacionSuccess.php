<style>
    /*.modal-dialog{
        position: absolute;
        left: 20%;
        top: 30%;
    }*/
    .help-block {
        margin-top: -14px;
        margin-bottom: 20px;
    }
    /*.padding-sm{
        padding: 0px 15px !important;
    }
    .has-warning .help-block, .has-error 
    .help-block, .has-success .help-block {
        margin-bottom: 0px;
    }*/

</style>

<div class="col-md-8">    
    <div class="panel panel-success">
        <div class="panel-heading ">
            <span class="panel-title"><?php echo __("Notificación")?></span>
            <div class="panel-heading-controls"></div>
        </div>
        <div class="panel-body">        
            <form action="<?php echo url_for('estratificacion/notificacion')?>" method="post" enctype="multipart/form-data" name="form_notificacion" id="form_notificacion" >
                <div class="row radicado_prefix" url="../solicitud/radicadoNotificacion">
                    <?php echo $formsolicitud['radicado_notificacion']->renderRow()?>
                </div>
                <div class="row">
                    <?php echo $formnotificacion['dapm']->renderRow()?>
                </div>
                <div class="row">
                    <?php echo $formnotificacion['fecha_notificacion']->renderRow()?>
                </div>
                <div class="row">
                    <?php echo $formdocumentos['archivonotificacion']->renderRow()?>
                    <small class="help-block">Debe adjuntar documento escaneado como soporte de la apelación <br> (Tamaño de archivo: máximo 2 MB, Formatos soportados: pdf).</small>
                </div>
                <div class="row padding-sm text-right-sm">
                    <button id="consultar" type="submit" class="btn btn-primary" value="<?php echo __("Notificar")?>">Anexar notificación</button>
                </div>
            </form>  
        </div>
    </div>
</div>