<style>
    .modal-dialog{
        position: absolute;
        left: 20%;
        top: 30%;
    }
    .help-block {
        margin-top: -14px;
        margin-bottom: 20px;
    }
    .padding-sm{
        padding: 0px 15px !important;
    }
    .has-warning .help-block, .has-error 
    .help-block, .has-success .help-block {
        margin-bottom: 0px;
    }

</style>

<div class="col-md-8">    
    <div class="panel panel-success">
        <div class="panel-heading ">
            <span class="panel-title"><?php echo __("Editar radicado de respuesta")?></span>
            <div class="panel-heading-controls"></div>
        </div>
        <div class="panel-body">        
            <form action="<?php echo url_for('estratificacion/editarRadicadoRespuesta')?>" method="post" enctype="multipart/form-data" name="form_consultar_radicado" id="form_consultar_radicado" >
                <?php echo $formsolicitud->renderHiddenFields()?>
                <input type="hidden" id="codigotiposolicitud" name="codigotiposolicitud" value="<?php echo __($sf_params->get('codigotiposolicitud'))?>" />
                <div class="row radicado_prefix" url="../solicitud/radicado">
                    <?php echo $formsolicitud['radicado']->renderRow()?>
                </div>
                <div class="row padding-sm text-right-sm">
                    <button id="consultar" type="submit" class="btn btn-primary" value="<?php echo __("Consultar")?>">Consultar</button>
                </div>
            </form>  
        </div>
    </div>
</div>

<?php if(isset($infosolicitud)):?>
    <?php if($infosolicitud == "certificado"):?>
        <div class="col-md-8">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <span class="panel-title"><?php echo __("Información del radicado")?></span>
                    <div class="panel-heading-controls">
                        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row padding-sm">   
                        <div class="row">
                            <div class="note note-danger">
                                <?php echo __("El radicado que esta consultando corresponde a una solicitud de certificado de estrato. &nbsp;")?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>

    <?php if($infosolicitud == "sinradicadohijo"):?>
        <div class="col-md-8">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <span class="panel-title"><?php echo __("Información del radicado")?></span>
                    <div class="panel-heading-controls">
                        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row padding-sm">   
                        <div class="row">
                            <div class="note note-danger">
                                <?php echo __("El radicado que esta consultando no tiene un radicado de respuesta asociado para reemplazar. &nbsp;")?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>

    <?php if(is_object($infosolicitud)):?>
        <div class="col-md-8">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <span class="panel-title"><?php echo __("Información del radicado")?></span>
                    <div class="panel-heading-controls">
                        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="<?php echo url_for('estratificacion/actualizarRadicadoHijo')?>" method="post" enctype="multipart/form-data" name="form_editar_radicado_hijo" id="form_editar_radicado_hijo" >
                        <div class="row padding-sm">
                            <div class="row">
                                <?php echo $formsolicitud->renderHiddenFields()?>
                                <input type="hidden" id="idsolicitud" name="idsolicitud" value="<?php echo __($infosolicitud['idsolicitud'])?>" />
                                <input type="hidden" id="idsolicitud" name="numradicadohijo" value="<?php echo __($infosolicitud['numradicadohijo'])?>" />
                                <div class="note note-success">
                                    <?php echo __("<b> Radicado padre: </b>")?><?php echo '<a href="http://www.cali.gov.co/aplicaciones/orweb/orweb/principal.php?id=' . $infosolicitud['numradicado'] . '" target="_blank">' . $infosolicitud['numradicado'] . '</a><br>'?>                                                                                          
                                    <?php echo __("<b> Radicado hijo: </b>")?><?php echo '<a href="http://www.cali.gov.co/aplicaciones/orweb/orweb/principal.php?id=' . $infosolicitud['numradicado'] . '" target="_blank">' . $infosolicitud['numradicadohijo'] . '</a><br>'?>                                                                                          
                                </div>

                            </div>  
                        </div>
                        <div class="row">
                            <?php echo $formsolicitud['radicado_hijo']->renderRow()?>
                        </div>
                        <div class="row padding-sm text-right-sm">
                            <button id="cambiar" type="submit" class="btn btn-primary" value="<?php echo __("Cambiar")?>">Cambiar</button>
                        </div>
                    </form>
                </div>
            </div>    
        </div>
    <?php endif;?>
<?php endif;?>


