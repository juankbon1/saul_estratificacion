<?php use_helper('I18N');
?>

<div class="col-md-7">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span class="panel-title"><?php echo __("Información de la actualización")?></span>
            <div class="panel-heading-controls">
                <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
            </div>
        </div>
        <div class="panel-body"> 
            <div class="row padding-sm">
                <div class="row">
                    <div class="note note-success">
                        <?php echo __("Se asigno el estrato al predio con metodología tipo 3 o metodología especial de forma exitosa. &nbsp;")?>
                        <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                    </div>
                </div> 
                <div class="row">
                    <div class="note note-success">
                        <?php echo __("<b> Número predial nacional: </b>")?><?php echo $infoestrato->Predio->codigounico . '<br>'?>
                        <?php if($infoestrato->getEstratoanterior()):?>
                            <?php echo __("<b> Estrato anterior: </b>")?><?php echo $infoestrato->getEstratoanterior() . '<br>'?> 
                        <?php endif;?>
                        <?php echo __("<b> Estrato actual: </b>")?><?php echo $infoestrato->getEstratoactual() . '<br>'?>
                    </div>
                </div>
                <div class="row padding-sm text-right-sm">
                    <?php echo button_to('Mis Solicitudes', 'solicitud/index', array('class'=>"btn btn-primary  boton_link"))?>
                </div>
            </div> 
        </div>
    </div>
</div>

