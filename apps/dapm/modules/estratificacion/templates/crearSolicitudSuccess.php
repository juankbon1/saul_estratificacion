<!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?libraries=geometry&sensor=true"></script>-->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBThRDG8GqEYOkooxSkSKS0S2EIHHbfwxk&callback=initMap"
type="text/javascript"></script>
<script type="text/javascript">
// VARIABLES GLOBALES JAVASCRIPT
    var geocoder;
    var marker;
    var latLng;
    var map;

// CAMBIA LA PROPIEDAD DISPLAY DEL CANVAS Y LO REINICIALIZA
    function displayMap() {
        document.getElementById('mapCanvas').style.display = "block";
        initialize();
    }

// INICIALIZACION DE MAPA
    function initialize() {
        geocoder = new google.maps.Geocoder();
        latLng = new google.maps.LatLng(3.4516467, -76.5319854);
        map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 12,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.HYBRID
        });

// CREACION DEL MARCADOR  
        marker = new google.maps.Marker({
            position: latLng,
            title: 'Arrastra el marcador si quieres moverlo',
            map: map,
            draggable: true
        });

// Escucho el CLICK sobre el mama y si se produce actualizo la posicion del marcador 
        google.maps.event.addListener(map, 'click', function (event) {
            updateMarker(event.latLng);
        });

        geocodePosition(latLng);

        // Permito los eventos drag/drop sobre el marcador
        google.maps.event.addListener(marker, 'dragstart', function () {
            updateMarkerAddress('Arrastrando...');
        });

        google.maps.event.addListener(marker, 'drag', function () {
            updateMarkerStatus('Arrastrando...');
            updateMarkerPosition(marker.getPosition());
        });

        google.maps.event.addListener(marker, 'dragend', function () {
            updateMarkerStatus('Arrastre finalizado');
            geocodePosition(marker.getPosition());
        });
    }

// Permito la gestion de los eventos DOM
    google.maps.event.addDomListener(window, 'load', initialize);
    google.maps.event.trigger(map, 'resize');

// RECUPERO LOS DATOS LON LAT Y DIRECCION Y LOS PONGO EN EL FORMULARIO
    function updateMarkerPosition(latLng) {
        document.form_solicitud.longitud.value = latLng.lng();
        document.form_solicitud.latitud.value = latLng.lat();
    }

// ACTUALIZO LA POSICION DEL MARCADOR
    function updateMarker(location) {
        marker.setPosition(location);
        updateMarkerPosition(location);
        geocodePosition(location);
    }

    function obtenerModalIngresoDireccion() {
        $.ajax({
            url: "../../../nomenclatura",
//            url: "https://172.18.1.35/saul_dev/dapm_dev.php/nomenclatura",
            data: {
                fieldNamecallbackFunction: "cerrarModal",
                fieldNameNPNrequerido: false,
                fieldNameDiv: "modalIngresoDireccion",
                fieldNameNumeroUnico: "numeroprenuevo",
                fieldNameDireccion: "direccionnueva",
                fieldNameDireccionAdicional: "direccionadicional"
            },
            success: function (data) {
                $('#modalIngresoDireccion').empty();
                $('#modalIngresoDireccion').append(data);
                $('#modalIngresoDireccion').modal('show');
                $('.overlap_espera_1').hide();
                $('.overlap_espera').hide();
            }
        });
    }

    function cerrarModal() {
        $('#modalIngresoDireccion').modal('toggle');
    }

</script>

<style>
    .modal-dialog{
        position: absolute;
        left: 20%;
        top: 30%;
    }
    .help-block {
        margin-top: -14px;
        margin-bottom: 20px;
    }
    .padding-sm{
        padding: 0px 15px !important;
    }
    .has-warning .help-block, .has-error 
    .help-block, .has-success .help-block {
        margin-bottom: 0px;
    }

</style>

<div id="modalIngresoDireccion" class="modal fade modal-change" role="dialog" aria-hidden="true" style="display: none;"></div>

<div class="col-md-8">    
    <div class="panel panel-success">
        <div class="panel-heading ">
            <?php if($sf_params->get('codigotiposolicitud') == Tiposolicitud::CERTIFICADO_ESTRATO):?>
                <span class="panel-title"><?php echo __("Solicitud de certificado de estrato")?></span>
            <?php endif;?>
            <?php if($sf_params->get('codigotiposolicitud') == Tiposolicitud::REVISION_ESTRATO):?>
                <span class="panel-title"><?php echo __("Solicitud de revisión de estrato")?></span>
            <?php endif;?>
            <?php if($sf_params->get('codigotiposolicitud') == Tiposolicitud::APELACION_ESTRATO):?>
                <span class="panel-title"><?php echo __("Solicitud de apelación")?></span>
            <?php endif;?>
            <div class="panel-heading-controls">
            </div>
        </div>
        <div class="panel-body">        
            <form action="<?php echo url_for('estratificacion/crearSolicitud')?>" method="post" enctype="multipart/form-data" name="form_solicitud" id="form_solicitud">
                <?php echo $formsolicitud->renderHiddenFields()?>
                <input type="hidden" id="codigotiposolicitud" name="codigotiposolicitud" value="<?php echo __($sf_params->get('codigotiposolicitud'))?>" />
                <?php if($sf_params->get('codigotiposolicitud') == Tiposolicitud::APELACION_ESTRATO):?>
                    <div class="row radicado_prefix_hijo" url="../../../solicitud/RadicadoHijoApelacion">
                        <?php echo $formsolicitud['radicado_hijo_apelacion']->renderRow()?>
                        <small class="help-block">El radicado de respuesta Orfeo lo puede encontrar en la parte superior derecha del documento que recibió como respuesta a su solicitud de revisión de estrato.</small>
                    </div>
                    <div class="row">
                        <?php echo $formsolicitud['observacion']->renderRow()?>
                        <small class="help-block" id="contador">Ingresar la información adicional que considere pertinente para complementar la solicitud. <br> Cantidad máxima de caracteres, caracteres restantes: <span> </span></small>
                    </div>
                    <div class="row">
                        <?php echo $formdocumentos['archivoapelacion']->renderRow()?>
                        <small class="help-block">Debe adjuntar documento escaneado como soporte de la apelación <br> (Tamaño de archivo: máximo 2 MB, Formatos soportados: pdf).</small>
                    </div>
                <?php else:?>
                    <div class="row padding-sm">
                        <?php echo $formubicacionpredio['tipozona']->renderRow()?>
                    </div>

                    <div id="ubicacionurbana">
                        <div class="row padding-sm">
                            <?php echo $formubicacionpredio['tipobusqueda']->renderRow()?>
                        </div>   
                        <div class="row dir_select" url="../../../solicitud/Direccion">
                            <?php echo $formubicacionpredio['direccion']->renderRow()?>
                        </div> 
                        <div class="row npn_prefix_urbano" url="../../../solicitud/Numpredial">
                            <?php echo $formubicacionpredio['numeropre']->renderRow()?>
                            <small class="help-block">El número predial nacional lo puede encontrar en el recibo predial, el cual puede ser solicitado en la subdirección de catastro municipal.</small>
                        </div>
                        <div class="row padding-sm">
                            <?php echo $formubicacionpredio['consistencia']->renderRow()?>
                        </div>
                        <div class="row">
                            <?php echo $formsolicitud['observacion']->renderRow()?>
                            <small class="help-block" id="contador">Ingresar la información adicional que considere pertinente para complementar la solicitud. <br> Cantidad máxima de caracteres, caracteres restantes: <span> </span></small>
                        </div>
                    </div>

                    <div id="ubicacionrural">
                        <div class="row npn_prefix_rural" url="../../../solicitud/NumpredialSinDireccion">
                            <?php echo $formubicacionpredio['numeropre_rural']->renderRow()?>
                            <small class="help-block">El número predial nacional lo puede encontrar en el recibo predial, el cual puede ser solicitado en la subdirección de catastro municipal.</small>
                        </div>
                        <div class="row padding-sm">
                            <?php echo $formubicacionpredio['existe_npn']->renderRow()?>
                        </div>
                        <div class="row" id="numprenuevorural">
                            <?php echo $formubicacionpredio['numeroprenuevo_rural']->renderRow()?>
                            <small class="help-block">Ingresar el número predial nacional de 30 caracteres correspondiente al predio.<span> </span></small>
                        </div>
                        <div class="row">
                            <?php echo $formubicacionpredio['latitud']->renderRow()?>
                            <small class="help-block">Debe marcar la ubicación de su predio en el mapa, la información de latitud aparecerá de forma automática.</small>
                        </div>
                        <div class="row">
                            <?php echo $formubicacionpredio['longitud']->renderRow()?>
                            <small class="help-block">Debe marcar la ubicación de su predio en el mapa, la información de longitud aparecerá de forma automática.</small>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <span class="panel-title"><i class="panel-title-icon fa fa-map-marker"></i>Google Maps</span>
                            </div>
                            <div class="panel-body" style="position:relative;height: 300px;">
                                <div class="widget-maps" id="mapCanvas">
                                </div>
                            </div>
                        </div>
                        <small class="help-block">Clic sobre el mapa para seleccionar la ubicación de su predio.</small>
                        <div class="row">
                            <?php echo $formsolicitud['comentario']->renderRow()?>
                            <small class="help-block">Ingresar información adicional relacionada a la ubicación de su predio.</small>
                        </div>
                        <div class="row">
                            <?php echo $formsolicitud['observacion_rural']->renderRow()?>
                            <small class="help-block" id="contador">Ingresar la información adicional que considere pertinente para complementar la solicitud. <br> Cantidad máxima de caracteres, caracteres restantes: <span> </span></small>
                        </div>
                    </div>

                    <div id="nuevaubicacion">
                        <div class="row">
                            <?php echo $formubicacionpredio['direccionnueva']->renderRow()?>
                        </div>  
                        <div class="row">
                            <?php echo $formubicacionpredio['numeroprenuevo']->renderRow()?>
                        </div>
                        <div class="row">
                            <?php echo $formubicacionpredio['direccionadicional']->renderRow()?>
                        </div>
                    </div>

                    <div class="row">
                        <?php echo $formdocumentos['archivoservicios']->renderRow()?>
                        <small class="help-block">Debe adjuntar documento escaneado del recibo de servicios públicos <br> (Tamaño de archivo: máximo 2 MB, Formatos soportados: jpg y pdf).</small>
                    </div>
                    <div  class="row">
                        <?php echo $formdocumentos['archivopredial']->renderRow()?>
                        <small class="help-block">Debe adjuntar documento escaneado del recibo predial, el cual puede ser solicitado en la subdirección de catastro municipal. <br> (Tamaño de archivo: máximo 2MB, Formatos soportados: jpg y pdf ).</small>
                    </div >
                    <div  class="row">
                        <?php echo $formdocumentos['archivoadicional']->renderRow()?>
                        <small class="help-block">Adjuntar documento adicional escaneado como sustentación de la solicitud. <br> (Tamaño de archivo: máximo 2MB, Formatos soportados: jpg y pdf ).</small>
                    </div >
                <?php endif;?>
                <div class="row padding-sm text-right-sm">
                    <button id="radicarsolicitud" type="submit" class="btn btn-primary" value="<?php echo __("Radicar solicitud")?>">Radicar solicitud</button>
                </div>
            </form>  
        </div>
    </div>
</div>