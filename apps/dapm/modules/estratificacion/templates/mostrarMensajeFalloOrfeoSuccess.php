<?php use_helper('I18N');
?>

<div class="col-md-7">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span class="panel-title"><?php echo __("Información de la solicitud")?></span>
            <div class="panel-heading-controls">
                <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row padding-sm">   
                <div class="row">
                    <div class="note note-danger">
                        <?php if(($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE) && ($infosolicitud['codigotiposolicitud'] == Tiposolicitud::CERTIFICADO_ESTRATO)):?>
                            <?php echo __("No ha sido posible generar el certificado de estrato por favor intentelo nuevamente. &nbsp;")?>
                            <i class="menu-icon fa fa-times-circle" style="color: red; font-size: x-large"></i>
                        <?php endif;?>
                        <?php if($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_POR_DOCS):?>
                            <?php echo __("No ha sido posible adjuntar los documentos anexos en Orfeo por favor intentelo nuevamente. &nbsp;")?>
                            <i class="menu-icon fa fa-times-circle" style="color: red; font-size: x-large"></i>
                        <?php endif;?>
                        <?php if($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_ANEXOS_ORFEO):?>
                            <?php echo __("No ha sido posible cargar los documentos anexos en Orfeo por favor intentelo nuevamente. &nbsp;")?>
                            <i class="menu-icon fa fa-times-circle" style="color: red; font-size: x-large"></i>
                        <?php endif;?>
                        <?php if($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_PLANTILLA_ORFEO):?>
                            <?php echo __("No ha sido posible cargar la plantilla en Orfeo por favor intentelo nuevamente. &nbsp;")?>
                            <i class="menu-icon fa fa-times-circle" style="color: red; font-size: x-large"></i>
                        <?php endif;?>
                    </div>
                </div>
                <div class="row">
                    <div class="note note-danger">
                        <?php if(isset($mensaje)):?>
                            <?php echo __("Por favor comuniquese con el administrador del sistema. <br><br>")?>
                            <?php echo __("<b> Mensaje: </b>")?><?php echo __($mensaje . '<br>')?>
                            <?php echo __("<b> Código de error: </b>")?><?php echo __($idlog . " <br>")?>
                        <?php endif;?>
                    </div>
                </div>
                <div class="row padding-sm text-right-sm">
                    <?php if($sf_user->getGuardUser()->hasPermission('ciudadano')):?>
                        <?php echo button_to('Regresar', 'estratificacion/crearSolicitud?codigotiposolicitud=' . $infosolicitud['codigoestadosolicitud'], array('class'=>"btn btn-primary  boton_link"))?>
                    <?php endif;?>
                    <?php echo button_to('Mis Solicitudes', 'solicitud/index', array('class'=>"btn btn-primary  boton_link"))?>
                </div>
            </div>
        </div>
    </div>
</div>

