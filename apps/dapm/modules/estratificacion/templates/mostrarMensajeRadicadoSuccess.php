<?php use_helper('I18N');?>

<script>
    $(document).ready(function () {

    });
    function generarsolicitudrevision() {
        $('.overlap_espera').fadeIn(500, 'linear');
        $('.overlap_espera_1').fadeIn(500, 'linear');
        $(this).attr('disabled', true);
        $(this).val('Enviando...');
        window.location.href = "../../../../generarSolicitudRevision/idsolicitud/" + "<?php echo $infosolicitud['idsolicitud'];?>";
    }
    function asociarRadicadoRespuesta() {
        $('.overlap_espera').fadeIn(500, 'linear');
        $('.overlap_espera_1').fadeIn(500, 'linear');
        $(this).attr('disabled', true);
        $(this).val('Enviando...');
        window.location.href = "../../../../../solicitud/asociarRadicadoRespuesta/numradicado/" + "<?php echo $infosolicitud['numradicado'];?>" + "/idsolicitud/" + "<?php echo $infosolicitud['idsolicitud'];?>";
    }
</script>

<style>
    .help-block {
        margin-top: -14px;
        margin-bottom: 20px;
    }
    .padding-sm{
        padding: 0px 15px !important;
    }
    .has-warning .help-block, .has-error 
    .help-block, .has-success .help-block {
        margin-bottom: 0px;
    }
    input.mayusculas{
        text-transform:uppercase;
    } 
</style>

<div class="col-md-7">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span class="panel-title"><?php echo __("Información de la solicitud")?></span>
            <div class="panel-heading-controls">
                <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
            </div>
        </div>
        <div class="panel-body">
            <input type="hidden" name="idsolicitud" id="idsolicitud" value="<?php echo $infosolicitud['idsolicitud']?>">
            <form method="post" name="registro_de_solicitudes" id="registro_de_solicitudes" action="<?php echo url_for('estratificacion/CambiarEstado')?>">
                <input type="hidden" name="idsolicitud" id="idsolicitud" value="<?php echo $infosolicitud['idsolicitud']?>"> 
                <input type="hidden" name="nombretiposolicitud" id="nombretiposolicitud" value="<?php echo $infosolicitud['nombretiposolicitud']?>">
                <input type="hidden" name="estratonuevo" id="estratonuevo" value="">
                <input type="hidden" name="numradicadohijo" id="numradicadohijo" value=<?php echo $infosolicitud['numradicadohijo']?>>
                <div class="row padding-sm">
                    <?php if($sf_user->getGuardUser()->hasPermission('ciudadano')):?>
                        <div class="row">
                            <div class="note note-success">
                                <?php echo __("La solicitud de " . $infosolicitud['nombretiposolicitud'] . " fue registrada de forma exitosa. &nbsp;")?><i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                            </div>
                        </div>
                    <?php endif;?>
                    <span class="panel-title"><?php echo __("Informarción del solicitante")?></span>        
                    <div class="row">
                        <div class="note note-success">
                            <?php echo __("<b> Nombre: </b>")?><?php echo $infosolicitud['nombresolicitante'] . "  " . $infosolicitud['apellidosolicitante'] . '<br>'?>
                            <?php echo __("<b> Correo: </b>")?><?php echo $infosolicitud['email'] . '<br>'?>
                            <?php echo __("<b> Telefono: </b>")?><?php echo $infosolicitud['telefono'] . '<br>'?>
                            <?php echo __("<b> Direccion: </b>")?><?php echo $infosolicitud['direccionsolicitante'] . '<br>'?>
                            <?php if($infosolicitud['nombrebarrio'] != ""):?>
                                <?php echo __("<b> Barrio: </b>")?><?php echo $infosolicitud['nombrebarrio'] . '<br>'?>
                            <?php else:?>
                                <?php echo __("<b> Vereda: </b>")?><?php echo $infosolicitud['nombrevereda'] . '<br>'?>
                            <?php endif;?>
                            <?php echo __("<b> Documento de identificación: </b>")?><?php echo $infosolicitud['tipodocumento'] . " - " . $infosolicitud['numeroidentificacion'] . '<br>'?>
                            <?php echo __("<b> Tipo de la empresa: </b>")?><?php echo $infosolicitud['nombretipoempresa'] . '<br>'?>
                        </div>
                    </div>
                    <span class="panel-title"><?php echo __("Informarción de la solicitud")?></span>
                    <div class="row">  
                        <div class="note note-success">
                            <?php echo __("<b> Fecha: </b>")?><?php echo $infosolicitud['fechasolicitud'] . '<br>'?>                                                         
                            <?php echo __("<b> Radicado: </b>")?><?php echo '<a href="http://www.cali.gov.co/aplicaciones/orweb/orweb/principal.php?id=' . $infosolicitud['numradicado'] . '" target="_blank">' . $infosolicitud['numradicado'] . '</a><br>'?>  
                            <?php if($infosolicitud['codigotiposolicitud'] == Tiposolicitud::APELACION_ESTRATO) :?>
                                <?php echo __("<b> Radicado revisión estrato: </b>")?><?php echo '<a href="http://www.cali.gov.co/aplicaciones/orweb/orweb/principal.php?id=' . $infosolicitud['numradicadopadre'] . '" target="_blank">' . $infosolicitud['numradicadopadre'] . '</a><br>'?>
                            <?php endif;?>
                            <?php echo __("<b> Tipo solicitud: </b>")?><?php echo $infosolicitud['nombretiposolicitud'] . '<br>'?>                                                          
                            <?php echo __("<b> Estado solicitud: </b>")?><?php echo $infosolicitud['nombreestadosolicitud'] . '<br>'?>  
                            <?php echo __("<b> Comentario: </b>")?><?php echo $infosolicitud['comentario'] . '<br>'?>                                                          
                            <?php if(($infosolicitud['tipopredio'] == "Nuevo") && (($sf_user->getGuardUser()->hasPermission('profesional_estratificacion')) || ($sf_user->getGuardUser()->hasPermission('tecnico_visita_estratificacion')))):?>
                                <?php echo __("<b> Número predial nacional: </b>")?><?php echo $infosolicitud['codigounico'] . ' -  <font color="red"> NUEVO</font> <br>'?>
                            <?php else:?>
                                <?php echo __("<b> Número predial nacional: </b>")?><?php echo $infosolicitud['codigounico'] . '<br>'?>
                            <?php endif;?>
                            <?php if($infosolicitud['codigoestadonomenclatura'] == Estado::CERTIFICADA):?>
                                <?php echo __("<b> Dirección: </b>")?><?php echo $infosolicitud['direccionpredio'] . ' -  <font color="green"><b> ( ' . $infosolicitud['nombreestadonomenclatura'] . ' ) </b></font><br>'?>
                            <?php elseif($infosolicitud['codigoestadonomenclatura'] == ""):?>
                                <?php echo __("<b> Dirección: </b>")?><?php echo '<font color="red"><b> No registra dirección </b></font><br>'?>
                            <?php else:?>
                                <?php echo __("<b> Dirección: </b>")?><?php echo $infosolicitud['direccionpredio'] . ' -  <font color="red"><b> ( Por Certificar ) </b></font> <br>'?>
                            <?php endif;?>
                            <?php if($infosolicitud['direccioncatastro'] != ""):?>
                                <?php echo __("<b> Direccion catastro: </b>")?><?php echo $infosolicitud['direccioncatastro'] . '<br>'?>
                            <?php endif;?>
                            <?php if($sf_user->getGuardUser()):?>
                                <?php if(($sf_user->getGuardUser()->hasPermission('profesional_estratificacion')) || ($sf_user->getGuardUser()->hasPermission('tecnico_visita_estratificacion'))):?>
                                    <?php echo __("<b> CUM: </b>")?><?php echo $infosolicitud['cum'] . '<br>'?>
                                    <?php if($infosolicitud['estrato']):?>
                                        <?php echo __("<b> Estrato: </b>")?><?php echo '<font id="estrato" color="black">' . $infosolicitud['estrato'] . '</font><br>'?>
                                    <?php else:?>
                                        <?php echo __("<b> Estrato: </b>")?><?php echo '<font color="red"> ESTRATO NO IDENTIFICADO </font><br>'?>
                                    <?php endif;?>
                                    <div id="estrato_cum">
                                        <?php echo __('<b id="nuevo_estrato"> </b>')?>
                                    </div>
                                    <?php if($infosolicitud['tipoatipicidad']):?>
                                        <?php echo __("<b> Tipo de atipicidad: </b>")?><?php echo '( ' . $infosolicitud['tipoatipicidad'] . ' )<br>'?>
                                    <?php endif;?>
                                    <?php if($sf_user->getGuardUser()->hasPermission('profesional_estratificacion')):?>
                                        <?php echo '<br>'?>
                                        <?php echo $formespecial['disperso']->renderRow()?>
                                        <?php echo $formespecial['expansion']->renderRow()?>
                                        <div id="opcion_expansion">
                                            <div class="px">
                                                Recalcular estrato a partir del CUM &nbsp;&nbsp; <i class="fa fa-long-arrow-right"></i> &nbsp;&nbsp;
                                                <a href="#" id="nuevo-cum" data-inputclass="mayusculas" data-type="text" data-pk="1" data-emptytext="Click para recalcular" data-placement="right" data-placeholder="CUM" data-title="Ingrese el nuevo CUM" url="../../../../RecalcularEstrato"></a>
                                            </div>
                                            <br>
                                            <?php echo $formespecial['cumnuevo']->renderRow()?>
                                            <?php echo $formespecial['direccion1']->renderRow()?>
                                            <small class="help-block">Debe ingresar la "Dirección 1" para proporcionar la ubicación del predio <br> (<b><em>Ejemplo:</em></b> el predio identificado con dirección "Dirección 1").</small>
                                            <?php echo $formespecial['direccion2']->renderRow()?>
                                            <small class="help-block">Debe ingresar la "Dirección 1" y la "Dirección 2" para dar una ubicación aproximada del predio <br> (<b><em>Ejemplo:</em></b> el predio cuya dirección se enmarca entre: "Dirección 1" y "Dirección 2").</small>
                                            <?php echo $formespecial['sector']->renderRow()?>
                                            <small class="help-block">Debe ingresar el "Nombre del sector" donde esta ubicado el predio <br> (<b><em>Ejemplo:</em></b> ubicado en el sector "Nombre sector").</small>
                                            <?php echo $formespecial['proyecto']->renderRow()?>
                                            <small class="help-block">Debe ingresar el "Nombre del proyecto" al cual pertenece el predio <br> (<b><em>Ejemplo:</em></b> proyecto "Nombre proyecto").</small>
                                        </div>
                                        <?php echo $formespecial['idcorregimiento']->renderRow()?>
                                    <?php endif;?>
                                <?php endif;?>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php if((($sf_user->getGuardUser()->hasPermission('profesional_estratificacion')) || ($sf_user->getGuardUser()->hasPermission('tecnico_visita_estratificacion'))) && ($infosolicitud['observacion'] != "")):?>
                        <span class="panel-title"><?php echo __("Observación")?></span>
                        <div class="row">                                              
                            <div class="note note-success">
                                <?php echo '<p align="justify">' . $infosolicitud['observacion'] . '</p>'?>
                            </div>
                        </div>
                    <?php endif;?>

                    <div class="row padding-sm text-right-sm">
                        <?php if($sf_user->isAuthenticated()):?>
                            <?php if($sf_user->getGuardUser()->hasPermission('ciudadano')):?>
                                <?php echo button_to('Regresar a la solicitud', 'estratificacion/crearSolicitud?codigotiposolicitud=' . $infosolicitud['codigotiposolicitud'], array('class'=>"btn btn-primary boton_link"))?>
                            <?php endif;?>
                        <?php else:?>
                            <?php echo button_to('Regresar a la solicitud', 'estratificacion/crearSolicitud?codigotiposolicitud=' . $infosolicitud['codigotiposolicitud'], array('class'=>"btn btn-primary boton_link"))?>
                        <?php endif;?>
                        <?php if($sf_user->getGuardUser() && ($infosolicitud['codigotiposolicitud'] == Tiposolicitud::CERTIFICADO_ESTRATO)):?>
                            <?php echo button_to('Mis Solicitudes', 'solicitud/index', array('class'=>"btn btn-primary boton_link"))?>
                            <?php if($sf_user->getGuardUser()->hasPermission('profesional_estratificacion') || $sf_user->getGuardUser()->hasPermission('aprobacioncertificado_estratificacion')): echo __("ó ");?>
                                <input type="submit" class="btn btn-primary" value="<?php echo __("Aprobar")?>"/>
                                <?php echo __("ó ");?>
                                <input type="button" class="btn btn-primary generar_solicitud_revision" value="<?php echo __("Generar revisión")?>" />
                            <?php endif;?>
                        <?php endif;?>
                        <?php if($sf_user->getGuardUser() && ($infosolicitud['codigotiposolicitud'] == Tiposolicitud::REVISION_ESTRATO)):?>
                            <?php echo button_to('Mis Solicitudes', 'solicitud/index', array('class'=>"btn btn-primary  boton_link", 'name'=>"regsolicitud"))?>
                            <?php if(($sf_user->getGuardUser()->hasPermission('profesional_estratificacion')) && ($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE)): echo __("ó ");?>
                                <input type="submit" class="btn btn-primary" value="<?php echo __("Generar visita")?>"/>
                            <?php endif;?>
                            <?php if(($sf_user->getGuardUser()->hasPermission('tecnico_visita_estratificacion')) && ($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_POR_VISITA)): echo __("ó ");?>
                                <input type="submit" class="btn btn-primary" value="<?php echo __("Marcar como visitada")?>"/>
                            <?php endif;?>
                            <?php if(($sf_user->getGuardUser()->hasPermission('profesional_estratificacion')) && ($infosolicitud['codigoestadosolicitud'] == Estado::VISITADA)): echo __("ó ");?>
                                <input type="button" class="btn btn-primary asociar_radicado_respuesta" value="<?php echo __("Asociar radicado respuesta")?>" />
                            <?php endif;?>
                        <?php endif;?>
                        <?php if($sf_user->getGuardUser() && ($infosolicitud['codigotiposolicitud'] == Tiposolicitud::APELACION_ESTRATO)):?>
                            <?php if(($sf_user->getGuardUser()->hasPermission('abogado_estratificacion')) && ($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE)):?>
                                <?php echo button_to('Mis Solicitudes', 'solicitud/index', array('class'=>"btn btn-primary"))?>
                                <?php echo __("ó ");?>
                                <input type="button" class="btn btn-primary asociar_radicado_respuesta" value="<?php echo __("Asociar radicado respuesta")?>" />
                            <?php endif;?>
                        <?php endif;?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>