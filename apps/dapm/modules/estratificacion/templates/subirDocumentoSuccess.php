<style>
    .modal-dialog{
        position: absolute;
        left: 20%;
        top: 30%;
    }
    .help-block {
        margin-top: -14px;
        margin-bottom: 20px;
    }
    .padding-sm{
        padding: 0px 15px !important;
    }
    .has-warning .help-block, .has-error 
    .help-block, .has-success .help-block {
        margin-bottom: 0px;
    }

</style>

<div id="modalIngresoDireccion" class="modal fade modal-change" role="dialog" aria-hidden="true" style="display: none;"></div>

<div class="col-md-8">    
    <div class="panel panel-success">
        <div class="panel-heading ">
            <?php if($sf_params->get('codigotiposolicitud') == Tiposolicitud::CERTIFICADO_ESTRATO):?>
                <span class="panel-title"><?php echo __("Solicitud de certificado de estrato")?></span>
            <?php endif;?>
            <?php if($sf_params->get('codigotiposolicitud') == Tiposolicitud::REVISION_ESTRATO):?>
                <span class="panel-title"><?php echo __("Solicitud de revisión de estrato")?></span>
            <?php endif;?>
            <?php if($sf_params->get('codigotiposolicitud') == Tiposolicitud::APELACION_ESTRATO):?>
                <span class="panel-title"><?php echo __("Solicitud de apelación")?></span>
            <?php endif;?>
            <div class="panel-heading-controls">
            </div>
        </div>
        <div class="panel-body">        
            <form action="<?php echo url_for('estratificacion/subirDocumento')?>" method="post" enctype="multipart/form-data" name="form_adjuntos" id="form_adjuntos">
                <input type="hidden" id="idsolicitud" name="idsolicitud" value="<?php echo __($sf_params->get('idsolicitud'))?>" />
                <?php if($infosolicitud['codigotiposolicitud'] == Tiposolicitud::APELACION_ESTRATO):?>
                    <div class="row">
                        <?php echo $formdocumentos['archivoapelacion']->renderRow()?>
                        <small class="help-block">Debe adjuntar documento escaneado como soporte de la apelación <br> (Tamaño de archivo: máximo 2 MB, Formatos soportados: pdf).</small>
                    </div>
                <?php else:?>
                    <div class="row">
                        <?php echo $formdocumentos['archivoservicios']->renderRow()?>
                        <small class="help-block">Debe adjuntar documento escaneado del recibo de servicios públicos <br> (Tamaño de archivo: máximo 2 MB, Formatos soportados: jpg y pdf).</small>
                    </div>
                    <div  class="row">
                        <?php echo $formdocumentos['archivopredial']->renderRow()?>
                        <small class="help-block">Debe adjuntar documento escaneado del recibo predial, el cual puede ser solicitado en la subdirección de catastro municipal. <br> (Tamaño de archivo: máximo 2MB, Formatos soportados: jpg y pdf ).</small>
                    </div >
                    <div  class="row">
                        <?php echo $formdocumentos['archivoadicional']->renderRow()?>
                        <small class="help-block">Adjuntar documento adicional escaneado como sustentación de la solicitud. <br> (Tamaño de archivo: máximo 2MB, Formatos soportados: jpg y pdf ).</small>
                    </div >
                <?php endif;?>
                <div class="row padding-sm text-right-sm">
                    <button id="cargararchivos" type="submit" class="btn btn-primary" value="<?php echo __("Cargar archivos")?>">Cargar archivos</button>
                </div>
            </form>  
        </div>
    </div>
</div>