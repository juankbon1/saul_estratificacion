<?php use_helper('I18N');
?>

<div class="col-md-7">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span class="panel-title"><?php echo __("Información de la solicitud")?></span>
            <div class="panel-heading-controls">
                <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
            </div>
        </div>
        <div class="panel-body">
            <input type="hidden" name="codigotiposolicitud" id="codigotiposolicitud" value="<?php echo $infosolicitud['codigotiposolicitud']?>"> 
            <div class="row padding-sm">
                <div class="row">
                    <div class="note note-success">
                        <?php if($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE):?>
                            <?php if($infosolicitud['codigoestadosolicitudanterior'] == Estado::PENDIENTE_POR_DOCS):?>
                                <?php echo __("Se cargaron los documentos anexos pendientes en SAUL de forma exitosa. &nbsp;")?>
                                <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                            <?php endif;?>
                            <?php if($infosolicitud['codigoestadosolicitudanterior'] == Estado::PENDIENTE_ANEXOS_ORFEO):?>
                                <?php echo __("Se adjuntaron los documentos anexos pendientes en Orfeo de forma exitosa. &nbsp;")?>
                                <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                            <?php endif;?>
                            <?php if($infosolicitud['codigoestadosolicitudanterior'] == Estado::PENDIENTE_PLANTILLA_ORFEO):?>
                                <?php echo __("Se adjunto la plantilla en Orfeo de forma exitosa. &nbsp;")?>
                                <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                            <?php endif;?>
                            <?php if($infosolicitud['codigoestadosolicitudanterior'] == Estado::PENDIENTE):?>
                                <?php echo __("La solicitud de <b>" . $infosolicitud['nombretiposolicitud'] . "</b> fue generada de forma exitosa. &nbsp;")?>
                                <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i></br></br>
                                <?php if($infosolicitud['codigotiposolicitud'] == Tiposolicitud::CERTIFICADO_ESTRATO):?>
                                    <?php echo __("<b>NOTA IMPORTANTE: </b>La respuesta a esta solicitud requiere de las siguientes estampillas: ProDesarrollo
                                                    Urbano (Alcaldía), Procultura (Alcaldia), Prosalud Departamental (Gobernación) y Prohospitales Universitarios
                                                    Departamentales (Gobernación). Sin ellas el certificado no tentrá validez.")?>
                                <?php endif;?>
                            <?php endif;?>
                        <?php endif;?>

                        <?php if($infosolicitud['codigoestadosolicitudanterior'] == Estado::PENDIENTE):?>
                            <?php if($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_POR_DOCS):?>
                                <?php echo __("La solicitud de <b>" . $infosolicitud['nombretiposolicitud'] . "</b> fue generada de forma exitosa. &nbsp;")?>
                                <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                            <?php endif;?>
                            <?php if($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_ANEXOS_ORFEO):?>
                                <?php echo __("La solicitud de <b>" . $infosolicitud['nombretiposolicitud'] . "</b> fue generada de forma exitosa. &nbsp;")?>
                                <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                            <?php endif;?>
                            <?php if($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_PLANTILLA_ORFEO):?>
                                <?php echo __("La solicitud de <b>" . $infosolicitud['nombretiposolicitud'] . "</b> fue generada de forma exitosa. &nbsp;")?>
                                <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                            <?php endif;?>
                        <?php endif;?>

                        <?php if(($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_POR_VISITA) || ($infosolicitud['codigoestadosolicitud'] == Estado::VISITADA)):?>
                            <?php echo __("La solicitud de <b>" . $infosolicitud['nombretiposolicitud'] . "</b> paso a estado <b>" . $infosolicitud['nombreestadosolicitud'] . "</b> de forma exitosa. &nbsp;")?>
                            <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                        <?php endif;?>

                        <?php if(($infosolicitud['codigoestadosolicitud'] == Estado::EMITIDO) && ! ($infosolicitud['codigoestadosolicitud'])):?>
                            <?php echo __("La solicitud de <b>" . $infosolicitud['nombretiposolicitud'] . "</b> fue aprobada de forma exitosa. &nbsp;")?>
                            <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                        <?php endif;?>

                        <?php if(($infosolicitud['codigoestadosolicitud'] == Estado::EMITIDO) && ($infosolicitud['codigoestadosolicitud'])):?>
                            <?php echo __("Se adjuntó el documento anexo de notificación en Orfeo de forma exitosa. &nbsp;")?>
                            <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                        <?php endif;?>

                        <?php if($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_POR_DOCS):?>
                            <?php if($infosolicitud['codigoestadosolicitudanterior'] == Estado::PENDIENTE_PLANTILLA_ORFEO):?>
                                <?php echo __("Se adjunto la plantilla en Orfeo de forma exitosa. &nbsp;")?>
                                <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                            <?php endif;?>
                        <?php endif;?>

                        <?php if($infosolicitud['codigoestadosolicitud'] == Estado::PENDIENTE_ANEXOS_ORFEO):?>
                            <?php if($infosolicitud['codigoestadosolicitudanterior'] == Estado::PENDIENTE_POR_DOCS):?>
                                <?php echo __("Se cargaron los documentos anexos pendientes en SAUL de forma exitosa. &nbsp;")?>
                                <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                            <?php endif;?>
                        <?php endif;?>
                    </div>
                </div> 
                <div class="row">
                    <div class="note note-success">
                        <?php if($infosolicitud['codigoestadosolicitud'] != Estado::PENDIENTE):?>
                            <?php echo __("<b> Estado anterior: </b>")?><?php echo $infosolicitud['nombreestadosolicitudanterior'] . '<br>'?>
                        <?php endif;?>
                        <?php echo __("<b> Estado actual: </b>")?><?php echo $infosolicitud['nombreestadosolicitud'] . '<br>'?>
                        <?php echo __("<b> Tipo solicitud: </b>")?><?php echo $infosolicitud['nombretiposolicitud'] . '<br>'?> 
                        <?php echo __("<b> Fecha de radicación: </b>")?><?php echo $infosolicitud['fechasolicitud'] . '<br>'?>   
                        <?php if($infosolicitud['codigoestadosolicitud'] == Estado::EMITIDO):?>
                            <?php echo __("<b> Fecha de respuesta: </b>")?><?php echo $infosolicitud['fecharespuesta'] . '<br>'?> 
                        <?php endif;?>
                        <?php echo __("<b> Radicado: </b>")?><?php echo '<a href="http://www.cali.gov.co/aplicaciones/orweb/orweb/principal.php?id=' . $infosolicitud['numradicado'] . '" target="_blank">' . $infosolicitud['numradicado'] . '</a><br>'?>                                                                                                       
                        <?php echo __("<b> Número predial nacional: </b>")?><?php echo $infosolicitud['codigounico'] . '<br>'?>  
                    </div>
                </div>
                <!--Esta es una nota temporal con vigencia hasta el 2 de Octubre de 2018-->
                <?php if($infosolicitud['nombretiposolicitud'] == "Certificado de Estrato"):?>
                    <!--Esta es una nota temporal con vigencia durante el tiempo que no se pueda entregar el certificado de estrato-->
                    <!--                    <div class="row">
                                            <div class="note note-danger"><strong>NOTA IMPORTANTE: </strong> Por motivos de ajustes en la plataforma web, se entregará respuesta a su solicitud de certificado de estrato a partir del día Lunes 12 de Febrero de 2018, podrá descargarlo a través de la plataforma SAUL o reclamarlo en la Oficina de Atención al Ciudadano Ventanilla Única, agradecemos su compresión.</div>
                                        </div>-->
                <?php endif;?>
                <div class="row padding-sm text-right-sm">
                    <?php echo button_to('Mis Solicitudes', 'solicitud/index', array('class'=>"btn btn-primary  boton_link"))?>
                </div>
            </div> 
        </div>
    </div>
</div>

