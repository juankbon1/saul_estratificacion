<?php use_helper('I18N')?> 

<div class="col-md-7">    
    <div class="panel panel-success">
        <div class="panel-heading ">
            <span class="panel-title"><?php echo __("Actualizar Atipicas")?></span>
            <div class="panel-heading-controls">
                <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
            </div>
        </div>
        <div class="panel-body">

            <div class="wizard ui-wizard-example">
                <div class="wizard-wrapper">
                    <ul class="wizard-steps">
                        <li data-target="#wizard-example-step1" >
                            <span class="wizard-step-number">1</span>
                            <span class="wizard-step-caption">
                                Paso 1
                                <span class="wizard-step-description">Eliminación de registros</span>
                            </span>
                        </li
                        ><li data-target="#wizard-example-step2"> <!-- ! Remove space between elements by dropping close angle -->
                            <span class="wizard-step-number">2</span>
                            <span class="wizard-step-caption">
                                Paso 2
                                <span class="wizard-step-description">Validación de registros</span>
                            </span>
                        </li
                        ><li data-target="#wizard-example-step3"> <!-- ! Remove space between elements by dropping close angle -->
                            <span class="wizard-step-number">3</span>
                            <span class="wizard-step-caption">
                                Paso 3
                                <span class="wizard-step-description">Actualización de atipicidades de los predios</span>
                            </span>
                        </li
                        ><li data-target="#wizard-example-step4"> <!-- ! Remove space between elements by dropping close angle -->
                            <span class="wizard-step-number">4</span>
                            <span class="wizard-step-caption">
                                Generar reporte
                            </span>
                        </li>
                    </ul> <!-- / .wizard-steps -->
                </div> <!-- / .wizard-wrapper -->
                
                <div class="wizard-content panel">
                    <div class="wizard-pane" id="wizard-example-step1">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description">En este paso se eliminaran los registros de la tabla <strong>"est_bd_atipicas"</strong>. <br><br></span>
                        </div>
                        <button class="btn btn-success wizard-eliminar-btn" url="../estratificacion/ElimiarRegistrosAtipica">Eliminar</button>
                        <button class="btn btn-primary wizard-next-step-btn">Siguiente</button>
                    </div> <!-- / .wizard-pane -->
                    
                    <div class="wizard-pane" id="wizard-example-step2" style="display: none;">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description"> En este paso se valida los registros ingresados en la tabla <strong>"est_bd_atipicas"</strong>.<br><br></span>
                        </div>
                        <button class="btn btn-success wizard-validar-btn" url="../estratificacion/ValidarRegistrosAtipica">Validar</button>
                        <button class="btn btn-primary wizard-next-step-btn">Siguiente</button>
                    </div> <!-- / .wizard-pane -->
                    
                    <div class="wizard-pane" id="wizard-example-step3" style="display: none;">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description"> En este paso se actualizan las atipicidades de los predios.<br><br></span>
                        </div>
                        <button class="btn btn-success wizard-actualizar-predio-btn" url="../estratificacion/ActualizarPrediosAtipica?codigoestadosubproceso=actualizarprediosatipica">Actualizar</button>
                        <button class="btn btn-primary wizard-next-step-btn">Siguiente</button>
                    </div> <!-- / .wizard-pane -->

                    <div class="wizard-pane" id="wizard-example-step4" style="display: none;">
                        <div class="note note-success">
                            <span class="estratificacion-wizard-description"> Generar reporte final del proceso<br><br></span>
                            <div id="reporte_est"> </div>
                        </div>
                        <button class="btn btn-success wizard-generar-reporte-btn" url="../estratificacion/GenerarReporteProcesoAtipica">Generar</button>
                        <button class="btn btn-primary wizard-finish-step-btn">Finalizar</button>
                    </div> <!-- / .wizard-pane -->
                    
                </div> <!-- / .wizard-content -->
            </div> <!-- / .wizard -->
        </div>
    </div>    
</div>
