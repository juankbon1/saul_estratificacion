<?php use_helper('I18N');
?>

<div class="col-md-7">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span class="panel-title"><?php echo __("Información de la solicitud")?></span>
            <div class="panel-heading-controls">
                <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
            </div>
        </div>
        <?php if ($estadoeditarradicadorespuesta == "true"):?>
        <div class="panel-body">
            <div class="row padding-sm">
                <div class="row">
                    <div class="note note-success">
                        <?php echo __("Se actualizó el radicado de respuesta de forma exitosa. &nbsp;")?>
                        <i class="menu-icon fa fa-check-circle" style="color: green; font-size: x-large"></i>
                    </div>
                </div> 
                <div class="row">
                    <div class="note note-success">  
                        <?php echo __("<b> Radicado padre: </b>")?><?php echo '<a href="http://www.cali.gov.co/aplicaciones/orweb/orweb/principal.php?id=' . $infosolicitud['numradicado'] . '" target="_blank">' . $infosolicitud['numradicado'] . '</a><br>'?>                                                                                          
                        <?php echo __("<b> Radicado hijo: </b>")?><?php echo '<a href="http://www.cali.gov.co/aplicaciones/orweb/orweb/principal.php?id=' . $infosolicitud['numradicadohijo'] . '" target="_blank">' . $infosolicitud['numradicadohijo'] . '</a><br>'?>
                    </div>
                </div>
                <div class="row padding-sm text-right-sm">
                    <?php echo button_to('Mis Solicitudes', 'solicitud/index', array('class'=>"btn btn-primary  boton_link"))?>
                </div>
            </div> 
        </div>
        <?php else:?>
        <div class="panel-body">
            <div class="row padding-sm">   
                <div class="row">
                    <div class="note note-danger">
                        <?php echo __("No ha sido posible actualizar el radicado de respuesta de la solicitud. &nbsp;")?>
                        <i class="menu-icon fa fa-times-circle" style="color: red; font-size: x-large"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="note note-danger">
                        <?php echo __("Por favor comuniquese con el administrador del sistema. <br><br>")?>
                    </div>
                </div>
                <div class="row padding-sm text-right-sm">
                    <?php if($sf_user->getGuardUser()->hasPermission('ciudadano')):?>
                        <?php echo button_to('Regresar', 'estratificacion/editarRadicadoRespuesta', array('class'=>"btn btn-primary  boton_link"))?>
                    <?php endif;?>
                    <?php echo button_to('Mis Solicitudes', 'solicitud/index', array('class'=>"btn btn-primary  boton_link"))?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>