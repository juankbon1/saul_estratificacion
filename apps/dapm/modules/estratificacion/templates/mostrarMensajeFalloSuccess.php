<?php use_helper('I18N');
?>

<div class="col-md-7">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span class="panel-title"><?php echo __("Información de la solicitud")?></span>
            <div class="panel-heading-controls">
                <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row padding-sm">   
                <div class="row">
                    <div class="note note-danger">
                        <?php if($tipo == 'radicacion'):?>
                            <?php echo __("No ha sido posible radicar la solicitud de " . $infosolicitud['nombretiposolicitud'] . ". &nbsp;")?>
                            <i class="menu-icon fa fa-times-circle" style="color: red; font-size: x-large"></i>
                        <?php endif;?>
                        <?php if($tipo == 'cambioestado'):?>
                            <?php echo __(" No ha sido posible cambiar el estado de la solicitud, por favor intentelo nuevamente. &nbsp;")?>
                            <i class="menu-icon fa fa-times-circle" style="color: red; font-size: x-large"></i>
                        <?php endif;?>
                        <?php if($tipo == 'firma'):?>
                            <?php echo __("No ha sido posible firmar la solicitud de " . $infosolicitud['nombretiposolicitud'] . ". &nbsp;")?>
                            <i class="menu-icon fa fa-times-circle" style="color: red; font-size: x-large"></i>
                        <?php endif;?>
                        <?php if($tipo == 'solicitudexistente'):?>
                            <?php echo __("No ha sido posible radicar la solicitud de " . $infosolicitud['nombretiposolicitud'] . ". &nbsp;")?>
                            <i class="menu-icon fa fa-times-circle" style="color: red; font-size: x-large"></i>
                        <?php endif;?>
                    </div>
                </div>
                <div class="row">
                    <div class="note note-danger">
                        <?php if($mensaje):?>
                            <?php echo __("Por favor comuniquese con el administrador del sistema. <br><br>")?>
                            <?php echo __("<b> Mensaje: </b>")?><?php echo __($mensaje . '<br>')?>
                            <?php echo __("<b> Código de error: </b>")?><?php echo __($idlog . " <br>")?>
                        <?php endif;?>
                        <?php if($tipo == 'solicitudexistente'):?>
                            <?php echo __("<b> Mensaje: </b>")?><?php echo __("Ya existe una solicitud de " . $infosolicitud['nombretiposolicitud'] . " que se encuentra en tramite"
                                    . " asociada al predio con Número Predial Nacional: " . $infosolicitud['codigounico'] . '<br>')?>
                        <?php endif;?>
                    </div>
                </div>
                <div class="row padding-sm text-right-sm">
                    <?php if($sf_user->getGuardUser()->hasPermission('ciudadano')):?>
                        <?php echo button_to('Regresar', 'estratificacion/crearSolicitud?codigotiposolicitud=' . $infosolicitud['codigotiposolicitud'], array('class'=>"btn btn-primary  boton_link"))?>
                    <?php endif;?>
                    <?php echo button_to('Mis Solicitudes', 'solicitud/index', array('class'=>"btn btn-primary  boton_link"))?>
                </div>
            </div>
        </div>
    </div>
</div>

