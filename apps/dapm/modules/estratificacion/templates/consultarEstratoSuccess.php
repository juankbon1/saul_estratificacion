<?php use_helper('I18N')?>  
<div class="col-md-7">    
    <div class="panel panel-success">
        <div class="panel-heading ">
            <span class="panel-title"><?php echo __("Consultar Estrato")?></span>
            <div class="panel-heading-controls">
                <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
            </div>
        </div>
        <div class="panel-body">
            <form action="<?php echo url_for('estratificacion/consultarEstrato')?>" method="post" name="busqueda_numero_predial" id="busqueda_numero_predial" class="">
                <div class="row padding-sm">
                    <?php echo $formubicacionpredio['tipozona']->renderRow()?>
                </div>

                <div id="ubicacionurbana">
                    <div class="row padding-sm">
                        <?php echo $formubicacionpredio['tipobusqueda']->renderRow()?>
                    </div>   
                    <div class="row dir_select" url="../solicitud/Direccion">
                        <?php echo $formubicacionpredio['direccion']->renderRow()?>
                    </div> 
                    <div class="row npn_prefix_sin_dir" url="../solicitud/NumpredialSinDireccion">
                        <?php echo $formubicacionpredio['numeropre_sin_dir']->renderRow()?>
                    </div>
                </div>

                <div id="ubicacionrural">
                    <div class="row npn_prefix_rural" url="../solicitud/NumpredialSinDireccion">
                        <?php echo $formubicacionpredio['numeropre_rural']->renderRow()?>
                    </div> 
                </div>

                <div class="row padding-sm text-right-sm">
                    <button id="consultar" type="submit" class="btn btn-primary" value="<?php echo __("Consultar estrato")?>">Consultar</button>
                </div>
            </form>
        </div>
    </div>    
</div>

<?php if(isset($infoestrato)):?>
    <?php if(count($infoestrato) > 1):?>
        <div class="col-md-7">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <span class="panel-title"><?php echo __("Información del predio")?></span>
                    <div class="panel-heading-controls">
                        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
                    </div>
                </div>
                <div class="panel-body">
                    <form method="post" name="estrato_especial" id="estrato_especial" action="<?php echo url_for('estratificacion/AsignarEstratoEspecial')?>">
                        <input type="hidden" name="estratonuevoespecial" id="estratonuevoespecial" value="">
                        <input type="hidden" name="npn" id="npn" value=<?php echo $infoestrato['codigounico']?>>
                        <div class="row padding-sm">
                            <div class="row">
                                <div class="note note-success">
                                    <?php echo __("<b> Número Predial Nacional: </b>")?><?php echo $infoestrato['codigounico'] . "  " . '<br>'?>
                                    <?php echo __("<b> Estrato del lado de manzana: </b>")?><?php echo $infoestrato['estrato'] . "  " . '<br>'?>
                                    <div id="estrato_especial_div">
                                        <?php echo __('<b id="nuevo_estrato_especial"> </b>')?>
                                    </div>
                                    <?php if($infoestrato['tipoatipicidad']):?>
                                        <?php echo __("<b> Tipo de atipicidad: </b>")?><?php echo '( ' . $infoestrato['tipoatipicidad'] . ' )<br>'?>
                                    <?php endif;?>
                                    <?php echo __("<b> CUM: </b>")?><?php echo $infoestrato['cum'] . '<br>'?>
                                    <?php echo __("<b> Comuna: </b>")?><?php echo $infoestrato['comuna'] . '<br>'?>
                                    <?php echo __("<b> Barrio: </b>")?><?php echo $infoestrato['barrio'] . '<br>'?>
                                    <?php echo __("<b> Manzana: </b>")?><?php echo $infoestrato['manzana'] . '<br>'?>
                                    <?php echo __("<b> Lado: </b>")?><?php echo $infoestrato['lado'] . '<br>'?>
                                    <?php if($infoestrato['isarray']):?>
                                        <?php foreach($infoestrato['direccion'] as $key=> $dir):?>
                                            <?php if($dir['Estado']['codigo'] == 'certificada'):?>
                                                <?php echo __("<b> Dirección " . ($key + 1) . ": </b>")?><?php echo $dir['direccion'] . '<font color="green"><b> ( ' . $dir['Estado']['nombre'] . ' ) </b></font><br>'?>
                                            <?php else:?>
                                                <?php echo __("<b> Dirección " . ($key + 1) . ": </b>")?><?php echo $dir['direccion'] . '<font color="red"><b> ( ' . $dir['Estado']['nombre'] . ' ) </b></font><br>'?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <?php if($infoestrato['direccion'] != ""):?>
                                            <?php if(($infoestrato['codigoestadonomenclatura'] == 'certificada') && ! ($infoestrato['rural'])):?>
                                                <?php echo __("<b> Dirección: </b>")?><?php echo $infoestrato['direccion'] . ' -  <font color="green"><b> ( ' . $infoestrato['nombreestadonomenclatura'] . ' ) </b></font><br>'?>
                                            <?php else:?>
                                                <?php echo __("<b> Dirección: </b>")?><?php echo $infoestrato['direccion'] . ' -  <font color="red"><b> ( ' . $infoestrato['nombreestadonomenclatura'] . ' ) </b></font><br>'?>
                                            <?php endif;?>
                                        <?php endif;?>
                                    <?php endif;?>
                                    <?php if(($infoestrato['direccioncatastro'] != "")):?>
                                        <?php echo __("<b> Dirección Catastro: </b>")?><?php echo $infoestrato['direccioncatastro'] . '<br>'?>
                                    <?php endif;?>
                                    <?php echo __("<b> Uso principal Catastro: </b>")?><?php echo $infoestrato['usocatastro'] . '<br>'?>
                                    <?php echo __("<b> Uso principal Emcali: </b>")?><?php echo $infoestrato['usoemcali'] . '<br>'?>
                                    <?php echo __("<b> Fecha de la consulta: </b>")?><?php echo $infoestrato['fecha'] . '<br>'?>
                                    <?php echo __("<br>")?>
                                    <?php if($infoestrato['tipoatipicidad']):?>
                                        <?php echo __('<font color="red"><b> NOTA: </b><font>')?><?php echo '<font color="red"><b> Este predio tiene atipicidad ( ' . $infoestrato['tipoatipicidad'] . ' ) </b></font><br>'?>
                                    <?php endif;?>
                                    <?php if($sf_user->getGuardUser()->hasPermission('asignarestratoespecial_estratificacion')):?>
                                        <?php echo '<br>'?>
                                        <div class="px">
                                            Asignar estrato especial &nbsp;&nbsp; <i class="fa fa-long-arrow-right"></i> &nbsp;&nbsp;
                                            <a href="#" id="nuevo-estrato" data-inputclass="mayusculas" data-type="text" data-pk="1" data-emptytext="Click para asignar estrato especial" data-placement="right" data-placeholder="Estrato especial" data-title="Ingrese el estrato especial" url="../../../../RecalcularEstrato"></a>
                                            <small class="help-block">Esta opción permite asignar el estrato a los predios que hacen parte de la metodología tipo 3 y la metodología especial.</small>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="row padding-sm text-right-sm">
                                <?php if($sf_user->isAuthenticated()):?>
                                    <?php if($sf_user->getGuardUser()->hasPermission('asignarestratoespecial_estratificacion')):?>
                                        <input type="submit" class="btn btn-primary cambiar_estrato" value="<?php echo __("Cambiar estrato")?>"/>
                                    <?php endif;?>
                                <?php endif;?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>    
        </div>
    <?php endif;?>
<?php endif;?>