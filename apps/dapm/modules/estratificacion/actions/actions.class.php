<?php

/**
 * estratificacion actions.
 *
 * @package    SAUL
 * @subpackage estratificacion
 * @author     juan.borrero@cali.gov.co
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class estratificacionActions extends sfActions
{

    /**
     * Función que ejecuta la accion index del módulo de estratificación
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        
    }

    /**
     * Función que busca un usuario por documento de identificación para desplegarlo por pantalla
     * @param sfWebRequest $request
     * @return type JSON
     */
    public function executeBuscarusuariounico(sfWebRequest $request)
    {
        $numd = $request->getParameter('q');

        $user = new sfGuardUser();
        $user = Doctrine::getTable('sfGuardUser')->findOneByNumidentificacion($numd);

        if( ! $user)
        {
            $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
            return $this->renderText(json_encode('No existe'));
        }
        else
        {
            $respuesta['num'] = $user->getNumidentificacion();
            $respuesta['name'] = $user->getFirstName();
            $respuesta['last_name'] = $user->getLastName();
            $respuesta['email'] = $user->getEmailAddress();
            $respuesta['telefono'] = $user->getTelefono();
            $respuesta['tipod'] = $user->getIdtipodocidentificacion();
            $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
            return $this->renderText(json_encode($respuesta));
        }
    }

    /**
     * Función que busca el número predial nacional de un predio para desplegarlo por pantalla
     * @param sfWebRequest $request
     * @return type JSON
     */
    public function executeNumpredial(sfWebRequest $request)
    {
        $predio = new Predio();
        $respuesta = $predio->ObtenerNumpredial($request);
        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que busca la dirección de un predio para desplegarlo por pantalla
     * @param sfWebRequest $request
     * @return type JSON
     */
    public function executeDireccion(sfWebRequest $request)
    {
        $predio = new Predio();
        $respuesta = $predio->ObtenerDireccion($request);
        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que busca un usuario para desplegarlo por pantalla
     * @param sfWebRequest $request
     * @return type JSON
     */
    public function executeBuscarUsuario(sfWebRequest $request)
    {
        $usuario = new Usuario();

        $respuesta = $usuario->ObtenerUsuario($request);
        print $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Funcion que busca el estrato de un predio y determina si tiene atipicidad
     * @param type $idpredio
     * @return type array
     */
    protected function consultarEstrato($idpredio)
    {
        $estestratopredio = new EstEstratopredio();
        $estratoatipica = $estestratopredio->obtenerEstratoAtipico($idpredio);

        if($estratoatipica)
        {
            $estrato = $estratoatipica[0]['estrato'];
            $tipoatipicidad = $estratoatipica[0]['tipoatipicidad'];
            $infoestrato = array('estrato'=>$estrato, 'tipoatipicidad'=>$tipoatipicidad);
            return $infoestrato;
        }
        else
        {
            $estestratopredio = $estestratopredio->obtenerEstrato($idpredio);

            if($estestratopredio)
            {
                $estrato = $estestratopredio;
                $tipoatipicidad = false;
                $infoestrato = array('estrato'=>$estrato, 'tipoatipicidad'=>$tipoatipicidad);
                return $infoestrato;
            }
            else
            {
                $estrato = false;
                $tipoatipicidad = false;
                $infoestrato = array('estrato'=>$estrato, 'tipoatipicidad'=>$tipoatipicidad);
                return $infoestrato;
            }
        }
    }

    /**
     * Función que despliega en pantalla el formulario para consultar el estrato de un predio
     * @param sfWebRequest $request
     */
    public function executeConsultarEstrato(sfWebRequest $request)
    {
        $this->formubicacionpredio = new UbicacionPredioForm();

        if($request->isMethod("post"))
        {
            $datospredio = array();

            if($request->getPostParameter('tipozona') == 'rural')
            {
                $datospredio = $request->getParameter('numeropre_rural');

                if($datospredio != "")
                {
                    $datospredio = explode('-', $datospredio);
                    $idpredio = $datospredio[0];
                    $direccion = "";
                }
            }
            else
            {
                $tipobusqueda = $request->getParameter('tipobusqueda');

                if($tipobusqueda == 'direccion')
                {
                    $datospredio = $request->getParameter('direccion');
                    $datospredio = explode('-', $datospredio);
                    $idpredio = $datospredio[0];

                    $idnomenclatura = $datospredio[1];
                    $nomenclaturapredio = Doctrine::getTable('Nomenclatura')->findOneByIdnomenclatura($idnomenclatura);
                    $direccion = $nomenclaturapredio->getDireccion();
                    $idestado = $nomenclaturapredio->getIdestado();
                    $codigoestadonomenclatura = Estado::obtenerCodigo($idestado);
                }
                else
                {
                    $datospredio = $request->getParameter('numeropre_sin_dir');
                    $idpredio = $datospredio;
                    $direccion = Nomenclatura::obtenerNomenclaturaxIdpredio($idpredio);

                    if($direccion)
                    {
                        $dirarray = true;
                        foreach($direccion as $key=> $value)
                        {
                            if($direccion[$key]['Estado']['codigo'] != Estado::CERTIFICADA)
                            {
                                $direccion[$key]['Estado']['codigo'] = Estado::POR_CERTIFICAR;
                                $direccion[$key]['Estado']['nombre'] = "Por Certificar";
                            }
                        }
                    }
                }
            }

            $infoestrato = $this->consultarEstrato($idpredio);

            $predio = Doctrine::getTable('Predio')->findOneByIdpredio($idpredio);
            $codigounico = $predio->getCodigounico();
            $cum = Predio::obtenerCum($idpredio);
            $usocatastro = $predio->getUsocatastro();
            $usoemcali = $predio->getUsoemcali();

            $estbdcatastral = Doctrine::getTable('Estbdcatastral')->findOneByNumeronal($predio->getCodigoUnico());

            if($estbdcatastral)
            {
                $direccioncatastro = $estbdcatastral->getDireccion();
            }
            else
            {
                $direccioncatastro = "";
            }

            $infoestrato['codigounico'] = $codigounico;
            $infoestrato['cum'] = $cum;
            $infoestrato['comuna'] = substr(trim($cum), 0, 2);
            $infoestrato['barrio'] = substr(trim($cum), 2, 2);
            $infoestrato['manzana'] = substr(trim($cum), 4, 4);
            $infoestrato['lado'] = substr(trim($cum), 8, 1);
            $infoestrato['direccion'] = $direccion;
            $infoestrato['direccioncatastro'] = $direccioncatastro;

            if(isset($codigoestadonomenclatura))
            {
                if($codigoestadonomenclatura != Estado::CERTIFICADA)
                {
                    $infoestrato['codigoestadonomenclatura'] = Estado::POR_CERTIFICAR;
                    $infoestrato['nombreestadonomenclatura'] = "Por Certificar";
                }
                else
                {
                    $infoestrato['codigoestadonomenclatura'] = Estado::CERTIFICADA;
                    $infoestrato['nombreestadonomenclatura'] = "Certificada";
                }
            }

            if(isset($dirarray))
            {
                $infoestrato['isarray'] = true;
            }
            else
            {
                $infoestrato['isarray'] = false;
            }

            $infoestrato['usocatastro'] = $usocatastro;
            $infoestrato['usoemcali'] = $usoemcali;
            $infoestrato['fecha'] = date("Y-m-d H:i:s");

            $this->infoestrato = $infoestrato;
        }
    }

    /**
     * Función que retorna la información asociada a una solicitud
     * @param type $solicitud
     * @return type array
     */
    protected function obtenerInfoSolicitud($solicitud)
    {
        $idsolicitud = $solicitud->getIdsolicitud();

        if($solicitud->getIdsolicitudpadre())
        {
            $solicitudpadre = Doctrine::getTable('solicitud')->findOneByIdsolicitud($solicitud->getIdsolicitudpadre());
            $numradicadopadre = $solicitudpadre->getRadicado();
        }
        else
        {
            $numradicadopadre = "";
        }

        if($solicitud->getRadicadohijo())
        {
            $numradicadohijo = $solicitud->getRadicadohijo();
        }
        else
        {
            $numradicadohijo = "";
        }

        $fechasolicitud = $solicitud->getCreatedAt();
        $fecharespuesta = $solicitud->getUpdatedAt();
        $comentario = $solicitud->getComentario();
        $observacion = $solicitud->getObservacion();
        $numradicado = $solicitud->getRadicado();
        $idsolicitante = $solicitud->getIdsolicitante();
        $idtiposolicitud = $solicitud->getIdtiposolicitud();

        $user = Doctrine::getTable('sfGuardUser')->findOneById($idsolicitante);
        $nombresolicitante = $user->getFirstName();
        $apellidosolicitante = $user->getLastName();
        $email = $user->getEmailAddress();
        $telefono = $user->getTelefono();
        $direccionsolicitante = $user->getDireccion();
        $numeroidentificacion = $user->getNumidentificacion();

        $tipodocidentificacion = Doctrine::getTable('tipodocidentificacion')->findOneByIdtipodocidentificacion($user->getIdtipodocidentificacion());
        $tipodocumento = $tipodocidentificacion->getNombre();

        if($user->getIdbarrio())
        {
            $barrio = Doctrine::getTable('Barrio')->findOneByIdbarrio($user->getIdbarrio());
            $nombrebarrio = $barrio->getNombre();
            $nombrevereda = "";
        }
        else
        {
            $vereda = Doctrine::getTable('Vereda')->findOneByIdvereda($user->getIdvereda());
            $nombrevereda = $vereda->getNombre();
            $nombrebarrio = "";
        }

        $tipoempresa = Doctrine::getTable('Tipoempresa')->findOneByIdtipoempresa($user->getIdtipoempresa());
        $nombretipoempresa = $tipoempresa->getNombre();

        $codigotiposolicitud = $solicitud->Tiposolicitud->codigo;
        $nombretiposolicitud = $solicitud->Tiposolicitud->nombre;

        $codigoestadosolicitud = $solicitud->Estado->codigo;
        $nombreestadosolicitud = $solicitud->Estado->nombre;

        $movimientosolicitud = new Movimientosolicitud();
        $movimiento = $movimientosolicitud->ultimoMovimiento($idsolicitud);

        if( ! $movimiento)
        {
            $nombreestadosolicitudanterior = false;
            $codigoestadosolicitudanterior = "";
        }
        else
        {
            $nombreestadosolicitudanterior = $movimiento[0]['estadoanterior'];
            $codigoestadosolicitudanterior = Estado::obtenerCodigoxNombre($nombreestadosolicitudanterior);
        }

        $subdireccion = Subdireccion::obtener($solicitud->Tiposolicitud->idsubdireccion);
        $nombresubdirecion = $subdireccion->getNombre();
        $idradicadororfeosubdirecion = $subdireccion->getIdradicadororfeo();
        $dependenciaorfeosubdireccion = $subdireccion->getDependenciaorfeo();

        $usuarioradicador = Doctrine::getTable('sfGuardUser')->findOneById($subdireccion->getIdusuario());
        $idsubdirector = $usuarioradicador->getId();
        $nombresubdirector = $usuarioradicador->getFirstName() . " " . $usuarioradicador->getLastName();
        $esactivo = $usuarioradicador->getIsActive();

        $predio = Doctrine::getTable('Predio')->findOneByIdpredio($solicitud->getIdpredio());
        $idpredio = $predio->getIdPredio();
        $codigounico = $predio->getCodigoUnico();
        $cum = Predio::obtenerCum($idpredio);
        $tipopredio = $predio->getTipo();

        if($solicitud->getIdnomenclatura())
        {
            $nomenclaturapredio = Doctrine::getTable('Nomenclatura')->findOneByIdnomenclatura($solicitud->getIdnomenclatura());
            $direccionpredio = $nomenclaturapredio->getDireccion();
            $idestado = $nomenclaturapredio->getIdestado();
            $codigoestadonomenclatura = Estado::obtenerCodigo($idestado);
            $nombreestadonomenclatura = $nomenclaturapredio->Estado->nombre;
        }
        else
        {
            $direccionpredio = "";
            $codigoestadonomenclatura = "";
            $nombreestadonomenclatura = "";
        }

        $estbdcatastral = Doctrine::getTable('Estbdcatastral')->findOneByNumeronal($predio->getCodigoUnico());

        if($estbdcatastral)
        {
            $direccioncatastro = $estbdcatastral->getDireccion();
        }
        else
        {
            $direccioncatastro = "";
        }

        $documento = Doctrine::getTable('Documento')->findOneByNombre('Notificación de la solicitud');

        $solicitudxdocumento = new Solicitudxdocumento();
        $solicitudxdocumento->setIdsolicitud($idsolicitud);
        $solicitudxdocumento->setIddocumento($documento->getIddocumento());
        $notificacion = $solicitudxdocumento->ValidaDocumentoExistente();

        $infoestrato = $this->consultarEstrato($solicitud->getIdpredio());
        $estrato = $infoestrato['estrato'];
        $tipoatipicidad = $infoestrato['tipoatipicidad'];

        $infosolicitud = array('idsolicitud'=>$idsolicitud,
            'fechasolicitud'=>$fechasolicitud,
            'fecharespuesta'=>$fecharespuesta,
            'comentario'=>$comentario,
            'observacion'=>$observacion,
            'numradicado'=>$numradicado,
            'numradicadohijo'=>$numradicadohijo,
            'numradicadopadre'=>$numradicadopadre,
            'nombresolicitante'=>$nombresolicitante,
            'apellidosolicitante'=>$apellidosolicitante,
            'email'=>$email,
            'telefono'=>$telefono,
            'direccionsolicitante'=>$direccionsolicitante,
            'numeroidentificacion'=>$numeroidentificacion,
            'tipodocumento'=>$tipodocumento,
            'nombrebarrio'=>$nombrebarrio,
            'nombrevereda'=>$nombrevereda,
            'nombretipoempresa'=>$nombretipoempresa,
            'codigotiposolicitud'=>$codigotiposolicitud,
            'nombretiposolicitud'=>$nombretiposolicitud,
            'codigounico'=>$codigounico,
            'cum'=>$cum,
            'tipopredio'=>$tipopredio,
            'direccionpredio'=>$direccionpredio,
            'direccioncatastro'=>$direccioncatastro,
            'codigoestadonomenclatura'=>$codigoestadonomenclatura,
            'nombreestadonomenclatura'=>$nombreestadonomenclatura,
            'estrato'=>$estrato,
            'tipoatipicidad'=>$tipoatipicidad,
            'codigoestadosolicitud'=>$codigoestadosolicitud,
            'nombreestadosolicitud'=>$nombreestadosolicitud,
            'codigoestadosolicitudanterior'=>$codigoestadosolicitudanterior,
            'nombreestadosolicitudanterior'=>$nombreestadosolicitudanterior,
            'nombresubdirecion'=>$nombresubdirecion,
            'idradicadororfeosubdirecion'=>$idradicadororfeosubdirecion,
            'idtiposolicitud'=>$idtiposolicitud,
            'dependenciaorfeosubdireccion'=>$dependenciaorfeosubdireccion,
            'idsubdirector'=>$idsubdirector,
            'nombresubdirector'=>$nombresubdirector,
            'esactivo'=>$esactivo,
            'notificacion'=>$notificacion);

        return $infosolicitud;
    }

    /**
     * Función que crea una nueva nomenclatura
     * @param sfWebRequest $request
     * @param type $idpredio
     * @return type string
     */
    private function crearNuevaNomenclatura(sfWebRequest $request, $idpredio)
    {
        $nomenclatura = new Nomenclatura();

        $nomenclatura->setIdpredio($idpredio);
        $nomenclatura->setDireccion($request->getParameter('direccionnueva'));
        $nomenclatura->setInformacionadicional($request->getParameter('direccionadicional'));
        $nomenclatura->save();

        return $nomenclatura->getIdnomenclatura();
    }

    /**
     * Función que crea un nuevo predio y retorna su id
     * @param sfWebRequest $request
     * @return type string
     */
    private function crearNuevoPredio(sfWebRequest $request)
    {
        $predio = new Predio();

        if($request->getPostParameter('tipozona') == 'urbano')
        {
            $predio->setCodigounico($request->getParameter('numeroprenuevo'));
            $predio->setTipo("Nuevo");
        }
        if($request->getPostParameter('tipozona') == 'rural')
        {
            $predio->setCodigounico($request->getParameter('numeroprenuevo_rural'));
            $predio->setLongitud($request->getParameter('longitud'));
            $predio->setLatitud($request->getParameter('latitud'));
            $predio->setTipo("Nuevo");
        }

        $predio->save();
        return $predio->getIdpredio();
    }

    /**
     * Función que actualiza la longitud y la latitud de un predio rural
     * @param sfWebRequest $request
     */
    private function actualizarPredio(sfWebRequest $request)
    {
        $predio = Doctrine::getTable('predio')->findOneByIdpredio($request->getParameter('numeropre_rural'));
        $predio->setLongitud($request->getParameter('longitud'));
        $predio->setLatitud($request->getParameter('latitud'));
        $predio->save();
    }

    /**
     * Función que registra un fallo en la tabla log
     * @param type $mensaje
     * @param type $idtiposolicitud
     * @param type $idusuario
     * @param type $idsolciitud
     * @param type $excepcion
     */
    private function registrarLogFallo($mensaje, $idtiposolicitud, $idusuario, $tipo, $excepcion, $idsolicitud = 0)
    {
        $log = new Log();

        if($excepcion)
        {
            $descripcion = $mensaje . ", Excepción: " . $excepcion;
        }
        else
        {
            $descripcion = $mensaje;
        }

        if($idsolicitud)
        {
            $log->registrar($descripcion, $this->getRequest()->getRemoteAddress(), $idusuario, $idsolicitud);
        }
        else
        {
            $log->registrar($descripcion, $this->getRequest()->getRemoteAddress(), $idusuario);
        }

        $idlog = $log->getIdlog();
        $this->redirect('estratificacion/mostrarMensajeFallo?mensaje=' . $mensaje . '&idtiposolicitud=' . $idtiposolicitud . '&idlog=' . $idlog . '&tipo=' . $tipo . '&idsolicitud=' . $idsolicitud);
    }

    /**
     * Función que crea una solicitud correspondiente a uno de los procesos del módulo de estratificación
     * @param sfWebRequest $request
     */
    public function executeCrearSolicitud(sfWebRequest $request)
    {
        if( ! $request->isMethod("post"))
        {
            $this->formsolicitud = new SolicitudEstratificacionForm();
            $idtiposolicitud = Tiposolicitud::obtenerId($request->getParameter('codigotiposolicitud'));
            $this->formsolicitud->setDefault('idtiposolicitud', $idtiposolicitud);
            $this->formdocumentos = new AdjuntarDocumentosEstratificacionForm();
            $this->formubicacionpredio = new UbicacionPredioForm();
        }
        if($request->isMethod("post"))
        {
            try
            {
                $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
                $conn->beginTransaction();

                $solicitud = new Solicitud();

                $solicitud->setFechainicial(date("Y-m-d H:i:s"));
                $solicitud->setFechafinal(date("Y-m-d H:i:s"));
                $solicitud->setIdestado(1);

                $solicitud->setIdtiposolicitud($request->getParameter('idtiposolicitud'));

                $usauriosolicitud = $this->getUser()->getGuardUser()->getId();
                $solicitud->setIdsolicitante($usauriosolicitud);
                $tiposolicitud = Doctrine::getTable('tiposolicitud')->find($request->getPostParameter('idtiposolicitud'));

                $idtiposolicitud = $tiposolicitud->getIdtiposolicitud();
                $codigotiposolicitud = $tiposolicitud->getCodigo();

                if($codigotiposolicitud == Tiposolicitud::APELACION_ESTRATO)
                {
                    $solicitudrevision = Doctrine::getTable('solicitud')->findOnebyRadicadohijo($request->getPostParameter('radicado_hijo_apelacion'));

                    $solicitud->setIdpredio($solicitudrevision->getIdpredio());
                    $solicitud->setIdnomenclatura($solicitudrevision->getIdnomenclatura());
                    $solicitud->setObservacion($request->getParameter('observacion'));
                    $solicitud->setIdsolicitudpadre($solicitudrevision->getIdsolicitud());
                    $solicitud->save();
                }
                else
                {
                    $tipobusqueda = '';
                    $idnomenclatura = '';
                    $idpredio = '';

                    if($request->getPostParameter('tipozona') == 'urbano')
                    {
                        if($request->getPostParameter('consistencia') == 'no')
                        {
                            $idpredio = Predio::obtenerPredioxNpn($request->getParameter('numeroprenuevo'));

                            if($idpredio)
                            {
                                $idnomenclaturaconsulta = Nomenclatura::obtenerNomenclaturaxDireccion($request->getParameter('direccionnueva'));

                                if($idnomenclaturaconsulta)
                                {
                                    $observacion = "La nomenclatura esta duplicada";
                                    $idnomenclatura = Nomenclatura::crearNuevaNomenclatura($request, $idpredio, $observacion);
                                }
                                else
                                {
                                    $idnomenclatura = Nomenclatura::crearNuevaNomenclatura($request, $idpredio);
                                }
                            }
                            else
                            {
                                $idnomenclaturaconsulta = Nomenclatura::obtenerNomenclaturaxDireccion($request->getParameter('direccionnueva'));

                                if($idnomenclaturaconsulta)
                                {
                                    $observacion = "La nomenclatura esta duplicada";
                                    $idpredio = Predio::crearNuevoPredio($request);
                                    $idnomenclatura = Nomenclatura::crearNuevaNomenclatura($request, $idpredio, $observacion);
                                }
                                else
                                {
                                    $idpredio = Predio::crearNuevoPredio($request);
                                    $idnomenclatura = Nomenclatura::crearNuevaNomenclatura($request, $idpredio);
                                }
                            }
                        }
                        else
                        {
                            $tipobusqueda = $request->getParameter('tipobusqueda');
                            $datospredio = array();

                            if($tipobusqueda == 'direccion')
                            {
                                $datospredio = $request->getParameter('direccion');
                            }
                            else
                            {
                                $datospredio = $request->getParameter('numeropre');
                            }

                            $datospredio = explode('-', $datospredio);
                            $idnomenclatura = $datospredio[1];
                            $idpredio = $datospredio[0];
                        }

                        $solicitud->setIdpredio($idpredio);
                        $solicitud->setIdnomenclatura($idnomenclatura);
                        $solicitud->setObservacion($request->getParameter('observacion'));
                    }
                    if($request->getPostParameter('tipozona') == 'rural')
                    {
                        if($request->getPostParameter('existe_npn') == 'si')
                        {
                            $datospredio = $request->getParameter('numeropre_rural');

                            if($datospredio != "")
                            {
                                $datospredio = explode('-', $datospredio);
                                $idpredio = $datospredio[0];
                                $solicitud->setIdpredio($idpredio);
                                $this->actualizarPredio($request);
                            }
                        }
                        else
                        {
                            $idpredio = $this->crearNuevoPredio($request);
                            $solicitud->setIdpredio($idpredio);
                        }

                        $solicitud->setComentario($request->getParameter('comentario'));
                        $solicitud->setObservacion($request->getParameter('observacion_rural'));
                    }

                    $respuesta = $this->validarSolicitudExistente($idpredio, $codigotiposolicitud);

                    if($respuesta['existe'])
                    {
                        $tipo = "solicitudexistente";
                        $this->redirect('estratificacion/mostrarMensajeFallo?mensaje=' . NULL . '&idlog=' . NULL . '&tipo=' . $tipo . '&idsolicitud=' . $respuesta['idsolicitud']);
                    }
                    else
                    {
                        $solicitud->save();
                    }
                }

                $ws = new OrfeoWS();
                $inforadicado = array("tipo"=>"creacion");
                $radicado = $this->radicarOrfeo($ws, $solicitud, $inforadicado);

                if($radicado != "")
                {
                    $conn->commit();

                    $solicitud->setRadicado($radicado);
                    $solicitud->actualizarRadicado();

                    $log = new Log();
                    $descripcion = 'Se crea la solicitud con id: ' . $solicitud->getIdsolicitud() .
                            ', de Tipo De Solicitud: ' . $solicitud->getTiposolicitud()->getNombre() .
                            ', del usuario: ' . $this->getUser()->getGuardUser()->getRazonsocial();
                    $log->registrar($descripcion, $request->getRemoteAddress(), $this->getUser()->getGuardUser()->getId(), $solicitud->getIdsolicitud());

                    $movimentosolicitud = new Movimientosolicitud();
                    $movimentosolicitud->registrar($solicitud, $this->getUser()->getGuardUser()->getId(), $descripcion);

                    if( ! ($this->subirDocumento($request, $solicitud)))
                    {
                        $estadoactual = Estado::PENDIENTE_POR_DOCS;
                        $this->actualizarEstado($solicitud, $estadoactual);
                    }
                    else
                    {
                        if( ! ($this->adjuntarDocumentosOrfeo($ws, $solicitud)))
                        {
                            $estadoactual = Estado::PENDIENTE_ANEXOS_ORFEO;
                            $this->actualizarEstado($solicitud, $estadoactual);
                        }
                    }
                    if( ! ($this->imprimirFormatos($solicitud, $ws, $solicitud->Estado->codigo)))
                    {
                        $estadoactual = Estado::PENDIENTE_PLANTILLA_ORFEO;
                        $this->actualizarEstado($solicitud, $estadoactual);
                    }

                    $mensaje = 'mensaje';
                    $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud());
                }
                else
                {
                    $conn->rollBack();
                    $mensaje = "No ha sido posible radicar la solicitud en el sistema Orfeo";
                    $this->registrarLogFallo($mensaje, $idtiposolicitud, $this->getUser()->getGuardUser()->getId(), 'radicacion', 0);
                }
            }
            catch(Exception $e)
            {
                $conn->rollBack();
                $mensaje = "No ha sido posible radicar la solicitud debido a que se ha producido una excepción del sistema";
                $this->registrarLogFallo($mensaje, $idtiposolicitud, $this->getUser()->getGuardUser()->getId(), 'radicacion', $e->getMessage());

                if(sfConfig::get('sf_logging_enabled'))
                {
                    sfContext::getInstance()->getLogger()->err($e->getMessage() . $e->getFile() . $e->getTraceAsString() . " Excepcion controlada en action Crearsolicitud del modulo Estratificacion");
                }
            }
        }
    }

    /**
     * Función que despliega en pantalla los mensajes de exito en alguno de los procesos de estratificacición
     * @param sfWebRequest $request
     */
    public function executeMostrarMensaje(sfWebRequest $request)
    {
        $mensaje = $request->getParameter('mensaje');

        if($request->getParameter('idsolicitud'))
        {
            $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getParameter('idsolicitud'))), sprintf('La solicitud no existe (%s).', $request->getParameter('idsolicitud')));
            $this->infosolicitud = $this->obtenerInfoSolicitud($solicitud);
        }

        switch($mensaje)
        {
            case 'radicado':
                $this->formespecial = new RespuestaEstratificacionEspecialForm();
                $this->setTemplate('mostrarMensajeRadicado');
                break;
            case 'mensaje':
                $this->setTemplate('mostrarMensaje');
                break;
            case 'mensajealternativo':
                $estestratopredio = Doctrine::getTable('estestratopredio')->findOneByIdestratopredio($request->getParameter('idestratopredio'));
                $this->infoestrato = $estestratopredio;
                $this->setTemplate('mostrarMensajeAlternativo');
                break;
            case 'docspendientes':
                $this->formdocumentos = new AdjuntarDocumentosEstratificacionForm();
                $this->setTemplate('subirDocumento');
                break;
            case 'editarradicadorespuesta':
                $this->estadoeditarradicadorespuesta = $request->getParameter('estadoeditarradicadorespuesta');
                $this->setTemplate('mostrarMensajeEditarRadicadoRespuesta');
                break;
            default:
                break;
        }
    }

    /**
     * Función que despliega en pantalla la informacción de un fallo en alguno de los procesos de estratificacición
     * @param sfWebRequest $request
     */
    public function executeMostrarMensajeFallo(sfWebRequest $request)
    {

        $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getParameter('idsolicitud'))), sprintf('La solicitud no existe (%s).', $request->getParameter('idsolicitud')));

        $infosolicitud = $this->obtenerInfoSolicitud($solicitud);

        $this->infosolicitud = $infosolicitud;

        $this->mensaje = $request->getParameter('mensaje');
        $this->idlog = $request->getParameter('idlog');
        $this->tipo = $request->getParameter('tipo');

        if($request->getParameter('tipo') == 'orfeo')
        {
            $this->setTemplate('mostrarMensajeFalloOrfeo');
        }
        else
        {
            if($this->tipo == "solicitudexistente")
            {
                $this->mensaje = false;
            }
            $this->setTemplate('mostrarMensajeFallo');
        }
    }

    /**
     * Función que despliega en pantalla la informacción de un fallo en la radicación de Orfeo para alguno de los procesos de estratificacición
     * @param sfWebRequest $request
     */
    public function executeMostrarMensajeFalloOrfeo(sfWebRequest $request)
    {
        $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getParameter('idsolicitud'))), sprintf('La solicitud no existe (%s).', $request->getParameter('idsolicitud')));
        $this->infosolicitud = $this->obtenerInfoSolicitud($solicitud);
        $this->setTemplate('mostrarMensajeFalloOrfeo');
    }

    /**
     * Función quer pemite adjuntar documentos pendientes por subir a Orfeo a través de un formulario de SAUL
     * @param sfWebRequest $request
     */
    public function executeSubirDocumento(sfWebRequest $request)
    {
        if( ! $request->isMethod("post"))
        {
            $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getParameter('idsolicitud'))), sprintf('La solicitud no existe (%s).', $request->getParameter('idsolicitud')));
            $infosolicitud = $this->obtenerInfoSolicitud($solicitud);
            $this->infosolicitud = $infosolicitud;
            $this->formdocumentos = new AdjuntarDocumentosEstratificacionForm();
        }

        if($request->isMethod("post"))
        {
            $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getParameter('idsolicitud'))), sprintf('La solicitud no existe (%s).', $request->getParameter('idsolicitud')));

            if($this->subirDocumento($request, $solicitud))
            {
                $ws = new OrfeoWS();

                if( ! ($this->adjuntarDocumentosOrfeo($ws, $solicitud)))
                {
                    $estadoactual = Estado::PENDIENTE_ANEXOS_ORFEO;
                    $this->actualizarEstado($solicitud, $estadoactual);
                    $this->redirect('estratificacion/mostrarMensajeFalloOrfeo?idsolicitud=' . $solicitud->getIdsolicitud());
                }
                else
                {
                    $estadoactual = Estado::PENDIENTE;
                    $this->actualizarEstado($solicitud, $estadoactual);

                    $mensaje = 'mensaje';
                    $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud());
                }
            }
            else
            {
                $this->redirect('estratificacion/mostrarMensajeFalloOrfeo?idsolicitud=' . $solicitud->getIdsolicitud());
            }
        }
    }

    /**
     * Función que sube al servidor un archivo y almacena en base de datos el registro del documento
     * @param sfWebRequest $request
     * @param Solicitud $solicitud
     * @return boolean
     */
    protected function subirDocumento(sfWebRequest $request, Solicitud $solicitud)
    {
        $archivo = $request->getFiles('documento');
        $retorno = "";

        $idsolicitud = $solicitud->getIdsolicitud();

        if(count($archivo) != 0)
        {
            foreach($archivo as $key=> $val)
            {
                if($archivo[$key]['size'] != 0)
                {
                    $extensionarchivo = substr($archivo[$key]['name'], -4);
                    $nombrearchivo = sha1(date("d-m-Y H:i:s") . $key) . $extensionarchivo;
                    $directoriotmp = $archivo[$key]['tmp_name'];
                    $directoriosubida = sfConfig::get("sf_dir_documentos");
                    $subirarchivo = move_uploaded_file($directoriotmp, $directoriosubida . $nombrearchivo);

                    $solicitudxdocumento = new Solicitudxdocumento();
                    $solicitudxdocumento->setIdsolicitud($solicitud->getIdsolicitud());

                    if($key == 'archivoservicios')
                    {
                        $nombre = "Comprobante numero predial nacional recibo servicios publicos";
                        $iddocumento = Documento::obtenerIddocumentoxNombre($nombre);
                        $solicitudxdocumento->setIddocumento($iddocumento);
                    }
                    if($key == 'archivopredial')
                    {
                        $nombre = "Comprobante numero predial nacional recibo predial";
                        $iddocumento = Documento::obtenerIddocumentoxNombre($nombre);
                        $solicitudxdocumento->setIddocumento($iddocumento);
                    }
                    if($key == 'archivoadicional')
                    {
                        $nombre = "Soporte adicional";
                        $iddocumento = Documento::obtenerIddocumentoxNombre($nombre);
                        $solicitudxdocumento->setIddocumento($iddocumento);
                    }
                    if($key == 'archivoapelacion')
                    {
                        $nombre = "Soporte de apelacion";
                        $iddocumento = Documento::obtenerIddocumentoxNombre($nombre);
                        $solicitudxdocumento->setIddocumento($iddocumento);
                    }
                    if($key == 'archivonotificacion')
                    {
                        $nombre = "Notificación de la solicitud";
                        $iddocumento = Documento::obtenerIddocumentoxNombre($nombre);
                        $solicitudxdocumento->setIddocumento($iddocumento);
                    }

                    $solicitudxdocumento->setArchivo($nombrearchivo);

                    if($subirarchivo)
                    {
                        $solicitudxdocumento->save();
                        $retorno = true;
                    }
                }
            }
        }

        if($retorno == true)
        {
            return $retorno;
        }
        else
        {
            return false;
        }
    }

    /**
     * Función que crea un radicado en Orfeo y retorna el numero de radicado
     * @param OrfeoWS $ws
     * @param Solicitud $solicitud
     * @param type $tipo
     * @return type string
     */
    protected function radicarOrfeo(OrfeoWS $ws, Solicitud $solicitud, $inforadicado)
    {
        $usuario = Doctrine_Core::getTable('SfGuardUser')->findOneById($solicitud->getIdsolicitante());
        $subdireccion = Subdireccion::obtener($solicitud->Tiposolicitud->idsubdireccion);

        if($inforadicado['tipo'] == "creacion")
        {
            $ws->establecerTipoRadicado(OrfeoWS::ENTRADA, $solicitud->getTiposolicitud()->getNombreorfeo());
            $ws->docIdentidadRemitente = $usuario->getNumidentificacion();
            $ws->nombreRemitente = $usuario->getFirst_name();
            $ws->apellidoRemitente = $usuario->getLast_name();
            $ws->emailRemitente = $usuario->getEmail_address();
            $ws->direccionRemitente = $usuario->getDireccion();
            $ws->telefonoRemitente = $usuario->getTelefono();
        }
        else
        {
            $ws->establecerRadicadoPadre($solicitud->radicado);
            $ws->establecerTipoRadicado(OrfeoWS::SALIDA, $solicitud->getTiposolicitud()->getNombreorfeo());
        }

        $ws->estableceIdRadicadorOrfeo($subdireccion->getIdradicadororfeo());
        $ws->estableceDepencienciaOrfeo($subdireccion->getDependenciaorfeo());
        $ws->estableceNumSolicitud($solicitud->getIdsolicitud());
        $ws->tipoDocumental = $solicitud->getTiposolicitud()->getTipodocumental();
        $ws->serie = $solicitud->getTiposolicitud()->getSeriedocumental();
        $ws->subserie = $solicitud->getTiposolicitud()->getSubseriedocumental();
        $ws->subdependencia = $solicitud->getTiposolicitud()->getSubdependencia();
        $ws->tipoSolicitud = $solicitud->getTiposolicitud()->getNombre();
        $ws->establecerParametrosWS();
        $ws->crearRadicado();

        return $ws->obtenerRadicado();
    }

    /**
     * Función que anexa uno o varios documentos a un radicado en Orfeo
     * @param OrfeoWS $ws
     * @param Solicitud $solicitud
     * @return boolean
     */
    protected function adjuntarDocumentosOrfeo(OrfeoWS $ws, Solicitud $solicitud)
    {
        $solicitudxdocumento = new Solicitudxdocumento();
        $solicitudxdocumento->setIdsolicitud($solicitud->getIdsolicitud());
        $idtiposolicitud = $solicitud->getIdtiposolicitud();

        try
        {
            $documentos = $solicitudxdocumento->traeDocumentos();

            $numdocumentos = sizeof($documentos);
            $numRadicado = $solicitud->getRadicado();

            if(($numdocumentos > 0) && ($numRadicado != ""))
            {
                $ws->establecerRadicado($numRadicado);
                $ws->numRadicado = $numRadicado;

                $nombre = "Solicitud inicial comprobante radicacion";
                $iddocumento = Documento::obtenerIddocumentoxNombre($nombre);

                foreach($documentos as $key=> $val)
                {
                    if($iddocumento == $val['iddocumento'])
                    {
                        unset($documentos[$key]);
                    }
                }

                if($numdocumentos == 1)
                {
                    $documentos[] = array('archivo'=>'',
                        'Documento'=>array('nombre'=>'',
                            'descripcion'=>''));
                }

                $resultado = $ws->adjuntarDocumentosv2($documentos);
                $resultado = json_decode($resultado, true);

                $anexos = $resultado['anexos'];

                $contdoc = 0;

                if($resultado['radicado'])
                {
                    foreach($anexos as $anex)
                    {
                        if($documentos[$contdoc]["archivo"] != "")
                        {
                            $iddocumento = $documentos[$contdoc]['iddocumento'];
                            $codigoorfeo = $anex['codigo'];
                            $solicitudxdocumento->actualizarCodigoOrfeo($codigoorfeo, $iddocumento);
                            $solicitudxdocumento->setEstado(Solicitudxdocumento::PROCESADO);
                            $solicitudxdocumento->actualizarEstadoSolicitudxDocumento($iddocumento);
                            $contdoc ++;
                        }
                    }

                    return true;
                }
                else
                {

                    return false;
                }
            }
            else
            {
                false;
            }
        }
        catch(Exception $e)
        {
            $mensaje = "No ha sido posible adjuntar los documentos en Orfeo debido a que se ha producido una excepción del sistema (adjuntarDocumentosOrfeo)";
            $this->registrarLogFallo($mensaje, $idtiposolicitud, $this->getUser()->getGuardUser()->getId(), '', $solicitud->getIdsolicitud(), $e->getMessage());

            if(sfConfig::get('sf_logging_enabled'))
            {
                sfContext::getInstance()->getLogger()->err($e->getMessage() . $e->getFile() . $e->getTraceAsString() . " Excepcion controlada en action Crearsolicitud del modulo Estratificacion");
            }
        }
    }

    /**
     * Retorna al javascript el resultado de la validación que permite identificar si hay una solicitud en proceso para el predio y del tipo seleccionado
     * @param sfWebRequest $request
     * @return type JSON
     */
    public function executeVerificarSolicitudExistente(sfWebRequest $request)
    {
        $datospredio = $request->getParameter('datospredio');
        $datospredio = explode('-', $datospredio);
        $idpredio = $datospredio[0];

        $codigotiposolicitud = $request->getParameter('codigotiposolicitud');
        $respuesta = $this->validarSolicitudExistente($idpredio, $codigotiposolicitud);
        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta["existe"]));
    }

    /**
     * Función que valida que no exista una solicitud de estratificacion en proceso para el predio y del tipo seleccionado
     * @param type $idpredio
     * @param type $codigotiposolicitud
     * @return boolean
     */
    private function validarSolicitudExistente($idpredio, $codigotiposolicitud)
    {
        $tiposolicitud = Doctrine::getTable('TipoSolicitud')->findOneByCodigo($codigotiposolicitud);
        $idtiposolicitud = $tiposolicitud->getIdtiposolicitud();
        $solicitud = Solicitud::consultarxIdpredioIdtiposolicitud($idpredio, $idtiposolicitud);
        $idsolicitud = "";

        if($solicitud != NULL)
        {
            $idsolicitud = $solicitud[0]['idsolicitud'];
            $idestado = $solicitud[0]['idestado'];
            $estado = Doctrine::getTable('Estado')->findOneByIdestado($idestado);
            $codigoestado = $estado->getCodigo();

            $idtiposolicitudconsulta = $solicitud[0]['idtiposolicitud'];
            $tiposolicitudconsulta = Doctrine::getTable('TipoSolicitud')->findOneByIdtiposolicitud($idtiposolicitudconsulta);
            $codigotiposolicitudconsulta = $tiposolicitudconsulta->getCodigo();

            switch($codigotiposolicitud)
            {
                case Tiposolicitud::CERTIFICADO_ESTRATO:
                    if($codigotiposolicitudconsulta == $codigotiposolicitud):
                        if($codigoestado != Estado::EMITIDO): $existe = true;
                        else: $existe = false;
                        endif;
                    else: return false;
                    endif;
                    break;
                case Tiposolicitud::REVISION_ESTRATO:
                    if($codigotiposolicitudconsulta == $codigotiposolicitud):
                        if($codigoestado != Estado::EMITIDO): $existe = true;
                        else: $existe = false;
                        endif;
                    else: $existe = false;
                    endif;
                    break;
                case Tiposolicitud::APELACION_ESTRATO:
                    if($codigoestado != Estado::EMITIDO): $existe = true;
                    elseif($codigotiposolicitudconsulta != Tiposolicitud::REVISION_ESTRATO): $existe = true;
                    else: return false;
                    endif;
                    break;
                default :
                    break;
            }
        }
        else
        {
            if($codigotiposolicitud == Tiposolicitud::APELACION_ESTRATO): return true;
            else: return false;
            endif;
        }

        $respuesta = array("idsolicitud"=>$idsolicitud, "existe"=>$existe);
        return $respuesta;
    }

    /**
     * Retorna al javascript el resultado de la validación que permite identificar si existe un predio
     * @param sfWebRequest $request
     * @return type
     */
    public function executeVerificarNpnExistente(sfWebRequest $request)
    {
        $npn = $request->getParameter('npn');

        $respuesta = $this->validarNpndExistente($npn);
        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que valida que no exista el predio especificado en el NPN
     * @param type $npn
     * @return boolean
     */
    private function validarNpndExistente($npn)
    {
        $predio = new Predio();
        $codigounico = $predio->obtenerPredioxNpn($npn);

        if($codigounico)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Retorna al javascript el resultado de la validación de la solicitud de apelación
     * @param sfWebRequest $request
     * @return type JSON
     */
    public function executeIdPredioApelacion(sfWebRequest $request)
    {
        $respuesta = $this->validarSolicitudApelacion($request->getParameter('idpredio'));
        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Valida si existe una solicitud de revisión de estrato previa asociada al predio al cual se le va a radicar la solicitud de apelación 
     * @param type $idpredio
     * @return boolean
     */
    private function validarSolicitudApelacion($idpredio)
    {
        $solicitud = Solicitud::consultarxIdpredioIdtiposolicitud($idpredio, Tiposolicitud::obtenerId(Tiposolicitud::REVISION_ESTRATO));

        if($solicitud != NULL)
        {
            $idestado = $solicitud[0]['idestado'];
            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::EMITIDO)
            {
                $fechaactual = date("Y-m-d");
                $fechasolicitud = substr($solicitud[0]['created_at'], 0, -9);
                $datetime1 = date_create($fechasolicitud);
                $datetime2 = date_create($fechaactual);
                $interval = date_diff($datetime1, $datetime2);
                $dias = $interval->format('%a');

                if($dias < 76)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "false";
        }
    }

    /**
     * Función que valida el estado de una solicitud
     * @param sfWebRequest $request
     */
    public function executeValidarestado(sfWebRequest $request)
    {
        $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getParameter('idsolicitud'))), sprintf('La solicitud no existe (%s).', $request->getParameter('idsolicitud')));

        if(false)
        {
            $this->info = 'Existen solicitudes con fechas anteriores y estas se deben atender en orden de llegada, por favor verifique';
            $this->setTemplate('mensajeinfo', 'solicitud');
        }
        else
        {
            switch($solicitud->Estado->codigo)
            {
                case Estado::PENDIENTE:
                    $mensaje = 'radicado';
                    $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud());
                    break;
                case Estado::PENDIENTE_POR_VISITA:
                    $mensaje = 'radicado';
                    $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud());
                    break;
                case Estado::VISITADA:
                    $mensaje = 'radicado';
                    $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud());
                    break;
                case Estado::PENDIENTE_POR_DOCS:
                    $mensaje = 'docspendientes';
                    $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud());
                    break;
                case Estado::PENDIENTE_PLANTILLA_ORFEO:
                    $ws = new OrfeoWS();

                    if($solicitud->radicadohijo == NULL)
                    {
                        $ws->numRadicado = $solicitud->radicado;

                        if(file_exists(sfConfig::get("sf_dir_documentos") . $solicitud->getRadicado() . "_comprobante.pdf"))
                        {
                            $resultado = $this->cargarPlantillaOrfeo($ws, $solicitud);
                        }
                        else
                        {
                            $resultado = $this->imprimirFormatos($solicitud, $ws, $solicitud->Estado->codigo);
                        }
                    }
                    else
                    {
                        $ws->numRadicado = $solicitud->radicadohijo;
                        $resultado = $this->imprimirFormatos($solicitud, $ws, $solicitud->Estado->codigo);
                    }

                    if($resultado)
                    {
                        $documentos = $this->validarDocumentos($solicitud);

                        if($documentos && $solicitud->radicadohijo == NULL)
                        {
                            $estadoactual = Estado::PENDIENTE;
                        }
                        elseif($solicitud->radicadohijo != NULL)
                        {
                            $estadoactual = Estado::EMITIDO;
                            $ws->establecerRadicado($solicitud->getRadicado());
                            $ws->cerrarRadicado();
                        }
                        else
                        {
                            $estadoactual = Estado::PENDIENTE_POR_DOCS;
                        }

                        $this->actualizarEstado($solicitud, $estadoactual);

                        $mensaje = 'mensaje';
                        $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud());
                    }
                    else
                    {
                        $this->redirect('estratificacion/mostrarMensajeFalloOrfeo?idsolicitud=' . $solicitud->getIdsolicitud());
                    }
                    break;
                case Estado::PENDIENTE_ANEXOS_ORFEO:
                    $documento = Doctrine::getTable('Documento')->findOneByNombre('Notificación de la solicitud');

                    $solicitudxdocumento = new Solicitudxdocumento();
                    $solicitudxdocumento->setIdsolicitud($solicitud->getIdsolicitud());
                    $solicitudxdocumento->setIddocumento($documento->getIddocumento());
                    $regsolicitudxdocumento = $solicitudxdocumento->traeDocumentosxid();

                    $estadonotificacion = $regsolicitudxdocumento['estado'];

                    $ws = new OrfeoWS();

                    if($this->adjuntarDocumentosOrfeo($ws, $solicitud))
                    {
                        if($estadonotificacion == 'pendiente')
                        {
                            $estadoactual = Estado::EMITIDO;
                        }
                        else
                        {
                            $estadoactual = Estado::PENDIENTE;
                        }

                        $this->actualizarEstado($solicitud, $estadoactual);

                        $mensaje = 'mensaje';
                        $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud());
                    }
                    else
                    {
                        $this->redirect('estratificacion/mostrarMensajeFalloOrfeo?idsolicitud=' . $solicitud->getIdsolicitud());
                    }
                    break;
                default :
                    break;
            }
        }
    }

    /**
     * Función que valida si los documentos de la solicitud existen en el servidor
     * @param Solicitud $solicitud
     * @return boolean3
     */
    protected function validarDocumentos(Solicitud $solicitud)
    {
        $tiposolicitud = $solicitud->Tiposolicitud->codigo;
        $idsolicitud = $solicitud->getIdsolicitud();

        $solicitudxdocumento = new Solicitudxdocumento();
        $solicitudxdocumento->setIdsolicitud($idsolicitud);

        switch($tiposolicitud)
        {
            case Tiposolicitud::CERTIFICADO_ESTRATO:
                $documento = "Comprobante numero predial nacional recibo servicios publicos";
                $iddocumento = Documento::obtenerIddocumentoxNombre($documento);
                $solicitudxdocumento->setIddocumento($iddocumento);
                $documentos = $solicitudxdocumento->traeDocumentosxid();
                break;

            case Tiposolicitud::REVISION_ESTRATO:
                $documento = "Comprobante numero predial nacional recibo servicios publicos";
                $iddocumento = Documento::obtenerIddocumentoxNombre($documento);
                $solicitudxdocumento->setIddocumento($iddocumento);
                $documentos = $solicitudxdocumento->traeDocumentosxid();
                break;

            case Tiposolicitud::APELACION_ESTRATO:
                $documento = "Soporte de apelacion";
                $iddocumento = Documento::obtenerIddocumentoxNombre($documento);
                $solicitudxdocumento->setIddocumento($iddocumento);
                $documentos = $solicitudxdocumento->traeDocumentosxid();
                break;

            default:
                break;
        }

        if($documentos)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Función que imprime todos los formatos de estratificación para los tres tramites, los certificados se generan con firma digital
     * @param Solicitud $solicitud
     * @param OrfeoWS $ws
     * @param type $estadoactual
     * @return boolean
     */
    private function imprimirFormatos(Solicitud $solicitud, OrfeoWS $ws, $estadoactual)
    {
        $estadosolicitud = Doctrine_Core::getTable('estado')->findOneBy('codigo', $estadoactual);
        $codigoestado = $estadosolicitud->codigo;
        $infosolicitud = $this->obtenerInfoSolicitud($solicitud);

        $idcorregimiento = $this->getRequest()->getParameter('idcorregimiento');

        if($infosolicitud['esactivo'])
        {
            $firmadigital = Doctrine_Core::getTable('firmadigital')->findOneBy('sf_guard_user_id', $infosolicitud['idsubdirector']);
            $firmasubdirector = $firmadigital->getFirmaMecanica();
            $passwordfirma = $firmadigital->getPassword();
            $salt = $firmadigital->getCreatedAt();
            $codificarcadena = new Codificarcadena;
            $password = $codificarcadena->desencriptar($passwordfirma, $salt);
        }

        $marcoinferior = (((strlen($infosolicitud['observacion'])) / 115) * 4);
        $nombreestrato = array(1=>'UNO', 2=>'DOS', 3=>'TRES', 4=>'CUATRO', 5=>'CINCO', 6=>'SEIS');

        switch($infosolicitud['codigotiposolicitud'])
        {
            case Tiposolicitud::CERTIFICADO_ESTRATO:

                if(($codigoestado == Estado::PENDIENTE) || (($codigoestado == Estado::PENDIENTE_PLANTILLA_ORFEO) && ($solicitud->getRadicadohijo() == NULL)))
                {
                    $pdf = new PlantillaSolicitudEstratificacion();
                    $pdf->nombretramite = "SOLICITUD DE CERTIFICADO DE ESTRATO";
                    $pdf->tiposolicitud = Tiposolicitud::CERTIFICADO_ESTRATO;
                    $pdf->radicado = $solicitud->radicado;
                    $pdf->establecerNombreArchivo($solicitud->radicado . '_comprobante.pdf');
                    $pdf->establecerFechaSolicitud($infosolicitud['fechasolicitud']);

                    $ws->archivo = $pdf->archivo;
                }

                if(($codigoestado == Estado::EMITIDO) || (($codigoestado == Estado::PENDIENTE_PLANTILLA_ORFEO) && ($solicitud->getRadicadohijo() != NULL)))
                {
                    $pdf = new PlantillaRespuetaCertificadoEstrato();
                    $pdf->nombretramite = "CERTIFICADO DE ESTRATIFICACIÓN SOCIOECONÓMICA";
                    $pdf->firmaMecanica = $firmasubdirector;
                    $pdf->nombreSubdirector = $infosolicitud['nombresubdirector'];
                    $pdf->infoSubdirector = $infosolicitud['nombresubdirecion'];
                    $pdf->radicadoPadre = $solicitud->radicado;
                    $pdf->radicado = $solicitud->radicadohijo;
                    $pdf->establecerFechaRespuesta($infosolicitud['fecharespuesta']);

                    if($idcorregimiento)
                    {
                        $corregimiento = Doctrine_Core::getTable('corregimiento')->findOneBy('idcorregimiento', $idcorregimiento);
                        $nombrecorregimiento = $corregimiento->getNombre();
                        $pdf->nombreCorregimiento = ucwords(strtolower($nombrecorregimiento));
                    }
                    else
                    {
                        $pdf->nombreCorregimiento = false;
                    }

                    $archivo = $pdf->archivo;
                }
                break;
            case Tiposolicitud::REVISION_ESTRATO:
                if(($codigoestado == Estado::PENDIENTE) || ($codigoestado == Estado::PENDIENTE_PLANTILLA_ORFEO))
                {
                    $pdf = new PlantillaSolicitudEstratificacion();
                    $pdf->nombretramite = "SOLICITUD DE REVISIÓN DE ESTRATO";
                    $pdf->tiposolicitud = Tiposolicitud::REVISION_ESTRATO;
                    $pdf->radicado = $solicitud->radicado;
                    $pdf->instancia = "primera";
                    $pdf->enteresponsable = "la Alcaldía";
                    $pdf->establecerNombreArchivo($solicitud->radicado . '_comprobante.pdf');
                    $pdf->establecerFechaSolicitud($infosolicitud['fechasolicitud']);
                    $ws->archivo = $pdf->archivo;
                }
                break;
            case Tiposolicitud::APELACION_ESTRATO:
                if(($codigoestado == Estado::PENDIENTE) || ($codigoestado == Estado::PENDIENTE_PLANTILLA_ORFEO))
                {
                    $pdf = new PlantillaSolicitudEstratificacion();
                    $pdf->nombretramite = "SOLICITUD DE APELACIÓN";
                    $pdf->tiposolicitud = Tiposolicitud::APELACION_ESTRATO;
                    $pdf->radicado = $solicitud->radicado;
                    $pdf->instancia = "segunda";
                    $pdf->enteresponsable = "el Comite permanente de estratificación";
                    $pdf->establecerNombreArchivo($solicitud->radicado . '_comprobante.pdf');
                    $pdf->establecerFechaSolicitud($infosolicitud['fechasolicitud']);
                    $ws->archivo = $pdf->archivo;
                }
                break;
            default :
                break;
        }

        if(($infosolicitud['codigotiposolicitud'] == Tiposolicitud::CERTIFICADO_ESTRATO) || ($infosolicitud['codigotiposolicitud'] == Tiposolicitud::REVISION_ESTRATO) || ($infosolicitud['codigotiposolicitud'] == Tiposolicitud::APELACION_ESTRATO))
        {
            if(($codigoestado == Estado::PENDIENTE) || (($codigoestado == Estado::PENDIENTE_PLANTILLA_ORFEO) && ($solicitud->getRadicadohijo() == NULL)))
            {
                $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
                $conn->beginTransaction();

                $solicitudxdocumento = new Solicitudxdocumento();
                $solicitudxdocumento->setIdsolicitud($solicitud->getIdsolicitud());

                $nombre = "Solicitud inicial comprobante radicacion";
                $iddocumento = Documento::obtenerIddocumentoxNombre($nombre);

                $solicitudxdocumento->setIddocumento($iddocumento);
                $solicitudxdocumento->setArchivo($pdf->archivo);
            }
        }

        $posiciontitulo = (93 - strlen($pdf->nombretramite));

        $pdf->solicitud = $infosolicitud['idsolicitud'];
        $pdf->nombreapellidosolicitante = $infosolicitud['nombresolicitante'] . " " . $infosolicitud['apellidosolicitante'];
        $pdf->email = $infosolicitud['email'];
        $pdf->telefono = $infosolicitud['telefono'];
        $pdf->direccionsolicitante = $infosolicitud['direccionsolicitante'];
        $pdf->barrio = $infosolicitud['nombrebarrio'];
        $pdf->documento = $infosolicitud['numeroidentificacion'];
        $pdf->tipoempresa = $infosolicitud['nombretipoempresa'];
        $pdf->nombretiposolicitud = $infosolicitud['nombretiposolicitud'];
        $pdf->posiciontitulo = $posiciontitulo;
        $pdf->comentario = $infosolicitud['comentario'];
        $pdf->observacion = $infosolicitud['observacion'];
        $pdf->marcoinferior = $marcoinferior;
        $pdf->numerounico = $infosolicitud['codigounico'];
        $pdf->direccionpredio = $infosolicitud['direccionpredio'];
        $pdf->tipoatipicidad = $infosolicitud['tipoatipicidad'];

        if(($this->getRequest()->getParameter('cumnuevo') != "") && ($this->getRequest()->getParameter('estratonuevo') != ""))
        {
            $pdf->cum = $this->getRequest()->getParameter('cumnuevo');
            $pdf->estrato = $this->getRequest()->getParameter('estratonuevo');
            $pdf->estratonuevo = $this->getRequest()->getParameter('estratonuevo');
            $pdf->nombreestrato = $nombreestrato[$this->getRequest()->getParameter('estratonuevo')];
            $pdf->direccion1 = $this->getRequest()->getParameter('direccion1');
            $pdf->direccion2 = $this->getRequest()->getParameter('direccion2');
            $pdf->sector = $this->getRequest()->getParameter('sector');
            $pdf->proyecto = $this->getRequest()->getParameter('proyecto');
        }
        else
        {
            $pdf->estratonuevo = false;
            $pdf->cum = $infosolicitud['cum'];

            if($infosolicitud['estrato'])
            {
                $idsolicitudpadre = Doctrine_Core::getTable('solicitud')->findOneBy('idsolicitudpadre', $solicitud->getIdsolicitud());

                if($idsolicitudpadre)
                {
                    $pdf->estrato = false;
                }
                else
                {
                    $pdf->estrato = $infosolicitud['estrato'];
                    $pdf->nombreestrato = $nombreestrato[$infosolicitud['estrato']];
                }
            }
            else
            {
                $pdf->estrato = false;
            }
        }

        $pdf->dependencia = $infosolicitud['dependenciaorfeosubdireccion'];
        $pdf->subdependencia = $solicitud->getTiposolicitud()->getSubdependencia();
        $pdf->serie = $solicitud->getTiposolicitud()->getSeriedocumental();
        $pdf->subserie = $solicitud->getTiposolicitud()->getSubseriedocumental();
        $pdf->tipoDocumental = $solicitud->getTiposolicitud()->getTipodocumental();
        $pdf->consecutivo = OrfeoWS::obtenerConsecutivoOrfeo($solicitud->radicado);
        $ws->extensionArchivo = 'pdf';

        $pdf->parametrizarPdf();
        $pdf->obtenerPlantilla();

        if(($infosolicitud['codigotiposolicitud'] == Tiposolicitud::CERTIFICADO_ESTRATO) && (($codigoestado == Estado::EMITIDO) || (($codigoestado == Estado::PENDIENTE_PLANTILLA_ORFEO) && ($solicitud->getRadicadohijo() != NULL))))
        {
            // Descomentar para utilizar la firma digital
//            $usuariofirma = Doctrine_Core::getTable('sfGuardUser')->findOneById($infosolicitud['idsubdirector']);
            // Descomentar para utilizar la firma digital
//            $documentofirmado = $this->firmarDocumento($password, $archivo, $usuariofirma, $infosolicitud['idsolicitud']);
            // Comentar para utilizar la firma digital
            $documentofirmado = true;

            if($documentofirmado)
            {
                // Descomentar para utilizar la firma digital
//                $ws->archivo = $documentofirmado;
                // Comentar para utilizar la firma digital
                $ws->archivo = $archivo;

                if($ws->cargarPlantilla())
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if($ws->cargarPlantilla())
            {
                $solicitudxdocumento->setEstado(Estado::PROCESADO);
                $solicitudxdocumento->save();
                $conn->commit();
                return true;
            }
            else
            {
                if(file_exists(sfConfig::get("sf_dir_documentos") . $solicitud->getRadicado() . '_comprobante.pdf'))
                {
                    $solicitudxdocumento->setEstado(Estado::PENDIENTE);
                    $solicitudxdocumento->save();
                    $conn->commit();
                    return false;
                }
                else
                {
                    $conn->rollBack();
                    return false;
                }
            }
        }
    }

    /**
     * Función que firma digitalmente el documento y retorna el documento firmado
     * @param type $password
     * @param type $archivo
     * @param type $usuariofirma
     * @param type $idsolicitud
     * @return boolean|string
     */
    protected function firmarDocumento($password, $archivo, $usuariofirma, $idsolicitud)
    {
        $firma = new firmaDigital();
        $resultadofirma = $firma->firmarSOAP($this->getRequest(), $password, $archivo, $usuariofirma, $idsolicitud);

        if($resultadofirma['resultado'])
        {
            $file = explode('.', $archivo);
            $nombre_archivo = $file[0];
            $ext_archivo = "." . $file[1];
            $documentofirmado = $nombre_archivo . "_firmado" . $ext_archivo;
            return $documentofirmado;
        }
        else
        {
            return false;
        }
    }

    /**
     * Función que gestiona dentro del flujo de trabajo el estado de una solicitud de certificado de estrato
     * @param Solicitud $solicitud
     * @param sfGuardUser $usuariosolicitud
     */
    public function gestionarEstadoCertificadoEstrato(Solicitud $solicitud, sfGuardUser $usuariosolicitud)
    {
        $infosolicitud = $this->obtenerInfoSolicitud($solicitud);

        $firmadigital = Doctrine_Core::getTable('firmadigital')->findOneBy('sf_guard_user_id', $infosolicitud['idsubdirector']);
        $firmasubdirector = $firmadigital->getFirmaMecanica();
        $passwordfirma = $firmadigital->getPassword();
        $salt = $firmadigital->getCreatedAt();
        $codificarcadena = new Codificarcadena;
        $password = $codificarcadena->desencriptar($passwordfirma, $salt);
        $codigotiposolicitud = $solicitud->Tiposolicitud->codigo;
        $idtiposolicitud = $solicitud->Tiposolicitud->idtiposolicitud;
        $estadoanterior = $solicitud->Estado->codigo;

        $usuariofirma = Doctrine_Core::getTable('sfGuardUser')->findOneById($infosolicitud['idsubdirector']);
        $parametros = Doctrine_Core::getTable('firmadigital')->findOneBy('sf_guard_user_id', $usuariofirma->getId());
        $user = $parametros->getUsuario();
        
        // Descomentar para utilizar la firma digital
//        $wsdl = Parametro::obtener("url_ws_firmadigital");
//        $clientOptions = array("login"=>$user, "password"=>$password, "trace"=>1, 'cache_wsdl'=>WSDL_CACHE_NONE);
//        $exepcionactiva = false;
//
//        try
//        {
//            $client = new SoapClient($wsdl, $clientOptions);
//        }
//        catch(SoapFault $e)
//        {
//            $excepcion = $e->getMessage();
//            $exepcionactiva = true;
//        }

        if( ! $infosolicitud['esactivo'])
        {
            $mensaje = "El usuario que firma se encuentra desactivado";
            $this->registrarLogFallo($mensaje, $idtiposolicitud, $this->getUser()->getGuardUser()->getId(), 'firma', 0, $solicitud->getIdsolicitud());
        }
        // Descomentar para utilizar la firma digital
//        elseif($exepcionactiva)
//        {
//            $mensaje = "El servicio de firma digital se encuentra fuera de servicio";
//            $this->registrarLogFallo($mensaje, $idtiposolicitud, $this->getUser()->getGuardUser()->getId(), 'firma', $excepcion, $solicitud->getIdsolicitud());
//        }
        else
        {
            $ws = new OrfeoWS();
            $inforadicado = array("tipo"=>"respuesta");
            $nuevoradicado = $this->radicarOrfeo($ws, $solicitud, $inforadicado);

            if($nuevoradicado != "")
            {
                $solicitud->setRadicadohijo($nuevoradicado);
                $solicitud->save();

                if( ! $this->imprimirFormatos($solicitud, $ws, Estado::EMITIDO))
                {
                    $estadoactual = Estado::PENDIENTE_PLANTILLA_ORFEO;
                    $this->actualizarEstado($solicitud, $estadoactual);
                    $mensaje = "Radicado de orfeo sin plantilla";
                    $this->registrarLogFallo($mensaje, $idtiposolicitud, $this->getUser()->getGuardUser()->getId(), 'orfeo', 0, $solicitud->getIdsolicitud());
                }
                else
                {
                    $estadoactual = Estado::EMITIDO;
                    $this->actualizarEstado($solicitud, $estadoactual);
                    $ws->establecerRadicado($solicitud->getRadicado());
                    $ws->cerrarRadicado();

                    if(($this->getRequest()->getParameter('cumnuevo') != "") && ($this->getRequest()->getParameter('estratonuevo') != ""))
                    {
                        $this->registrarLog('gestionarestadocertificadoestrato', $solicitud);
                    }

                    $mensaje = 'mensaje';
                    $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud());
                }
            }
            else
            {
                $mensaje = "El servicio de orfeo fallo";
                $this->registrarLogFallo($mensaje, $idtiposolicitud, $this->getUser()->getGuardUser()->getId(), 'orfeo', 0, $solicitud->getIdsolicitud());
            }
        }
    }

    /**
     * Función que gestiona dentro del flujo de trabajo el estado de una solicitud de revisión de estrato
     * @param type $solicitud
     * @param type $usuariosolicitud
     * @param type $estadoanterior
     */
    public function gestionarEstadoRevisionEstrato($solicitud, $usuariosolicitud, $estadoanterior)
    {
        if(($estadoanterior == Estado::PENDIENTE) || ($solicitud->Estado->codigo == Estado::PENDIENTE_POR_VISITA))
        {
            if($estadoanterior == Estado::PENDIENTE)
            {
                $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::PENDIENTE_POR_VISITA);
                $estadoactual = $estado->codigo;
                $idestado = $estado->idestado;
                $template = 'mensaje';
            }
            if($solicitud->Estado->codigo == Estado::PENDIENTE_POR_VISITA)
            {
                $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::VISITADA);
                $estadoactual = $estado->codigo;
                $idestado = $estado->idestado;
                $template = 'mensaje';
            }
            if(isset($template))
            {
                $solicitud->setIdEstado($idestado);
                $solicitud->setComentario("");
                $solicitud->actualizarEstado();

                $log = new Log();
                $descripcion = 'Se cambió el estado de ' . $estadoanterior . ' a ' . $estadoactual . ', en la solicitud No. ' . $solicitud->getIdsolicitud() .
                        ', de Tipo De Solicitud: ' . $solicitud->getTiposolicitud()->getNombre() .
                        ', del usuario: ' . $usuariosolicitud->getRazonsocial() .
                        '.Realizado por:  ' . $this->getUser()->getGuardUser()->getFirstName() . ' ' . $this->getUser()->getGuardUser()->getLastName();

                $log->registrar($descripcion, $this->getRequest()->getRemoteAddress(), $usuariosolicitud->getId(), $solicitud->getIdsolicitud());

                $movimientosolicitud = new Movimientosolicitud();
                $movimientosolicitud->registrar($solicitud, $this->getUser()->getGuardUser()->getId(), $descripcion, $estadoanterior, $estadoactual);
                $infoestado = array('template'=>$template, 'estadoactual'=>$estadoactual, 'estadoanterior'=>$estadoanterior,);

                $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $infoestado['template'] . '&idsolicitud=' . $solicitud->getIdsolicitud());
            }
        }
        elseif($estadoanterior == Estado::VISITADA)
        {
            $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::EMITIDO);
            $estadoactual = $estado->codigo;
            $idestado = $estado->idestado;
            $solicitud->setIdEstado($idestado);
            $solicitud->setComentario("");
            $solicitud->actualizarEstado();

            $log = new Log();
            $descripcion = 'Se cambió el estado de ' . $estadoanterior . ' a ' . $estadoactual . ', en la solicitud No. ' . $solicitud->getIdsolicitud() .
                    ', de Tipo De Solicitud: ' . $solicitud->getTiposolicitud()->getNombre() .
                    ', del usuario: ' . $usuariosolicitud->getRazonsocial() .
                    '.Realizado por:  ' . $this->getUser()->getGuardUser()->getFirstName() . ' ' . $this->getUser()->getGuardUser()->getLastName();

            $log->registrar($descripcion, $this->getRequest()->getRemoteAddress(), $usuariosolicitud->getId(), $solicitud->getIdsolicitud());

            $movimientosolicitud = new Movimientosolicitud();
            $movimientosolicitud->registrar($solicitud, $this->getUser()->getGuardUser()->getId(), $descripcion, $estadoanterior, $estadoactual);
            $template = 'mensaje';
            $infoestado = array('template'=>$template, 'estadoactual'=>$estadoactual, 'estadoanterior'=>$estadoanterior);

            $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $infoestado['template'] . '&idsolicitud=' . $solicitud->getIdsolicitud());
        }
        elseif($estadoanterior == Estado::PENDIENTE_ASOCIAR_RADICADO_RESPUESTA)
        {
            $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::EMITIDO);
            $estadoactual = $estado->codigo;
            $idestado = $estado->idestado;
            $solicitud->setIdEstado($idestado);
            $solicitud->setComentario("");
            $solicitud->actualizarEstado();

            $log = new Log();
            $descripcion = 'Se cambió el estado de ' . $estadoanterior . ' a ' . $estadoactual . ', en la solicitud No. ' . $solicitud->getIdsolicitud() .
                    ', de Tipo De Solicitud: ' . $solicitud->getTiposolicitud()->getNombre() .
                    ', del usuario: ' . $usuariosolicitud->getRazonsocial() .
                    '.Realizado por:  ' . $this->getUser()->getGuardUser()->getFirstName() . ' ' . $this->getUser()->getGuardUser()->getLastName();

            $log->registrar($descripcion, $this->getRequest()->getRemoteAddress(), $usuariosolicitud->getId(), $solicitud->getIdsolicitud());

            $movimientosolicitud = new Movimientosolicitud();
            $movimientosolicitud->registrar($solicitud, $this->getUser()->getGuardUser()->getId(), $descripcion, $estadoanterior, $estadoactual);
            $template = 'mensaje';
            $infoestado = array('template'=>$template, 'estadoactual'=>$estadoactual, 'estadoanterior'=>$estadoanterior);

            $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $infoestado['template'] . '&idsolicitud=' . $solicitud->getIdsolicitud());
        }
        else
        {
            $mensaje = "No fue posible cambiar el estado de la solicitud";
            $this->registrarLogFallo($mensaje, $solicitud->getIdtiposolicitud(), $this->getUser()->getGuardUser()->getId(), 'cambioestado', 0, $solicitud->getIdsolicitud());
        }
    }

    /**
     * Función que gestiona dentro del flujo de trabajo el estado de una solicitud de apelación
     * @param type $solicitud
     * @param type $usuariosolicitud
     * @param type $estadoanterior
     */
    public function gestionarEstadoApelacion($solicitud, $usuariosolicitud, $estadoanterior)
    {
        if($estadoanterior == Estado::PENDIENTE)
        {
            $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::EMITIDO);
            $estadoactual = $estado->codigo;
            $idestado = $estado->idestado;
            $solicitud->setIdEstado($idestado);
            $solicitud->setComentario("");
            $solicitud->actualizarEstado();

            $log = new Log();
            $descripcion = 'Se cambió el estado de ' . $estadoanterior . ' a ' . $estadoactual . ', en la solicitud No. ' . $solicitud->getIdsolicitud() .
                    ', de Tipo De Solicitud: ' . $solicitud->getTiposolicitud()->getNombre() .
                    ', del usuario: ' . $usuariosolicitud->getRazonsocial() .
                    '.Realizado por:  ' . $this->getUser()->getGuardUser()->getFirstName() . ' ' . $this->getUser()->getGuardUser()->getLastName();

            $log->registrar($descripcion, $this->getRequest()->getRemoteAddress(), $usuariosolicitud->getId(), $solicitud->getIdsolicitud());

            $movimientosolicitud = new Movimientosolicitud();
            $movimientosolicitud->registrar($solicitud, $this->getUser()->getGuardUser()->getId(), $descripcion, $estadoanterior, $estadoactual);
            $template = 'mensaje';
            $infoestado = array('template'=>$template, 'estadoactual'=>$estadoactual, 'estadoanterior'=>$estadoanterior);

            $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $infoestado['template'] . '&idsolicitud=' . $solicitud->getIdsolicitud());
        }
        else
        {
            $mensaje = "No fue posible cambiar el estado de la solicitud";
            $this->registrarLogFallo($mensaje, $solicitud->getIdtiposolicitud(), $this->getUser()->getGuardUser()->getId(), 'cambioesatado', 0, $solicitud->getIdsolicitud());
        }
    }

    /**
     * Función que actualiza el estado de una solicitud en general
     * @param sfWebRequest $request
     */
    public function executeCambiarEstado(sfWebRequest $request)
    {
        if( ! $request->isMethod('post'))
        {
            $this->redirect('solicitud/index');
        }
        else
        {
            $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getPostParameter('idsolicitud'))), sprintf('La Solicitud no existe (%s).', $request->getPostParameter('idsolicitud')));

            $usuariosolicitud = Doctrine_Core::getTable('SfGuardUser')->findOneById($solicitud->getIdsolicitante());
            $codigotiposolicitud = $solicitud->Tiposolicitud->codigo;
            $estadoanterior = $solicitud->Estado->codigo;

            switch($codigotiposolicitud)
            {
                case Tiposolicitud::CERTIFICADO_ESTRATO:
                    $this->gestionarEstadoCertificadoEstrato($solicitud, $usuariosolicitud);
                    break;
                case Tiposolicitud::REVISION_ESTRATO:
                    $radicadohijo = $request->getParameter('radicadohijo');

                    if($radicadohijo != "")
                    {
                        $solicitud->setRadicadohijo($radicadohijo);
                        $solicitud->save();
                    }

                    $this->gestionarEstadoRevisionEstrato($solicitud, $usuariosolicitud, $estadoanterior);
                    break;
                case Tiposolicitud::APELACION_ESTRATO:
                    $radicadohijo = $request->getParameter('radicadohijo');

                    if($radicadohijo != NULL)
                    {
                        $solicitud->setRadicadohijo($radicadohijo);
                        $solicitud->save();
                    }

                    $this->gestionarEstadoApelacion($solicitud, $usuariosolicitud, $estadoanterior);
                    break;
                default :
                    break;
            }
        }
    }

    /**
     * Función que genera una solicitud de revisión a partir de una solicitud de certificado de estrato
     * @param sfWebRequest $request
     */
    public function executeGenerarSolicitudRevision(sfWebRequest $request)
    {
        $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getParameter('idsolicitud'))), sprintf('La Solicitud no existe (%s).', $request->getPostParameter('idsolicitud')));
        $usuariosolicitud = Doctrine_Core::getTable('SfGuardUser')->findOneById($solicitud->getIdsolicitante());

        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();

        if($solicitud->Estado->codigo == Estado::PENDIENTE)
        {
            $solicitudhijo = new Solicitud();
            $solicitudhijo->setIdsolicitudpadre($solicitud->getIdsolicitud());
            $solicitudhijo->setFechainicial(date("Y-m-d H:i:s"));
            $solicitudhijo->setFechafinal(date("Y-m-d H:i:s"));
            $solicitudhijo->setIdestado(1);
            $tiposolicitud = Doctrine_Core::getTable('tiposolicitud')->findOneBy('codigo', Tiposolicitud::REVISION_ESTRATO);
            $solicitudhijo->setIdtiposolicitud($tiposolicitud->idtiposolicitud);
            $solicitudhijo->setIdsolicitante($solicitud->getIdsolicitante());
            $solicitudhijo->setComentario($solicitud->getComentario());
            $solicitudhijo->setObservacion($solicitud->getObservacion());
            $solicitudhijo->setIdpredio($solicitud->getIdpredio());
            $solicitudhijo->setIdnomenclatura($solicitud->getIdnomenclatura());
            $solicitudhijo->save();

            try
            {
                $ws = new OrfeoWS();
                $inforadicado = array("tipo"=>"creacion");
                $radicado = $this->radicarOrfeo($ws, $solicitudhijo, $inforadicado);

                if($radicado != "")
                {
                    $solicitudhijo->setRadicado($radicado);

                    if( ! $this->imprimirFormatos($solicitudhijo, $ws, $solicitudhijo->Estado->codigo))
                    {
                        $conn->rollBack();
                        $mensaje = "";
                        $this->registrarLogFallo($mensaje, $solicitud->getIdtiposolicitud(), $usuariosolicitud->getId(), 'radicacion', 0);
                    }
                    else
                    {
                        $solicitudhijo->actualizarRadicado();
                        $conn->commit();

                        $log = new Log();
                        $descripcion = 'Se crea la solicitud con id: ' . $solicitudhijo->getIdsolicitud() .
                                ', de Tipo De Solicitud: ' . $solicitudhijo->getTiposolicitud()->getNombre() .
                                ', del usuario: ' . $usuariosolicitud->getRazonsocial();
                        $log->registrar($descripcion, $request->getRemoteAddress(), $usuariosolicitud->getId(), $solicitudhijo->getIdsolicitud());

                        $movimentosolicitud = new Movimientosolicitud();
                        $movimentosolicitud->registrar($solicitudhijo, $usuariosolicitud->getId(), $descripcion);

                        $this->gestionarEstadoCertificadoEstrato($solicitud, $usuariosolicitud);
                        $this->redirect('estratificacion/mostrarMensaje?mensaje=mensaje&idsolicitud=' . $solicitudhijo->getIdsolicitud());
                    }
                }
                else
                {
                    $conn->rollBack();
                    $mensaje = "";
                    $this->registrarLogFallo($infoestado['mensaje'], $solicitud->getIdtiposolicitud(), $usuariosolicitud->getId(), 'radicacion', 0);
                }
            }
            catch(Exception $e)
            {
                $conn->rollBack();
                $mensaje = "No ha sido posible radicar la solicitud debido a que se ha producido una excepción del sistema";
                $this->registrarLogFallo($mensaje, $solicitud->getIdtiposolicitud(), $solicitud->getIdsolicitante(), 'radicacion', $e->getMessage());

                if(sfConfig::get('sf_logging_enabled'))
                {
                    sfContext::getInstance()->getLogger()->err($e->getMessage() . $e->getFile() . $e->getTraceAsString() . " Excepcion controlada en action Crearsolicitud del modulo Estratificacion");
                }
            }
        }
    }

    /**
     * Función que actualiza el estado de una solicitud en general,no se permite el cambio del estado de una solicitud si esta 
     * se encuentra en Aprobada o Denegada
     * @param sfWebRequest $request
     */
    public function executeAsociarRadicadoRespuesta(sfWebRequest $request)
    {
        
    }

    /**
     * Función que carga la plantilla en Orfeo
     * @param OrfeoWS $ws
     * @param Solicitud $solicitud
     * @return boolean
     */
    protected function cargarPlantillaOrfeo(OrfeoWS $ws, Solicitud $solicitud)
    {
        $ws->archivo = $solicitud->getRadicado() . '_comprobante.pdf';
        $ws->numRadicado = $solicitud->radicado;
        $ws->extensionArchivo = 'pdf';

        if($ws->cargarPlantilla())
        {
            $solicitudxdocumento = new Solicitudxdocumento();
            $solicitudxdocumento->setEstado(Estado::PROCESADO);
            $solicitudxdocumento->setIdsolicitud($solicitud->getIdsolicitud());

            $nombre = "Solicitud inicial comprobante radicacion";
            $iddocumento = Documento::obtenerIddocumentoxNombre($nombre);

            $solicitudxdocumento->actualizarEstadoSolicitudxDocumento($iddocumento);
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Función que actualiza el estado de una solicitud, del movimientosolicitud y registra en el log
     * @param Solicitud $solicitud
     * @param type $estadoactual
     */
    protected function actualizarEstado(Solicitud $solicitud, $estadoactual)
    {
        $log = new Log();
        $estadoanterior = $solicitud->getEstado()->getCodigo();
        $descripcion = 'Se cambió el estado de ' . $solicitud->getEstado()->getCodigo() . ' a ' . $estadoactual . ', en la solicitud No. ' . $solicitud->getIdsolicitud() .
                ', de Tipo De Solicitud: ' . $solicitud->getTiposolicitud()->getNombre() .
                ', del usuario: ' . $this->getUser()->getGuardUser()->getFirstName() . ' ' . $this->getUser()->getGuardUser()->getLastName() .
                '.Realizado por el sistema de forma automatica';
        $log->registrar($descripcion, $this->getRequest()->getRemoteAddress(), $this->getUser()->getGuardUser()->getId(), $solicitud->getIdsolicitud());

        $movimientosolicitud = new Movimientosolicitud();
        $movimientosolicitud->registrar($solicitud, $this->getUser()->getGuardUser()->getId(), $descripcion, $estadoanterior, $estadoactual);

        $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', $estadoactual);
        $idestado = $estado->idestado;

        $solicitud->setIdEstado($idestado);
        $solicitud->setComentario("");
        $solicitud->actualizarEstado();
    }

    /**
     * Función que registra en el log cambios sensibles en la base de datos
     * @param Solicitud $solicitud
     */
    protected function registrarLog($funcion, Solicitud $solicitud)
    {
        $log = new Log();

        switch($funcion)
        {
            case 'gestionarestadocertificadoestrato':
                $descripcion = 'Se genera el certificado especial de zona de expansión con estrato ' . $this->getRequest()->getParameter('estratonuevo') . ' '
                        . 'en la solicitud No. ' . $solicitud->getIdsolicitud() . ', identificada con radicado de respuesta ' . $solicitud->getRadicadohijo() . ', '
                        . 'para el predio con NPN ' . $solicitud->getPredio()->getCodigounico() . ''
                        . ', este cambio fue realizado por el usuario ' . $this->getUser()->getGuardUser()->getFirstName() . ' ' . $this->getUser()->getGuardUser()->getLastName();
                break;
            case 'actualizarradicadohijo':
                $descripcion = 'Se cambió el radicado de respuesta ' . $this->getRequest()->getParameter('numradicadohijo') .
                        ' por el nuevo radicado de respuesta ' . $this->getRequest()->getParameter('radicado_hijo') .
                        ', en la solicitud No. ' . $solicitud->getIdsolicitud() .
                        ', de Tipo De Solicitud: ' . $solicitud->getTiposolicitud()->getNombre() .
                        ', por el usuario: ' . $this->getUser()->getGuardUser()->getFirstName() . ' ' . $this->getUser()->getGuardUser()->getLastName();
                break;
            default :
                break;
        }

        $log->registrar($descripcion, $this->getRequest()->getRemoteAddress(), $this->getUser()->getGuardUser()->getId(), $solicitud->getIdsolicitud());
    }

    /**
     * Función que despliega en pantalla el formulario para consultar el radicado padre de una solicitud
     * @param sfWebRequest $request
     */
    public function executeEditarRadicadoRespuesta(sfWebRequest $request)
    {
        $this->formsolicitud = new SolicitudEstratificacionForm();

        if($request->isMethod("post"))
        {
            $solicitud = new Solicitud();
            $idsolicitud = $solicitud->obtenerIdsolicitudxRadicado($request->getParameter('radicado'));
            $solicitud = $this->editarRadicadoRespuesta($idsolicitud);

            $isobject = is_object($solicitud);

            if(is_object($solicitud))
            {
                $infosolicitud = $this->obtenerInfoSolicitud($solicitud);
                $this->infosolicitud = $infosolicitud;
            }
            else
            {
                $this->infosolicitud = $solicitud;
            }
        }
    }

    /**
     * Funcion que valida si el redicado padre es de un certificado y tiene radicado de respusta
     * @param type $idsolicitud
     * @return string, objeto Solicitud
     */
    protected function editarRadicadoRespuesta($idsolicitud)
    {
        $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($idsolicitud)), sprintf('La Solicitud no existe (%s).', $idsolicitud));

        if($solicitud->Tiposolicitud->codigo == Tiposolicitud::CERTIFICADO_ESTRATO)
        {
            return "certificado";
        }
        else
        {
            if($solicitud->getRadicadohijo() == "")
            {
                return "sinradicadohijo";
            }
            else
            {
                return $solicitud;
            }
        }
    }

    /**
     * Funcion que actualiza el radicado hijo (redicado de respuesta) de una solicitud y despliega el mensaje correspondiente al usuario
     * @param sfWebRequest $request
     * @return type redirecciona
     */
    public function executeActualizarRadicadoHijo(sfWebRequest $request)
    {
        if($request->isMethod("post"))
        {
            $respuesta = $this->actualizarRadicadoHijo($request);
            $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getParameter('idsolicitud'))), sprintf('La Solicitud no existe (%s).', $request->getPostParameter('idsolicitud')));
        }

        if($respuesta)
        {
            $mensaje = 'editarradicadorespuesta';
            $estado = 'true';
        }
        else
        {
            $mensaje = 'editarradicadorespuesta';
            $estado = 'false';
        }

        return $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $solicitud->getIdsolicitud() . '&estadoeditarradicadorespuesta=' . $estado);
    }

    /**
     * Funcion que actualiza el radicado hijo (redicado de respuesta) de una solicitud
     * @param sfWebRequest $request
     * @return boolean
     */
    protected function actualizarRadicadoHijo(sfWebRequest $request)
    {
        $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($request->getParameter('idsolicitud'))), sprintf('La Solicitud no existe (%s).', $request->getPostParameter('idsolicitud')));

        $solicitud->setRadicadohijo($request->getParameter('radicado_hijo'));
        $solicitud->save();

        if($solicitud->getRadicadohijo() == $request->getParameter('radicado_hijo'))
        {
            $this->registrarLog('actualizarradicadohijo', $solicitud);
            return true;
        }
    }

    /**
     * Funcion que retorna al javascript el CUM y el estrato nuevo que fue recalculado
     * @param sfWebRequest $request
     * @return type JSON
     */
    public function executeRecalcularEstrato(sfWebRequest $request)
    {
        $respuesta = $this->recalcularEstrato($request->getParameter('cum'));
        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Funcion que recalcula el estrato a partir de la comuna, barrio, manzana y lado del CUM sobre la tabla est_es y lo retorna
     * @param type $cum
     * @return type Array
     */
    private function recalcularEstrato($cum)
    {
        $comuna = ltrim(substr($cum, 0, 2), "0");
        $barrio = ltrim(substr($cum, 2, 2), "0");
        $manzana = ltrim(substr($cum, 4, 4), "0");
        $lado = substr($cum, 8, 1);

        $estes = new EstEs();
        $estrato = $estes->obtenerEstratoxCum($comuna, $barrio, $manzana, $lado);
        $respuesta = array('cum'=>$cum, 'estrato'=>$estrato);

        return $respuesta;
    }

    /**
     * Funcion que asigna el estrato especial a los predios que pertenencen a la metodología tipo 3 y metodlogía especial
     * @param sfWebRequest $request
     * @return type redirecciona
     */
    public function executeAsignarEstratoEspecial(sfWebRequest $request)
    {
        $idestratopredio = $this->asignarEstratoEspecial($request->getParameter('estratonuevoespecial'), $request->getParameter('npn'));

        if($idestratopredio)
        {
            $mensaje = 'mensajealternativo';
            return $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idestratopredio=' . $idestratopredio);
        }
    }

    /**
     * Funcion que asigna el estrato especial a los predios que pertenencen a la metodología tipo 3 y metodlogía especial
     * @param type $estratonuevoespecial
     * @param type $npn
     * @return type int
     */
    private function asignarEstratoEspecial($estratonuevoespecial, $npn)
    {
        $estestratopredio = new EstEstratopredio();
        $idpredio = Predio::obtenerPredioxNpn($npn);

        if($idpredio)
        {
            $estratoactual = EstEstratopredio::obtenerEstrato($idpredio);
        }

        if($estratoactual)
        {
            $estestratopredio->setEstratoanterior($estratoactual);
            $descestratoanterior = ', cuyo estrato anterior era: ' . $estratoactual;
            EstEstratopredio::actualizarActiva($idpredio);
        }
        else
        {
            $estestratopredio->setEstratoanterior(NULL);
            $descestratoanterior = '';
        }

        $estestratopredio->setEstratoactual($estratonuevoespecial);
        $estestratopredio->setActiva(1);
        $estestratopredio->setIdpredio($idpredio);
        $estestratopredio->save();

        $log = new Log();

        $descripcion = 'Se asigna el estrato ' . $estratonuevoespecial . ' para el predio identificado con NPN: ' . $npn . ' ' . $descestratoanterior
                . ', este cambio fue realizado por el usuario ' . $this->getUser()->getGuardUser()->getFirstName() . ' ' . $this->getUser()->getGuardUser()->getLastName();

        $log->registrar($descripcion, $this->getRequest()->getRemoteAddress(), $this->getUser()->getGuardUser()->getId());

        return $estestratopredio->getIdestratopredio();
    }

    /**
     * Función que redirecciona a la función actualizarEstrato
     */
    public function executeActualizarEstrato()
    {
        $this->setTemplate('actualizarEstrato');
    }

    /**
     * Función que elimina los registros de las tablas "est_bdcatastral", "est_es_anterior" y "est_es", esta función hace parte del 
     * proceso de actualización de la información de estratificación para actualizar estratos
     * @return type JSON boolean
     */
    public function executeElimiarRegistrosEstrato()
    {
        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ESTRATO_MASIVO);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ELIMINAR_REGISTROS);
        $idestadosubproceso = $estadosubproceso->idestado;

        EstControl::eliminarRegistrosEstrato();
        $idflujo = EstControl::obtenerIdFlujo();

        if($idflujo)
        {
            $idestado = EstControl::obtenerEstadoEstrato($idestadoproceso, $idestadosubproceso, $idflujo);
            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::FALLIDO)
            {
                $idestado = false;
            }
        }
        else
        {
            $idestado = false;
        }

        if($idestado)
        {
            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::COMPLETADA)
            {
                $respuesta = true;
            }
        }
        else
        {
            $respuesta = false;
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que valida la existencia de por lo menos un registro en las tablas "est_bdcatastral", "est_es_anterior" y "est_es", esta 
     * función hace parte del proceso de actualización de la información de estratificación para actualizar estratos
     * @return type JSON array
     */
    public function executeValidarRegistrosEstrato()
    {
        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ESTRATO_MASIVO);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::VALIDAR_REGISTROS);
        $idestadosubproceso = $estadosubproceso->idestado;

        $registros = EstControl::comprobarRegistrosEstrato();
        $respuesta = array();

        foreach($registros as $key=> $val)
        {
            if($val > 0)
            {
                $respuesta[$key] = $val;
                $respuesta['estado'] = true;
            }
            else
            {
                $respuesta['estado'] = false;
                break;
            }
        }

        if($respuesta['estado'])
        {
            $estcontrol = new EstControl();
            $estcontrol->setIdflujo($respuesta['idflujo']);
            $estcontrol->setIdestadoproceso($idestadoproceso);
            $estcontrol->setIdestadosubproceso($idestadosubproceso);
            $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::COMPLETADA);
            $estcontrol->setIdestado($estado->idestado);
            $estcontrol->setMensaje("Se realizo la validacion de registros con exito");
            $estcontrol->save();
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que depura los registros de las tablas "est_bdcatastral", "est_es_anterior" y "est_es" eliminando espacios en blanco, caracteres
     * extraños, etc, esta función hace parte del proceso de actualización de la información de estratificación para actualizar estratos
     * @return type JSON boolean
     */
    public function executeDepurarRegistrosEstrato()
    {
        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ESTRATO_MASIVO);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::DEPURAR_REGISTROS);
        $idestadosubproceso = $estadosubproceso->idestado;

        EstControl::depurarRegistrosEstrato();
        $idflujo = EstControl::obtenerIdFlujo();

        if($idflujo)
        {
            $idestado = EstControl::obtenerEstadoEstrato($idestadoproceso, $idestadosubproceso, $idflujo);
            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::FALLIDO)
            {
                $idestado = false;
            }
        }
        else
        {
            $idestado = false;
        }


        if($idestado)
        {
            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::COMPLETADA)
            {
                $respuesta = true;
            }
        }
        else
        {
            $respuesta = false;
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que actualiza la bandera (campo) "actualizacionestato" de la tabla "predio" para todos aquellos registros que hayan sufrido 
     * cambios en su información catastral con respecto a la tabla "est_bdcatastral", esta función hace parte del proceso de actualización 
     * de la información de estratificación para actualizar estratos
     * @return type JSON array
     */
    public function executeActualizarPrediosEstrato(sfWebRequest $request)
    {
        $codigoestadosubproceso = $request->getParameter('codigoestadosubproceso');
        $respuesta = array();

        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ESTRATO_MASIVO);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', $codigoestadosubproceso);
        $idestadosubproceso = $estadosubproceso->idestado;

        $fecha = date('Y-m-d H:i:s');
        $fechaevento = strtotime('+10 second', strtotime($fecha));
        $fechaevento = date('Y-m-d H:i:s', $fechaevento);

        switch($codigoestadosubproceso)
        {
            case Estado::ACTUALIZAR_PREDIOS:
                EstControl::actualizarPrediosEstrato($fechaevento);
                $nombreevento = "actualizarPredioEstrato";
                break;
            case Estado::ACTUALIZAR_PREDIOS_ES:
                EstControl::actualizarPrediosEsEstrato($fechaevento);
                $nombreevento = "actualizarPredioEsEstrato";
                break;
            default:
                break;
        }

        $fechaeventocreado = EstControl::obtenerEventoEstrato($nombreevento);
        $idflujo = EstControl::obtenerIdFlujo();

        if($fechaevento == $fechaeventocreado)
        {
            $respuesta['estado'] = true;
        }
        else
        {
            $respuesta['estado'] = false;
        }

        $fechaactual = date("Y-m-d H:i:s");
        $segundos = Utilidades::obtenerDiferenciaFechas($fechaevento, $fechaactual, "segundos");
        $tiempo = EstControl::calcularTiempo($codigoestadosubproceso);
        $minutos = ($tiempo / 60);
        $minutos = number_format($minutos, 0);

        $respuesta['tiempo'] = $tiempo;
        $respuesta['segundos'] = $segundos;
        $respuesta['minutos'] = $minutos;

        if($idflujo)
        {
            $id = EstControl::obtenerIdSubproceso($idestadoproceso, $idestadosubproceso, $idflujo);
        }
        else
        {
            $id = false;
        }

        if($id)
        {
            $estcontrol = Doctrine_Core::getTable('estcontrol')->find($id);
            $estcontrol->setTiempo($tiempo);
            $estcontrol->save();

            if($tiempo > 10)
            {
                $porcentaje = ((100 / $tiempo) * $segundos);
                $porcentaje = number_format($porcentaje, 0);
                $respuesta['porcentaje'] = $porcentaje;
            }
            else
            {
                $respuesta['porcentaje'] = false;
            }
        }
        else if($tiempo > 10)
        {
            $respuesta['porcentaje'] = 1;
        }
        else
        {
            $respuesta['porcentaje'] = false;
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que valida el estado de la función executeActualizarPrediosEstrato() tantas veces como itereciones haya, esta función 
     * hace parte del proceso de actualización de la información de estratificación para actualizar estratos
     * @return type JSON array
     */
    public function executeValidarActualizarPrediosEstrato(sfWebRequest $request)
    {
        $codigoestadosubproceso = $request->getParameter('codigoestadosubproceso');
        $respuesta = array();

        $respuesta['codigoestadosubproceso'] = $codigoestadosubproceso;

        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ESTRATO_MASIVO);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', $codigoestadosubproceso);
        $idestadosubproceso = $estadosubproceso->idestado;

        switch($codigoestadosubproceso)
        {
            case Estado::ACTUALIZAR_PREDIOS:
                $nombreevento = "actualizarPredioEstrato";
                break;
            case Estado::ACTUALIZAR_PREDIOS_ES:
                $nombreevento = "actualizarPredioEsEstrato";
                break;
            default:
                break;
        }

        $idflujo = EstControl::obtenerIdFlujo();
        $idestado = EstControl::obtenerEstadoEstrato($idestadoproceso, $idestadosubproceso, $idflujo);

        $tiempo = EstControl::calcularTiempo($codigoestadosubproceso);

        if($idestado)
        {
            $id = EstControl::obtenerIdSubproceso($idestadoproceso, $idestadosubproceso, $idflujo);
            $estcontrol = Doctrine_Core::getTable('estcontrol')->find($id);
            $tiempoest = $estcontrol->getTiempo();

            if( ! $tiempoest)
            {
                $estcontrol->setTiempo($tiempo);
                $estcontrol->save();
            }

            $fechaactual = date("Y-m-d H:i:s");
            $fechaevento = EstControl::obtenerEventoEstrato($nombreevento);
            $segundos = Utilidades::obtenerDiferenciaFechas($fechaactual, $fechaevento, "segundos");

            $respuesta['fechaactual'] = $fechaactual;
            $respuesta['fechaevento'] = $fechaevento;
            $respuesta['segundos'] = $segundos;
            $respuesta['tiempo'] = $tiempo;

            if($tiempo > 10)
            {
                $porcentaje = ((100 / $tiempo) * $segundos);
                $porcentaje = number_format($porcentaje, 0);
                $respuesta['porcentaje'] = $porcentaje;
            }
            else
            {
                $respuesta['porcentaje'] = false;
            }

            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::COMPLETADA)
            {
                $respuesta['estado'] = true;
            }
            else
            {
                if($codigoestado == Estado::FALLIDO)
                {
                    $respuesta['estado'] = "fallido";
                }
                else
                {
                    $respuesta['estado'] = false;
                }
            }
        }
        else if($tiempo > 10)
        {
            $respuesta['porcentaje'] = 1;
            $respuesta['estado'] = false;
        }
        else
        {
            $respuesta['porcentaje'] = false;
            $respuesta['estado'] = false;
        }

        sleep(12);

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que actualiza el estrato de los predios cuya información catastral ha cambiado.
     * @return type JSON boolean
     */
    public function executeActualizarMasivoEstrato()
    {
        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ESTRATO_MASIVO);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ESTRATOS);
        $idestadosubproceso = $estadosubproceso->idestado;

        EstControl::actualizarMasivoEstrato();
        $idflujo = EstControl::obtenerIdFlujo();
        $idestado = EstControl::obtenerEstadoEstrato($idestadoproceso, $idestadosubproceso, $idflujo);

        if($idestado)
        {
            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::COMPLETADA)
            {
                $respuesta = true;
            }
        }
        else
        {
            $respuesta = false;
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que genera el reporte de actualización de estratos por predio en formato CSV
     * @return type JSON array
     */
    public function executeGenerarReporteProcesoEstrato()
    {
        $respuesta = array();

        $reporte = EstControl::generarReporteProcesoEstrato();
        $datos = Utilidades::descargarCSV($reporte, ';');
        $datos = utf8_encode($datos);

        $directoriosubida = sfConfig::get("sf_dir_documentos");

        $nombrearchivo = "reporte_actualizacion_estrato_" . date("Ymd_His");
        $archivo = fopen($directoriosubida . $nombrearchivo . ".csv", "w");
        fwrite($archivo, $datos);
        fclose($archivo);

        if(file_exists($directoriosubida . $nombrearchivo . ".csv"))
        {
            $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ESTRATO_MASIVO);
            $idestadoproceso = $estadoproceso->idestado;

            $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::GENRAR_REPORTE);
            $idestadosubproceso = $estadosubproceso->idestado;

            $idflujo = EstControl::obtenerIdFlujo();

            $estcontrol = new EstControl();
            $estcontrol->setIdflujo($idflujo);
            $estcontrol->setIdestadoproceso($idestadoproceso);
            $estcontrol->setIdestadosubproceso($idestadosubproceso);
            $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::COMPLETADA);
            $estcontrol->setIdestado($estado->idestado);
            $estcontrol->setArchivo($nombrearchivo . ".csv");
            $estcontrol->save();

            $respuesta['link'] = $nombrearchivo . ".csv";
            $respuesta['estado'] = true;
        }
        else
        {
            $respuesta['estado'] = false;
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que redirecciona a la función actualizarAtipica
     */
    public function executeActualizarAtipica()
    {
        $this->setTemplate('actualizarAtipica');
    }

    /**
     * Función que elimina los registros de la tabla "est_bd_atipicas", esta función hace parte del proceso de actualización de la 
     * información de estratificación para actualizar atipicidades.
     * @return type JSON boolean
     */
    public function executeElimiarRegistrosAtipica()
    {
        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ATIPICA);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ELIMINAR_REGISTROS);
        $idestadosubproceso = $estadosubproceso->idestado;

        EstControl::eliminarRegistrosEstrato();
        $idflujo = EstControl::obtenerIdFlujo();

        if($idflujo)
        {
            $idestado = EstControl::obtenerEstadoEstrato($idestadoproceso, $idestadosubproceso, $idflujo);
            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::PENDIENTE)
            {
                $idestado = false;
            }
        }
        else
        {
            $idestado = false;
        }

        if($idestado)
        {
            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::COMPLETADA)
            {
                $respuesta = true;
            }
        }
        else
        {
            $respuesta = false;
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que valida la existencia de por lo menos un registro en las tablas "est_bd_atipicas", esta función hace parte del proceso de 
     * actualización de la información de estratificación para actualizar atipicidades.
     * @return type JSON array
     */
    public function executeValidarRegistrosAtipica()
    {
        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ATIPICA);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::VALIDAR_REGISTROS);
        $idestadosubproceso = $estadosubproceso->idestado;

        $registros = EstControl::comprobarRegistrosAtipica();
        $respuesta = array();

        foreach($registros as $key=> $val)
        {
            if($val > 0)
            {
                $respuesta[$key] = $val;
                $respuesta['estado'] = true;
            }
            else
            {
                $respuesta['estado'] = false;
                break;
            }
        }

        if($respuesta['estado'])
        {
            $estcontrol = new EstControl();
            $estcontrol->setIdflujo($respuesta['idflujo']);
            $estcontrol->setIdestadoproceso($idestadoproceso);
            $estcontrol->setIdestadosubproceso($idestadosubproceso);
            $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::COMPLETADA);
            $estcontrol->setIdestado($estado->idestado);
            $estcontrol->save();
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que depura los registros de las tablas "est_bdcatastral", "est_es_anterior" y "est_es" eliminando espacios en blanco, caracteres
     * extraños, etc, esta función hace parte del proceso de actualización de la información de estratificación para actualizar atipicidades.
     * @return type JSON boolean
     */
    public function executeDepurarRegistrosAtipica()
    {
        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ATIPICA);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::DEPURAR_REGISTROS);
        $idestadosubproceso = $estadosubproceso->idestado;

        EstControl::depurarRegistrosAtipica();
        $idflujo = EstControl::obtenerIdFlujo();

        if($idflujo)
        {
            $idestado = EstControl::obtenerEstadoEstrato($idestadoproceso, $idestadosubproceso, $idflujo);
        }
        else
        {
            $idestado = false;
        }


        if($idestado)
        {
            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::COMPLETADA)
            {
                $respuesta = true;
            }
        }
        else
        {
            $respuesta = false;
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que actualiza las atipicidades de los predios en la tabla "est_atipicas", esta función hace parte del proceso de actualización 
     * de la información de estratificación para actualizar atipicidades.
     * @return type JSON array
     */
    public function executeActualizarPrediosAtipica(sfWebRequest $request)
    {
        $codigoestadosubproceso = $request->getParameter('codigoestadosubproceso');

        $respuesta = array();

        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ATIPICA);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', $codigoestadosubproceso);
        $idestadosubproceso = $estadosubproceso->idestado;

        $fecha = date('Y-m-d H:i:s');
        $fechaevento = strtotime('+10 second', strtotime($fecha));
        $fechaevento = date('Y-m-d H:i:s', $fechaevento);

        EstControl::actualizarPrediosAtipica($fechaevento);
        $nombreevento = "actualizarPredioAtipica";

        $fechaeventocreado = EstControl::obtenerEventoEstrato($nombreevento);

        if($fechaevento == $fechaeventocreado)
        {
            $respuesta['estado'] = true;
        }
        else
        {
            $respuesta['estado'] = false;
        }

        $fechaactual = date("Y-m-d H:i:s");
        $segundos = Utilidades::obtenerDiferenciaFechas($fechaevento, $fechaactual, "segundos");
        $tiempo = EstControl::calcularTiempo($codigoestadosubproceso);
        $minutos = ($tiempo / 60);
        $minutos = number_format($minutos, 0);

        $respuesta['tiempo'] = $tiempo;
        $respuesta['segundos'] = $segundos;
        $respuesta['minutos'] = $minutos;

        $idflujo = EstControl::obtenerIdFlujo();

        if($idflujo)
        {
            $id = EstControl::obtenerIdSubproceso($idestadoproceso, $idestadosubproceso, $idflujo);
        }
        else
        {
            $id = false;
        }

        if($id)
        {
            $estcontrol = Doctrine_Core::getTable('estcontrol')->find($id);
            $estcontrol->setTiempo($tiempo);
            $estcontrol->save();

            if($tiempo > 10)
            {
                $porcentaje = ((100 / $tiempo) * $segundos);
                $porcentaje = number_format($porcentaje, 0);
                $respuesta['porcentaje'] = $porcentaje;
            }
            else
            {
                $respuesta['porcentaje'] = false;
            }
        }
        else if($tiempo > 10)
        {
            $respuesta['porcentaje'] = 1;
        }
        else
        {
            $respuesta['porcentaje'] = false;
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que valida el estado de la función executeActualizarPrediosAtipica() tantas veces como itereciones haya, esta función 
     * hace parte del proceso de actualización de la información de estratificación para actualizar atipicidades.
     * @return type JSON array
     */
    public function executeValidarActualizarPrediosAtipica(sfWebRequest $request)
    {
        $codigoestadosubproceso = $request->getParameter('codigoestadosubproceso');
        $respuesta = array();

        $respuesta['codigoestadosubproceso'] = $codigoestadosubproceso;

        $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ATIPICA);
        $idestadoproceso = $estadoproceso->idestado;

        $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', $codigoestadosubproceso);
        $idestadosubproceso = $estadosubproceso->idestado;

        $nombreevento = "actualizarPredioAtipica";

        $idflujo = EstControl::obtenerIdFlujo();
        $idestado = EstControl::obtenerEstadoEstrato($idestadoproceso, $idestadosubproceso, $idflujo);

        $tiempo = EstControl::calcularTiempo($codigoestadosubproceso);

        if($idestado)
        {
            $id = EstControl::obtenerIdSubproceso($idestadoproceso, $idestadosubproceso, $idflujo);
            $estcontrol = Doctrine_Core::getTable('estcontrol')->find($id);
            $tiempoest = $estcontrol->getTiempo();

            if( ! $tiempoest)
            {
                $estcontrol->setTiempo($tiempo);
                $estcontrol->save();
            }

            $fechaactual = date("Y-m-d H:i:s");
            $fechaevento = EstControl::obtenerEventoEstrato($nombreevento);
            $segundos = Utilidades::obtenerDiferenciaFechas($fechaactual, $fechaevento, "segundos");

            $respuesta['fechaactual'] = $fechaactual;
            $respuesta['fechaevento'] = $fechaevento;
            $respuesta['segundos'] = $segundos;
            $respuesta['tiempo'] = $tiempo;

            if($tiempo > 10)
            {
                $porcentaje = ((100 / $tiempo) * $segundos);
                $porcentaje = number_format($porcentaje, 0);
                $respuesta['porcentaje'] = $porcentaje;
            }
            else
            {
                $respuesta['porcentaje'] = false;
            }

            $codigoestado = Estado::obtenerCodigo($idestado);

            if($codigoestado == Estado::COMPLETADA)
            {
                $respuesta['estado'] = true;
            }
            else
            {
                $respuesta['estado'] = false;
            }
        }
        else if($tiempo > 10)
        {
            $respuesta['porcentaje'] = 1;
            $respuesta['estado'] = false;
        }
        else
        {
            $respuesta['porcentaje'] = false;
            $respuesta['estado'] = false;
        }

        sleep(12);

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que genera el reporte de actualización de atipicidades por predio en formato CSV
     * @return type JSON array
     */
    public function executeGenerarReporteProcesoAtipica()
    {
        $respuesta = array();

        $reporte = EstControl::generarReporteProcesoAtipica();
        $datos = Utilidades::descargarCSV($reporte, ';');
        $datos = utf8_encode($datos);

        $directoriosubida = sfConfig::get("sf_dir_documentos");

        $nombrearchivo = "reporte_actualizacion_atipica_" . date("Ymd_His");
        $archivo = fopen($directoriosubida . $nombrearchivo . ".csv", "w");
        fwrite($archivo, $datos);
        fclose($archivo);

        if(file_exists($directoriosubida . $nombrearchivo . ".csv"))
        {
            $estadoproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::ACTUALIZAR_ATIPICA);
            $idestadoproceso = $estadoproceso->idestado;

            $estadosubproceso = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::GENRAR_REPORTE);
            $idestadosubproceso = $estadosubproceso->idestado;

            $idflujo = EstControl::obtenerIdFlujo();

            $estcontrol = new EstControl();
            $estcontrol->setIdflujo($idflujo);
            $estcontrol->setIdestadoproceso($idestadoproceso);
            $estcontrol->setIdestadosubproceso($idestadosubproceso);
            $estado = Doctrine_Core::getTable('estado')->findOneBy('codigo', Estado::COMPLETADA);
            $estcontrol->setIdestado($estado->idestado);
            $estcontrol->setArchivo($nombrearchivo . ".csv");
            $estcontrol->save();

            $respuesta['link'] = $nombrearchivo . ".csv";
            $respuesta['estado'] = true;
        }
        else
        {
            $respuesta['estado'] = false;
        }

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que despliega en pantalla el formulario para registrar una notificación
     * @param sfWebRequest $request
     */
    public function executeNotificacion(sfWebRequest $request)
    {
        $this->formsolicitud = new SolicitudEstratificacionForm();
        $this->formnotificacion = new NotificacionEstratificacionForm();
        $this->formdocumentos = new AdjuntarDocumentosEstratificacionForm();
        
        if($request->isMethod("post"))
        {
            $this->notificacion($request);
        }
    }

    /**
     * Funcion que registra la información del DAPM y la notificación para la solcitud de revsión de estrato y apelación
     * @param sfWebRequest $request
     */
    protected function notificacion(sfWebRequest $request)
    {
        $solicitud = new Solicitud();
        $idsolicitud = $solicitud->obtenerIdsolicitudxRadicado($request->getParameter('radicado_notificacion'));
        $this->forward404Unless($solicitud = Doctrine_Core::getTable('solicitud')->find(array($idsolicitud)), sprintf('La solicitud no existe (%s).', $idsolicitud));
        $codigodapm = $request->getParameter('dapm');
        $fechanotificacion = $request->getParameter('fecha_notificacion');

        $notificacion = new EstDapm();
        $notificacion->setIdsolicitud($idsolicitud);
        $notificacion->setCodigodapm($codigodapm);
        $notificacion->setFechaNotificacion($fechanotificacion);
        $notificacion->save();

        $resultado = array();

        if($this->subirDocumento($request, $solicitud))
        {
            $ws = new OrfeoWS();

            if( ! ($this->adjuntarDocumentosOrfeo($ws, $solicitud)))
            {
                $estadoactual = Estado::PENDIENTE_ANEXOS_ORFEO;
                $this->actualizarEstado($solicitud, $estadoactual);
                $this->redirect('estratificacion/mostrarMensajeFalloOrfeo?idsolicitud=' . $solicitud->getIdsolicitud());
            }
            else
            {
                $mensaje = 'mensaje';
                $this->redirect('estratificacion/mostrarMensaje?mensaje=' . $mensaje . '&idsolicitud=' . $idsolicitud);
            }
        }
        else
        {
            $this->redirect('estratificacion/mostrarMensajeFalloOrfeo?idsolicitud=' . $solicitud->getIdsolicitud());
        }
    }

    /**
     * Retorna al javascript el resultado de la validación que permite identificar si hay una notificación asociada al radicado ingresado
     * @param sfWebRequest $request
     * @return type JSON
     */
    public function executeVerificarNotificacionExistente(sfWebRequest $request)
    {
        $radicadonotificacion = $request->getParameter('radicadonotificacion');
        $respuesta = $this->validarNotificacionExistente($radicadonotificacion);

        $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
        return $this->renderText(json_encode($respuesta));
    }

    /**
     * Función que valida que no exista una notificación asociada al radicado ingresado
     * @param type $radicadonotificacion
     * @return boolean
     */
    private function validarNotificacionExistente($radicadonotificacion)
    {
        $solicitud = Doctrine::getTable('Solicitud')->findOneByRadicado($radicadonotificacion);
        $idsolicitud = $solicitud->getIdsolicitud();

        $documento = Doctrine::getTable('Documento')->findOneByNombre('Notificación de la solicitud');

        $solicitudxdocumento = new Solicitudxdocumento();
        $solicitudxdocumento->setIdsolicitud($idsolicitud);
        $solicitudxdocumento->setIddocumento($documento->getIddocumento());
        $regsolicitudxdocumento = $solicitudxdocumento->traeDocumentosxid();

        if($regsolicitudxdocumento)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
