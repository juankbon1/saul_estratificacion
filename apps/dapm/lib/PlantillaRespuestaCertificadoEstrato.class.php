<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlantillaRespuetaCertificadoEstrato
 *
 * @author juan.borrero@cali.gov.co
 */
class PlantillaRespuetaCertificadoEstrato extends CertificadoPDF
{

    public $radicado = null;
    public $direccionPredio = null;
    public $nombreSubdirector = null;
    public $firmaMecanica = null;
    public $urlQR = null;
    public $urlConsulta = null;

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false)
    {
        setlocale(LC_ALL, "es_CO");

        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
        $this->archivo = sha1(date("d-m-Y H:i:s")) . '_P.pdf';

        $this->urlQR = sfContext::getInstance()->getRequest()->getUriPrefix() .
                sfContext::getInstance()->getRequest()->getRelativeUrlRoot() .
                '/solicitud/consultarestado/radicado/';
        $this->urlConsulta = sfContext::getInstance()->getRequest()->getUriPrefix() .
                sfContext::getInstance()->getRequest()->getRelativeUrlRoot() .
                '/solicitud/consultarestado';
    }

    public function Header()
    {
        $this->insertarMarcadeagua('emitido.jpg');

//        parent::Header();
        // Logo
        $image_file = $this->dirImagenes . $this->logo;
        //$file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
        $this->Image($image_file, 17, 7, 30, '', 'PNG', '', 'T', true, 900, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        $this->Cell(0, 15, '', 0, false, 'C', 0, '', 0, false, 'M', 'M');

        if($this->radicado)
        {
            $this->establecerEncabezadoRadicacion(0, 12);
        }
    }

    public function Footer()
    {
        $this->SetY(273);
//        $this->SetX(25);
        $this->SetFont('helvetica', 'A', 6);
        $texto = 'Este documento ha sido generado a través del Sistema Automatizado en Línea - SAUL del Departamento Administrativo '
                . 'de Planeación Municipal, el ' . $this->fechaHoy . ' a las ' . $this->hora . ' y está firmado digitalmente. '
                . 'Queda prohibida su alteración o modificación por cualquier medio, sin previa autorización del Alcalde.' . "\n \n"
                . 'En atención del desarrollo de nuestros Sistemas de Gestión y Control Integrados le solicito comedidamente diligenciar '
                . 'la encuesta de satisfacción de usuario accediendo al siguiente enlace http://www.cali.gov.co/publicaciones/103935/percepcion-del-usuario/';
        ;
//        $texto = 'LA PRESENTE CERTIFICACIÓN ES VALIDA SIN ESTAMPILLA' . "\n"
//                . 'Este documento es propiedad de la Administración Central del Municipio de Santiago de Cali y ha sido generado automáticamente por el aplicativo SAUL del DEPARTAMENTO ADMINISTRATIVO DE PLANEACIÓN MUNICIPAL, el ' . $this->fechaHoy . ' a las ' . $this->hora
//                . ', está prohibida su alteración o modificación por cualquier medio, sin previa autorización del Alcalde.' . "\n"
//                . 'Se ha firmado digitalmente y se ha adjuntado al radicado No ' . $this->radicado . ', para efectos de verificación. Número Interno del Sistema: ' . $this->solicitud . ".\n \n"
//                . 'En atención del desarrollo de nuestros Sistemas de Gestión y Control Integrados le solicito comedidamente diligenciar la encuesta de satisfacción de usuario accediendo al siguiente enlace http://www.cali.gov.co/publicaciones/103935/percepcion-del-usuario/';
//        $this->MultiCell(0, 8, $texto . "\n", 0, 'C', 0, 2, $this->GetX(), $this->GetY(), true, 0, false, true, false, '', true);
        $this->MultiCell(148, 8, $texto . "\n", 0, 'C', 0, 2, $this->GetX(), $this->GetY(), true, 0, false, true, false, '', true);
        $this->insertarFirmaMecanica();
        $this->insertarQR();
    }

    /**
     * Establece el titulo de la plantilla
     */
    public function establecerTitulo()
    {
        $this->SetY(73);
        $this->SetX(45);
        $this->SetFont('helvetica', 'B', 13);
        $this->Cell($w = 15, $h = 0, $this->nombretramite, $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
    }

    /**
     * Establece el contenido o cupero de la plantilla
     */
    public function establecerContenido()
    {
        if(strlen($this->cum) == 9)
        {
            $rural = "ubicado en la dirección <strong>$this->direccionpredio</strong> cuyo lado se identifica con el Código Único Municipal <strong>$this->cum,</strong>";
        }
        else
        {
            $rural = "";
        }

        if($this->tipoatipicidad)
        {
            if($this->tipoatipicidad == "+")
            {
                $nombreatipicidad = "POSITIVA";
            }
            else
            {
                $nombreatipicidad = "NEGATIVA";
            }

            $tipoatipicidad = " Dada su atipicidad <b>" . $nombreatipicidad . " ( " . $this->tipoatipicidad . " )</b>";
        }
        else
        {
            $tipoatipicidad = "";
        }

        $this->SetFont('helvetica', 'A', 10);

        // Genera el certificado con estrato general y para predios en zona de expansión
        if($this->estrato)
        {
            if($this->estratonuevo)
            {
                $direccion1 = strtoupper($this->direccion1);

                if($this->direccion2 != "")
                {
                    $direccion2 = strtoupper($this->direccion2);
                    $direccion = "cuya dirección se enmarca entre: <strong>$direccion1</strong> y <strong>$direccion2</strong>";
                }
                else
                {
                    $direccion = "identificado con dirección <strong>$direccion1</strong>";
                }
                if($this->sector != "")
                {
                    $sector = "ubicado en el sector <strong>$this->sector</strong>,";
                }
                else
                {
                    $sector = "";
                }

                $parrafonpn = "";
                $rural = "$direccion, para el proyecto <strong> $this->proyecto</strong>, $sector se identifica con Código Único Municipal <strong>$this->cum</strong> y";
            }
            else
            {
                $parrafonpn = "identificado con Número Predial Nacional <strong>$this->numerounico,</strong>";
            }
            $alturatexto = 90;
            $area2 = <<<EOD
                    <table>
                        <tr>
                         <td>
                            <p align="justify" style="font-size: 12px">La Secretaria Técnica del Comité Permanente de Estratificación hace constar que de conformidad con la base de 
                               datos de la Estratificación Socioeconómica del Municipio de Cali, el predio $parrafonpn $rural pertenece al estrato 
                               <strong>$this->nombreestrato ($this->estrato)</strong>. $tipoatipicidad
                            </p>
                            <p align="justify" style="font-size: 12px">La presente certificación se expide a la solicitud de (la) señor(a) <strong>$this->nombreapellidosolicitante</strong>.
                            </p>
                            <p align="justify" style="font-size: 10px"><strong>Nota 1:</strong> Este certificado es válido única y exclusivamente con las estampillas de ProDesarrollo 
                                Urbano (Alcaldía), Procultura (Alcaldia), Prosalud Departamental (Gobernación) y Prohospitales Universitarios Departamentales (Gobernación).<br><strong>Nota 2: </strong> 
                                Este certificado tiene vigencia de 2 meses.<br><strong>Nota 3: </strong> Para verificar la autenticidad de este certificado,  puede utilizar el código QR ubicado 
                                en la parte inferior de este documento o ingresar al siguiente enlace: <a href="$this->urlConsulta/radicado/$this->radicadoPadre">$this->urlConsulta/radicado/$this->radicadoPadre</a>
                            </p>
                            <!--<p align="center" style="font-size: 14px">
                                <a href="$this->urlConsulta/radicado/$this->radicadoPadre">$this->urlConsulta/radicado/$this->radicadoPadre</a>
                            </p>-->
                         </td>
                        </tr>                
                    </table>                
EOD;
            // Genera el certificado de predio disperso            
        }
        else if($this->nombreCorregimiento)
        {
            $alturatexto = 90;
            $area2 = <<<EOD
                    <table>
                        <tr>
                         <td>
                            <p align="justify" style="font-size: 12px">Con relación a su solicitud de Certificado de Estrato para el predio ubicado en el Corregimiento $this->nombreCorregimiento,
                                me  permito informarle  que el Municipio de Santiago de Cali, mediante Decreto Municipal 2105 de Diciembre 31 de 1998 y en cumplimiento 
                                con lo estipulado en el Artículo 55 de la Ley 383 de 1997, adoptó la Estratificación Socioeconómica de los Centros Poblados del Área Rural del Municipio, 
                                quedando pendiente por elaborar y adoptar la correspondiente a la de Fincas y Viviendas Dispersas,  dentro de la cual se encuentra clasificado su predio. 
                            </p>
                            <p align="justify" style="font-size: 12px">Por lo anterior y al tenor de lo estipulado en el Parágrafo 2do. Del artículo 6o. de la Ley 732 de 2002 que a la 
                                letra dice: "Parágrafo 2°. Cuando la estratificación socioeconómica no haya sido adoptada por decreto municipal o distrital, la empresa que presta el 
                                servicio público domiciliario por cuyo cobro se reclama deberá atenderlo directamente en primera instancia, y la apelación se surtirá ante la Superintendencia 
                                de Servicios Públicos Domiciliarios".
                            </p>
                            <p align="justify" style="font-size: 10px"><strong>Nota 1:</strong> Este certificado es válido única y exclusivamente con las estampillas de ProDesarrollo 
                                Urbano (Alcaldía), Procultura (Alcaldia), Prosalud Departamental (Gobernación) y Prohospitales Universitarios Departamentales (Gobernación).<br><strong>Nota 2: </strong> 
                                Este certificado tiene vigencia de 2 meses.<br><strong>Nota 3: </strong> Para verificar la autenticidad de este certificado,  puede utilizar el código QR ubicado 
                                en la parte inferior de este documento o ingresar al siguiente enlace: <a href="$this->urlConsulta/radicado/$this->radicadoPadre">$this->urlConsulta/radicado/$this->radicadoPadre</a>
                            </p>
                         </td>
                        </tr>                
                    </table>                
EOD;
            // Genera el certificado de información no existente en base de datos    
        }
        else
        {
            $alturatexto = 94;
            $area2 = <<<EOD
                    <table>
                        <tr>
                         <td>
                            <p align="justify" style="font-size: 14px">Con relación a su solicitud de Revisión y/o Certificado de Estrato me permito comunicarle que la información de estrato 
                               correspondiente a su predio no aparece en nuestra base de datos, por lo tanto para que su solicitud sea atendida se debe realizar una visita al predio con el 
                               fin de levantar la información pertinente de acuerdo con la metodología suministrada por el Departamento Administrativo de Planeación.
                            </p>
                             <p align="justify" style="font-size: 14px">Para efectos de lo anterior se dispone de un término de dos (2) meses, prorrogables por treinta (30) días hábiles más, 
                                al tenor de lo estipulado en el Artículo 6 de la Ley 732 de 2002.
                            </p>
                            <p align="justify" style="font-size: 14px">Una vez se tengan los resultados definitivos del cálculo del estrato que le corresponde a su predio, se le notificará 
                               personalmente.
                            </p>
                            <p align="justify" style="font-size: 14px">Cordialmente,
                            </p>
                         </td>
                        </tr>                
                    </table>                
EOD;
        }
        $this->SetFont('helvetica', 'A', 10);
        $this->writeHTMLCell($w = 175, $h = 12, $x = '', $y = $alturatexto, $area2, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
    }

    /**
     * Inserta el codigo QR en el formato
     */
    protected function insertarQR()
    {
        $estilo = array(
            'border'=>0,
            'fgcolor'=>array(0, 0, 0),
            'bgcolor'=>false, //array(255,255,255)
            'module_width'=>2, // width of a single module in points
            'module_height'=>2 // height of a single module in points
        );
        $this->write2DBarcode($this->urlQR . $this->radicadoPadre, 'QRCODE,L', $x = 169, $y = 266, $w = 20, $h = 20, $estilo, 'N');
    }

    protected function insertarFirmaMecanica()
    {
        if($this->estrato || $this->nombreCorregimiento)
        {
            $alturafirma = 170;
            $alturatextofirma = 189;
        }
        else
        {
            $alturafirma = 196;
            $alturatextofirma = 215;
        }

        $image_file = '..\..\..\web\images\FirmaSubdirector.jpg';
//        firma Cesar Londono
//        $this->Image($this->firmaMecanica, 75, 190, 60, '', 'PNG', '', 'T', true, 900, '', false, false, 0, false, false, false);
//        firma Victoria Munoz
//        $this->Image($this->firmaMecanica, 75, 202, 60, '', 'PNG', '', 'T', true, 900, '', false, false, 0, false, false, false);
//        firma Elena Londono
//        $this->Image($this->firmaMecanica, 75, 205, 60, '', 'PNG', '', 'T', true, 900, '', false, false, 0, false, false, false);
//        firma Esperanza Forero
//        $this->Image($this->firmaMecanica, 68, 197, 60, '', 'PNG', '', 'T', true, 900, '', false, false, 0, false, false, false);
        $this->Image($this->firmaMecanica, 73, $alturafirma, 60, '', 'PNG', '', 'T', true, 900, '', false, false, 0, false, false, false);

        $this->SetY($alturatextofirma);
        $this->SetX(8);
        $this->SetFont('helvetica', 'B', 9);
        $texto = "Subdirectora de " . $this->infoSubdirector;
        $texto1 = $this->nombreSubdirector;

        $this->MultiCell(196, 0, $texto1 . "\n" . $texto . "\n", 0, 'C', 0, 2, $this->GetX(), $this->GetY(), true, 0, false, true, false, '', true);
    }

    protected function insertarEstampillas()
    {
        // -- set new background ---
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->getAutoPageBreak();
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        //$file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
        $this->Image($this->dirImagenes . DIRECTORY_SEPARATOR . 'EspacioParaEstampillas.png', 65, 205, 80, 60, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        //$this->setPageMark();
    }

    /**
     * Sobreescribe el atributo fecha
     * @param type $fecharespuesta
     */
    public function establecerFechaRespuesta($fecharespuesta)
    {
        $this->fecharespuesta = $fecharespuesta;

        $date = date_create($fecharespuesta);
        $fecharespuesta = date_format($date, 'd-m-Y');
        $hora = date_format($date, 'H:i:s');

        $this->fechaHoy = $fecharespuesta;
        $this->hora = $hora;
    }

    /**
     * Establece la informacion del encabezado de radicacion
     * @param Integer $y
     */
    protected function establecerEncabezadoRadicacion($y = 0, $x = 0)
    {
        $this->establecerEstiloCodBarras();
        $this->escribirCodBarras(153 - $x, $y + 7, 50, 8, 60);
        // Set font
        $this->SetFont('helvetica', 'A', 10);
        $this->SetY($y + 16);
        $this->SetX(153 - $x);
        $this->Cell($w = 50, $h = 0, '*' . $this->radicado . '*', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 4, $ignore_min_height = false, $calign = 'T', $valign = 'M');
        $this->SetY($y + 20);
        $this->SetX(153 - $x);
        $this->SetFont('helvetica', 'B', 10);
        $this->Cell($w = 50, $h = 0, 'Al contestar por favor cite estos datos', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');

        $this->SetY($y + 24);
        $this->SetX(172 - $x);
        $this->SetFont('helvetica', 'A', 10);
        $this->Cell($w = 19, $h = 0, 'Solicitud No: ', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
        $this->SetX(178 - $x);
        $this->SetFont('helvetica', 'B', 10);
        $this->Cell($w = 24, $h = 0, $this->solicitud, $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');

        $this->SetY($y + 28);
        $this->SetX(158 - $x);
        $this->SetFont('helvetica', 'A', 10);
        if(strlen($this->radicado) > 15)
        {
            $this->Cell($w = 8, $h = 0, 'Radicado No: ', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
        }
        else
        {
            $this->Cell($w = 13, $h = 0, 'Radicado No: ', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
        }
        $this->SetX(178 - $x);
        $this->SetFont('helvetica', 'B', 10);
        $this->Cell($w = 25, $h = 0, $this->radicado, $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');

        $this->SetY($y + 32);
        $this->SetX(171 - $x);
        $this->SetFont('helvetica', 'A', 10);
        $this->Cell($w = 11, $h = 0, 'Fecha: ', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
        $this->SetX(178 - $x);
        $this->SetFont('helvetica', 'B', 10);
        $this->Cell($w = 25, $h = 0, $this->fechaHoy, $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');

        $this->SetY($y + 36);
        $this->SetX(150);
        $trd = $this->dependencia . '.' . $this->subdependencia . '.' . $this->serie . '.' . $this->subserie . '.' . $this->tipoDocumental . '.' . $this->consecutivo;
        $this->SetFont('helvetica', 'A', 10);
        if(strlen($trd) > 19)
        {
            $this->Cell($w = 1, $h = 0, 'TRD: ', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
        }
        else
        {
            $this->Cell($w = 14, $h = 0, 'TRD: ', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
        }
        $this->SetX(178 - $x);
        $this->SetFont('helvetica', 'B', 10);
        $this->Cell($w = 25, $h = 0, $trd, $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');

        if($this->radicadoPadre)
        {
            $this->SetY($y + 40);
            $this->SetX(158 - $x);
            $this->SetFont('helvetica', 'A', 10);
            if(strlen($this->radicadoPadre) > 17)
            {
                $this->Cell($w = 8, $h = 0, 'Radicado Padre: ', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
            }
            else
            {
                $this->Cell($w = 11, $h = 0, 'Radicado Padre: ', $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
            }
            $this->SetX(178 - $x);
            $this->SetFont('helvetica', 'B', 10);
            $this->Cell($w = 25, $h = 0, $this->radicadoPadre, $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
        }
    }

    /**
     * Genera la plantilla PDF
     * @param boolean $plantillaNP false por defecto para generar una sola plntilla (Permitidas), true si se desean generar las dos plantillas, Permitidas y No Permitidas, en caso de true
     * se debe pasar en el segundo parametro las actividades No Permitidas
     * @param array $actividadesNP Arreglo con las actividades No Permitidas, solo funciona en caso de que $plantillaNP sea true
     */
    public function obtenerPlantilla()
    {
        $this->establecerTitulo();
        $this->establecerContenido();
        if($this->estrato || $this->nombreCorregimiento)
        {
            $this->insertarEstampillas();
        }

        // Comentar para imprimir el archivo sobre el navegador
        $this->Output($this->dirDocumentos . $this->archivo, $this->salidaArchivo);
        // Descomentar para imprimir el archivo sobre el navegador  
//        $this->Output($this->archivo, 'I');
    }

}

?>
