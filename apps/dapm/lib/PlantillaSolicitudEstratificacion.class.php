<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlantillaCertificadoEstratoSolicitud
 *
 * @author juan.borrero@cali.gov.co
 */
class PlantillaSolicitudEstratificacion extends CertificadoPDF
{

    public $solicitud = null;
    public $email = null;
    public $barrio = null;
    public $direccionsolicitante = null;
    public $fechaRadicacion = null;
    public $radicado = null;
    public $telefono = null;
    public $nombretramite = null;
    public $urlQR = null;
    public $urlConsulta = null;
    public $archivo = null;
    public $dependencia = null;
    public $subdependencia = null;
    public $serie = null;
    public $subserie = null;
    public $tipoDocumental = null;
    public $consecutivo = null;
    public $instancia = null;
    public $enteresponsable = null;

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false)
    {
        setlocale(LC_ALL, "es_CO");
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
        $this->urlQR = sfContext::getInstance()->getRequest()->getUriPrefix() .
                sfContext::getInstance()->getRequest()->getRelativeUrlRoot() .
                '/solicitud/consultarestado/radicado/';
        $this->urlConsulta = sfContext::getInstance()->getRequest()->getUriPrefix() .
                sfContext::getInstance()->getRequest()->getRelativeUrlRoot() .
                '/solicitud/consultarestado';
    }

    public function Header()
    {
        parent::Header();
        if($this->radicado)
        {
            $this->establecerEncabezadoRadicacion(0);
        }

        $this->insertarMarco($x = 5, $y = 71, $w = 200, $h = 32);

        $this->SetY(80);
        $this->SetX(10);
        // Set font
        $this->SetFont('helvetica', 'I', 9);
        $this->Cell(0, 4, 'Para realizar seguimiento a su trámite, puede ingresar a la página web: ');

        $this->SetY(84);
        $this->SetX(10);
        // Set font
        $this->SetFont('helvetica', 'I', 9);
        $this->Cell(0, 4, $this->urlConsulta . '/radicado/' . $this->radicado . ',');

        $this->SetY(88);
        $this->SetX(10);
        // Set font
        $this->SetFont('helvetica', 'I', 9);
        $this->Cell(0, 4, 'o utilizar el código QR que se presenta a continuación.');

        $this->insertarQR();
    }

    public function Footer()
    {
        $this->SetY(280);
        $this->SetFont('helvetica', 'A', 6);
        $texto = 'Este documento es propiedad de la Administración Central del Municipio de Santiago de Cali y ha sido generado automáticamente por el aplicativo SAUL del DEPARTAMENTO ADMINISTRATIVO DE PLANEACIÓN MUNICIPAL, el ' . $this->fechaHoy . ' a las ' . $this->hora
                . ', está prohibida su alteración o modificación por cualquier medio, sin previa autorización del Alcalde.' . "\n"
                . 'Este comprobante se ha adjuntado al radicado No ' . $this->radicado . ', para efectos de verificación. Número Interno del Sistema: ' . $this->solicitud . "\n";
        $this->MultiCell(0, 6, $texto . "\n", 0, 'C', 0, 2, $this->GetX(), $this->GetY(), true, 0, false, true, false, '', true);
    }

    /**
     * Establece el titulo de la plantilla
     */
    public function establecerTitulo()
    {
        $this->SetY(55);
        $this->SetX($this->posiciontitulo);
        $this->SetFont('helvetica', 'B', 13);
        $this->Cell($w = 15, $h = 0, $this->nombretramite, $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M');
    }

    protected function insertarQR()
    {
        $estilo = array(
            'border'=>0,
            'fgcolor'=>array(0, 0, 0),
            'bgcolor'=>false, //array(255,255,255)
            'module_width'=>2, // width of a single module in points
            'module_height'=>2 // height of a single module in points
        );
        $this->write2DBarcode($this->urlQR . $this->radicado, 'QRCODE,L', $x = 165, $y = 75, $w = 25, $h = 25, $estilo, 'N');
    }

    /**
     * Establece el contenido principal de la plantilla
     */
    public function establecerContenidoPrincipal()
    {
        $this->SetFont('helvetica', 'A', 10);
        $this->fechaRadicacion = date("d-m-Y H:i:s");

        $this->datosFuncionario = <<<EOD
        <table border="0" cellspacing="0" cellpadding="1"  width="840">
          <tr>
            <td width ="695"><b>Nombre: </b> $this->nombreapellidosolicitante </td>

          </tr>
          <tr>
            <td><b>Correo: </b> $this->email </td>

          </tr>
          <tr>
            <td><b>Telefono: </b> $this->telefono </td>

          </tr>
          <tr>
            <td><b>Dirección: </b> $this->direccionsolicitante </td>

          </tr>
          <tr>
            <td><b>Barrio: </b> $this->barrio </td>

          </tr>
          <tr>
            <td><b>Documento de identificación: </b> $this->documento </td>
 
          </tr>
          <tr>
            <td><b>Tipo de la empresa: </b> $this->tipoempresa </td>

          </tr>
        </table>
EOD;

        $this->datosUbicacion = <<<EOD
        <table border="0" cellspacing="0" cellpadding="1"  width="840">
          <tr>
            <td width ="695"><b>Fecha: </b> $this->fechasolicitud </td>
          </tr>
          <tr>
            <td><b>Radicado: </b> $this->radicado </td>
          </tr>
          <tr>
            <td><b>Tipo de solicitud: </b> $this->nombretiposolicitud </td>
          </tr>
          <tr>
            <td><b>Número único nacional: </b> $this->numerounico </td>
          </tr>
          <tr>
            <td><b>Dirección: </b>$this->direccionpredio</td>
          </tr>
        </table>
EOD;

        $this->justificacion = <<<EOD
        <table border="0" cellspacing="0" cellpadding="1"  width="690">
          <tr>
            <td><p align="justify">$this->observacion</p></td>
          </tr> 
        </table>
EOD;

        $this->nota = <<<EOD
        <table border="0" cellspacing="0" cellpadding="1"  width="690">
          <tr>
            <td>
                <p align="justify"><strong>NOTA: </strong> De conformidad con el artículo 6 de la ley 732 de 2002, su revisión será atendida y resuelta en $this->instancia instancia 
                por $this->enteresponsable en un término de dos (2) meses y al cabo de dicho termino se le notificará en forma personal de los resultados de su reclamación bajo las 
                pautas de los artículos 74 y siguientes de la ley 1437 de 2011.</p>
            </td>
          </tr> 
        </table>
EOD;
// Esta es una nota temporal con vigencia durante el tiempo que no se pueda entregar el certificado de estrato        
//        $this->nota2 = <<<EOD
//        <table border="0" cellspacing="0" cellpadding="1"  width="690">
//          <tr>
//            <td>
//                <p align="justify"><strong>NOTA IMPORTANTE: </strong> Por motivos de ajustes en la plataforma web, se entregará respuesta a su solicitud de certificado de estrato a 
//                partir del día Lunes 12 de febrero de 2018, podrá descargarlo a través de la plataforma SAUL o reclamarlo en la Oficina de Atención al Ciudadano 
//                Ventanilla Única, agradecemos su compresión.</p>
//            </td>
//          </tr> 
//        </table>    
//EOD;
              
        $this->nota3 = <<<EOD
        <table border="0" cellspacing="0" cellpadding="1"  width="690">
          <tr>
            <td>
                <p align="justify"><strong>NOTA IMPORTANTE: </strong> La respuesta a esta solicitud requiere de las siguientes estampillas: ProDesarrollo
                Urbano (Alcaldía), Procultura (Alcaldia), Prosalud Departamental (Gobernación) y Prohospitales Universitarios
                Departamentales (Gobernación). Sin ellas el certificado no tentrá validez.</p>
            </td>
          </tr> 
        </table>    
EOD;
    }

    /**
     * Configura el contenido del documento
     */
    public function escribirHtml()
    {
        $this->establecerContenidoPrincipal();
        // INFORMACION DEL SOLICITANTE
        $this->establecerTituloNoEncabezado($x = 5, $y = 110, "INFORMACIÓN DEL SOLICITANTE", NULL, 'B', 10);
        $this->insertarMarco($x = 5, $y = 117, $w = 88, $h = 40);
        $this->SetFont('', '', 10);
        $this->writeHTMLCell($w = 175, $h = 12, $x = 6, $y = 119, $this->datosFuncionario, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        // INFORMACION DE LA SOLICITUD
        $this->establecerTituloNoEncabezado($x = 98, $y = 110, "INFORMACIÓN DE LA SOLICITUD", NULL, 'B', 10);
        $this->insertarMarco($x = 98, $y = 117, $w = 107, $h = 40);
        $this->SetFont('', '', 10);
        $this->writeHTMLCell($w = 175, $h = 12, $x = 100, $y = 119, $this->datosUbicacion, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        // JUSTIFICACION
        $this->establecerTituloNoEncabezado($x = 5, $y = 160, "OBSERVACIÓN", NULL, 'B', 10);
        $this->insertarMarco($x = 5, $y = 167, $w = 200, $h = 14 + $this->marcoinferior);
        $this->SetFont('', '', 10);
        $this->writeHTMLCell($w = 175, $h = 12, $x = 6, $y = 170, $this->justificacion, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        if(($this->tiposolicitud == Tiposolicitud::REVISION_ESTRATO) || ($this->tiposolicitud == Tiposolicitud::APELACION_ESTRATO))
        {
            // NOTA
            $this->SetFont('', '', 10);
            $this->writeHTMLCell($w = 175, $h = 12, $x = 6, $y = 176 + $this->marcoinferior, $this->nota, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        }
        // Esta es una nota temporal con vigencia durante el tiempo que no se pueda entregar el certificado de estrato
//        if($this->tiposolicitud == Tiposolicitud::CERTIFICADO_ESTRATO)
//        {
//            // NOTA TEMPORAL PARA CERTIFICADO
//            $this->SetFont('', '', 10);
//            $this->writeHTMLCell($w = 175, $h = 12, $x = 6, $y = 176 + $this->marcoinferior, $this->nota2, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
//        }
        if(($this->tiposolicitud == Tiposolicitud::CERTIFICADO_ESTRATO))
        {
            // NOTA
            $this->SetFont('', '', 10);
            $this->writeHTMLCell($w = 175, $h = 12, $x = 6, $y = 176 + $this->marcoinferior, $this->nota3, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        }
    }

    /**
     * 
     */
    public function adicionarSegundaHoja()
    {
        $this->lastPage();
        $this->AddPage();
        $this->establecerTituloNoEncabezado(5, 50, "FOTOGRAFÍA DE LA VISITA", NULL, 'B', 12);
        $this->insertarMarco($x = 5, $y = 55, $w = 200, $h = 149);
    }

    /**
     * Establece el valor del documento adjunto
     * @param string $adjunto
     */
    public function establecerAdjunto($adjunto)
    {
        $this->adjunto = $adjunto;
    }
    
    /**
     * Sobreescribe el atributo fecha
     * @param type $fechasolicitud
     */
    public function establecerFechaSolicitud($fechasolicitud)
    {
        $this->fechasolicitud = $fechasolicitud;
        
        $date = date_create($fechasolicitud);
        $fechasolicitud = date_format($date, 'd-m-Y');
        $hora = date_format($date, 'H:i:s');
        
        $this->fechaHoy = $fechasolicitud;
        $this->hora = $hora;
    }

    /**
     * Sobreescribe el atributo archivo
     * @param type $nombrearchivo
     */
    public function establecerNombreArchivo($nombrearchivo)
    {
        $this->archivo = $nombrearchivo;
    }

    /**
     * Imprime el archivo
     */
    public function obtenerPlantilla()
    {
        $this->establecerTitulo();
        $this->escribirHtml();
        // Comentar para imprimir el archivo sobre el navegador
        $this->Output($this->dirDocumentos . $this->archivo, 'F');
//         Descomentar para imprimir el archivo sobre el navegador  
//        $this->Output($this->archivo, 'I');
    }

}

?>
