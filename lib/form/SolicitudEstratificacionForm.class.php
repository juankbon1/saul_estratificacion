<?php
/**
 * SolicitudEstratificacionForm
 * Formulario para registrar una solicitud de estratificacion 
 *
 * @package    ruva
 * @subpackage form
 * @author     Juan Carlos Borrero Lopez
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SolicitudEstratificacionForm extends BaseSolicitudForm
{

    public function configure()
    {
        $this->widgetSchema['idtiposolicitud'] = new sfWidgetFormInputHidden(
                array(), array('id'=>'idtiposolicitud', 'name'=>'idtiposolicitud'));

        $this->widgetSchema['radicado'] = new sfWidgetFormInput(
                array('label'=>'Radicado: '), array('placeholder'=>'Ingrese el radicado de la solicitud', 'class'=>'input-xlarge', 'id'=>'radicado', 'name'=>'radicado',));
        
        $this->widgetSchema['radicado_hijo_apelacion'] = new sfWidgetFormInput(
                array('label'=>'Radicado de respuesta Orfeo: '), array('placeholder'=>'Ingrese el radicado de respuesta de Orfeo', 'class'=>'input-xlarge', 'id'=>'radicado_hijo_apelacion', 'name'=>'radicado_hijo_apelacion',));
        
        $this->widgetSchema['radicado_hijo'] = new sfWidgetFormInput(
                array('label'=>'Nuevo radicado hijo: '), array('placeholder'=>'Ingrese el nuevo radicado hijo', 'class'=>'form-control input-xlarge', 'id'=>'radicado_hijo', 'name'=>'radicado_hijo'));
        
        $this->widgetSchema['radicado_notificacion'] = new sfWidgetFormInput(
                array('label'=>'Radicado de la notificación: '), array('placeholder'=>'Ingrese el radicado de la notificación', 'class'=>'form-control input-xlarge', 'id'=>'radicado_notificacion', 'name'=>'radicado_notificacion'));

        $this->widgetSchema['observacion'] = new sfWidgetFormTextarea(
                array('label'=>'Observación: '), array('placeholder'=>'', 'class'=>'form-control', 'name'=>'observacion', 'id'=>'observacion', 'maxlength'=>'1840'));
        
        $this->widgetSchema['observacion_rural'] = new sfWidgetFormTextarea(
                array('label'=>'Observación: '), array('placeholder'=>'', 'class'=>'form-control', 'name'=>'observacion_rural', 'id'=>'observacion_rural', 'maxlength'=>'1840'));

        $this->widgetSchema['comentario'] = new sfWidgetFormTextarea(
                array('label'=>'Comentario: '), array('placeholder'=>'Escriba las indicaciones de ubicación de su predio', 'class'=>'form-control', 'name'=>'comentario', 'id'=>'comentario'));
    }
}

?>
