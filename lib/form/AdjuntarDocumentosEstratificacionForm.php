<?php

/**
 * AdjuntarDocumentosEstratificacionForm
 * Formulario para adjuntar los documentos relacionados a una solicitud de estratificacion 
 *
 * @package    ruva
 * @subpackage form
 * @author     Juan Carlos Borrero Lopez
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdjuntarDocumentosEstratificacionForm extends BaseForm
{

    public function configure()
    {
        $this->setWidgets(array(
            'archivoservicios'=>new sfWidgetFormInputFile(array('label'=>'Recibo de servicios públicos:'), array('class'=>'form-control input-sm')),
            'archivopredial'=>new sfWidgetFormInputFile(array('label'=>'Recibo predial:'), array('class'=>'form-control input-sm')),
            'archivoadicional'=>new sfWidgetFormInputFile(array('label'=>'Documento adicional de la solicitud:'), array('class'=>'form-control input-sm')),
            'archivoapelacion'=>new sfWidgetFormInputFile(array('label'=>'Documento soporte de la apelación:'), array('class'=>'form-control input-sm')),
            'archivonotificacion'=>new sfWidgetFormInputFile(array('label'=>'Documento soporte de la notificación:'), array('class'=>'form-control input-sm'))
        ));

        $this->setValidators(array(
            'archivoservicios'=>new sfValidatorFile(array(
                'mime_categories'=>array('formatos'=>array('application/pdf', 'application/x-pdf', 'image/jpeg', 'image/pjpeg')),
                'mime_types'=>'formatos',
                'path'=>sfConfig::get('sf_dir_documentos'),
                    )),
            'archivopredial'=>new sfValidatorFile(array(
                'mime_categories'=>array('formatos'=>array('application/pdf', 'application/x-pdf', 'image/jpeg', 'image/pjpeg')),
                'mime_types'=>'formatos',
                'path'=>sfConfig::get('sf_dir_documentos'),
                    )),
            'archivoadicional'=>new sfValidatorFile(array(
                'mime_categories'=>array('formatos'=>array('aplication/pdf', 'aplication/x-pdf')),
                'mime_types'=>'formatos',
                'path'=>sfConfig::get('sf_dir_documentos'),
                    )),
            'archivoapelacion'=>new sfValidatorFile(array(
                'mime_categories'=>array('formatos'=>array('aplication/pdf', 'aplication/x-pdf')),
                'mime_types'=>'formatos',
                'path'=>sfConfig::get('sf_dir_documentos'),
                    )),
        ));

        $this->widgetSchema->setNameFormat('documento[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
    }

    /**
     * Establece el id de la Solicitud a la cual se adjunta el Documento
     * @param type $idSolicitud
     */
    public function estableceIdSolicitud($idSolicitud)
    {
        $this->setDefault('idsolicitud', $idSolicitud);
    }

    /**
     * Establece el nombre del elemento tipo archivoservicios del formulario
     * @param type $nombre
     */
    public function estableceNombreDocumento($nombre)
    {
        $this->widgetSchema->setLabel('archivo', $nombre);
    }

}

?>