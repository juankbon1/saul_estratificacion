<?php

class UbicacionPredioForm extends BasePredioForm
{

    public function configure()
    {
        $opcionesbusqueda = array('numeropre'=>'Número predial nacional', 'direccion'=>'Dirección');
        $opcionestipozona = array('urbano'=>'Urbano', 'rural'=>'Rural');
        $opcionesconsitencia = array('si'=>'SI', 'no'=>'NO');
        $opcionesexistenpn = array('si'=>'SI', 'no'=>'NO');

        $this->widgetSchema['tipobusqueda'] = new sfWidgetFormChoice(array(
            'multiple'=>false,
            'label'=>'Seleccione una opción de búsqueda para su predio:  ',
            'expanded'=>true,
            'choices'=>$opcionesbusqueda,
            'renderer_options'=>array('formatter'=>array($this, 'formatter'))), array('class'=>'px', 'name'=>'tipobusqueda', 'id'=>'tipobusqueda'));

        $this->widgetSchema['tipobusqueda']->setDefault('numeropre');

        $this->widgetSchema['numeropre_rural'] = new sfWidgetFormInput(
                array('label'=>'Número predial nacional: '), array('placeholder'=>'Ingrese el número predial nacional', 'class'=>'input-xlarge', 'id'=>'numeropre_rural', 'name'=>'numeropre_rural'));
        
        $this->widgetSchema['numeropre_sin_dir'] = new sfWidgetFormInput(
                array('label'=>'Número predial nacional: '), array('placeholder'=>'Ingrese el número predial nacional', 'class'=>'input-xlarge', 'id'=>'numeropre_sin_dir', 'name'=>'numeropre_sin_dir'));

        $this->widgetSchema['tipozona'] = new sfWidgetFormChoice(array(
            'multiple'=>false,
            'label'=>'Seleccione el tipo de zona de su predio:  ',
            'expanded'=>true,
            'choices'=>$opcionestipozona,
            'renderer_options'=>array('formatter'=>array($this, 'formatter'))), array('class'=>'px', 'name'=>'tipozona', 'id'=>'tipozona'));

        $this->widgetSchema['tipozona']->setDefault('urbano');

        $this->widgetSchema['consistencia'] = new sfWidgetFormChoice(array(
            'multiple'=>false,
            'label'=>'¿El número predial nacional y la dirección corresponde a la de su predio? ',
            'expanded'=>true,
            'choices'=>$opcionesconsitencia,
            'renderer_options'=>array('formatter'=>array($this, 'formatter'))), array('class'=>'px', 'name'=>'consistencia', 'id'=>'consistencia'));
        
        $this->widgetSchema['consistencia']->setDefault('si');

        $this->widgetSchema['existe_npn'] = new sfWidgetFormChoice(array(
            'multiple'=>false,
            'label'=>'¿Encontró el número predial nacional en nuestra base de datos? ',
            'expanded'=>true,
            'choices'=>$opcionesexistenpn,
            'renderer_options'=>array('formatter'=>array($this, 'formatter'))), array('class'=>'px', 'name'=>'existe_npn', 'id'=>'existe_npn'));

        $this->widgetSchema['existe_npn']->setDefault('si');

        $this->widgetSchema['direccion'] = new sfWidgetFormInput(
                array('label'=>'Dirección del predio: <br> <font style = "font-size: 85%; color:#959595; font-weight: normal">Ingresar la dirección utilizando el siguiente estándar: A:Avenida, C:Calle, K:Carrera, P:Pasaje, T:Transversal, D:Diagonal, N:Norte, O:Oeste, 
                            no utilizar numeral(#) y guion(-). Ejemplo 1: C 14C 34 38, Ejemplo 2: K 26J D 72C 24, Ejemplo 3: K 102 21 74, Ejemplo 4: D 23 T 25 49</font>'), array('placeholder'=>'Ingrese la dirección del predio', 'class'=>'input-xlarge', 'id'=>'direccion', 'name'=>'direccion'));

        $this->widgetSchema['numeropre'] = new sfWidgetFormInput(
                array('label'=>'Número predial nacional: '), array('placeholder'=>'Ingrese el número predial nacional', 'class'=>'input-xlarge', 'id'=>'numeropre', 'name'=>'numeropre'));

        $this->widgetSchema['latitud'] = new sfWidgetFormInput(
                array('label'=>'Latitud: '), array('class'=>'form-control input-xlarge', 'id'=>'latitud', 'name'=>'latitud', 'readonly'=>true));

        $this->widgetSchema['longitud'] = new sfWidgetFormInput(
                array('label'=>'Longitud: '), array('class'=>'form-control input-xlarge', 'id'=>'longitud', 'name'=>'longitud', 'readonly'=>true));

        $this->widgetSchema['direccionnueva'] = new sfWidgetFormInput(
                array('label'=>'Nueva dirección del predio: '), array('class'=>'form-control input-xlarge', 'id'=>'direccionnueva', 'name'=>'direccionnueva', 'readonly'=>true));

        $this->widgetSchema['numeroprenuevo'] = new sfWidgetFormInput(
                array('label'=>'Nuevo Número predial nacional: '), array('class'=>'form-control input-xlarge', 'id'=>'numeroprenuevo', 'name'=>'numeroprenuevo', 'readonly'=>true));

        $this->widgetSchema['numeroprenuevo_rural'] = new sfWidgetFormInput(
                array('label'=>'Nuevo Número predial nacional: '), array('class'=>'form-control input-xlarge', 'id'=>'numeroprenuevo_rural', 'name'=>'numeroprenuevo_rural'));

        $this->widgetSchema['direccionadicional'] = new sfWidgetFormInput(
                array('label'=>'Dirección adicional: '), array('class'=>'form-control input-xlarge', 'id'=>'direccionadicional', 'name'=>'direccionadicional', 'readonly'=>true));
    }

    public function formatter($widget, $inputs)
    {
        $result = '<div class="checkbox">';

        foreach($inputs as $input)
        {

            $result .= '<div ><label> ' . $input ['input'] . '<span class="lbl">' . $input ['label'] . '</span></label></div>';
        }
        $result .= '</div>';
        return $result;
    }

}
